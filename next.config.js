module.exports = {
  // future: {
  //   webpack5: true,
  // },
  images: {
    domains: [
      "images.unsplash.com",
      "devm-one.kalbe.co.id",
      "hosting-open-innovation-gitlab.vercel.app",
      "open-innovation-web-staging-open-innovation-staging.apps.alpha.kalbe.co.id",
    ],
  },
};
