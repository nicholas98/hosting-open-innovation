import { Row, Col } from "reactstrap";
import { Show, Chat } from "react-iconly";
import Image from "next/image";
import Link from "next/link";
import Moment from "react-moment";
import React from "react";
import { useState, useEffect } from "react";
import { getImage } from "helper";
import { stripHtml } from "string-strip-html";

const CreateCategoryTag = ({ tagName, subTopik }) => (
  <Col xs="auto" className={`${!subTopik && "mr-2"} mb-2`}>
    <p className="border font detail-text text-center border-radius-8 p-2 truncate-one-line">
      {tagName}
    </p>
  </Col>
);

const IdeaCatalogCard = ({ home, data }) => {
  const [image, setImage] = useState([]);

  const idImage =
    data.ideaCatalogThumbnails !== null ? data.ideaCatalogThumbnails.fileId : 0;

  useEffect(() => {
    return getImage(idImage).then((data) => {
      const urlImage = URL.createObjectURL(data);
      setImage(urlImage);
    });
  }, []);

  if (!data) {
    return <p>Loading</p>;
  }

  return (
    <Col xl="3" lg="4" md="12">
      <div className={`card ${!home && "mb-4"} border-radius-12`}>
        <Link href={`/profile/${data.createdBy}/ideas`}>
          <a className="text-decoration-none">
            <div
              className="ideas-card-header"
              style={{
                backgroundColor: "#FFFFFF",
                borderRadius: "12px 12px 0 0",
              }}
            >
              <div className="d-flex align-items-center">
                <img
                  src={data.creatorProfilePicture !== "" ? data.creatorProfilePicture : "/images/profile-picture.jpg"}
                  width={32}
                  height={32}
                  className="card-header-photo"
                />
                <div className="mx-1"></div>
                <div className="d-flex flex-column">
                  <p className="font detail-text font-weight-bold">
                    {data.createdName}
                  </p>
                  <p className="font sub-detail-text text-kalbe-darkGrey">
                    <Moment format="DD MMM YYYY">{data.createdOn}</Moment>
                  </p>
                </div>
              </div>
            </div>
          </a>
        </Link>
        {/* <a className="text-decoration-none">
          <div
            className="ideas-card-header"
            style={{
              backgroundColor: "#FFFFFF",
              borderRadius: "12px 12px 0 0",
            }}
          >
            <div className="d-flex align-items-center">
              <Image
                src="/images/profile-picture.jpg"
                width={32}
                height={32}
                className="card-header-photo"
              />
              <div className="mx-1"></div>
              <div className="d-flex flex-column">
                <p className="font detail-text font-weight-bold">
                  {data.createdBy}
                </p>
                <p className="font sub-detail-text text-kalbe-darkGrey">
                  <Moment format="DD MMM YYYY">{data.createdOn}</Moment>
                </p>
              </div>
            </div>
          </div> */}

        <Link href={`/idea_catalog/detail/${data.id}`}>
          <a className="text-decoration-none">
            <div className="ideas-card-image-container pt-0">
              <img
                src={image !== null ? image : "/images/profile-picture.jpg"}
                layout="fill"
                style={{
                  width: "100%",
                  maxHeight: "140px",
                  maxWidth: "1148px",
                }}
                className="object-fit-cover"
              />
            </div>
            <div className="p-3">
              <p className="font body-copy font-weight-bold truncate-one-line mb-1">
                {data.title}
              </p>
              <p className="font detail-text truncate-two-line mb-2">
                {/* {data.description} */}
                {/* {unescape(data.description)} */}
                {stripHtml(unescape(data.description)).result}
              </p>
              <div className="d-flex align-items-center">
                {/* <div className="d-flex align-items-center">
                  <Show
                    set="light"
                    size="small"
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="font detail-text text-kalbe-darkGrey">{212}</p>
                </div> */}
                <div className="mx-0"></div>
                <div className="d-flex align-items-center">
                  <img
                    src="/images/like-icon.svg"
                    width={14}
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="tags-text text-kalbe-darkGrey m-0">
                    {data.totalLike}
                  </p>
                </div>
                <div className="mx-2"></div>
                <div className="d-flex align-items-center">
                  <Chat
                    set="light"
                    size="small"
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="font detail-text text-kalbe-darkGrey">
                    {data.totalComment}
                  </p>
                </div>
              </div>
            </div>
            <hr className="m-0" />
            <div className="px-3 pt-2">
              <Row noGutters>
                <CreateCategoryTag tagName={data.topikName && data.topikName} />
                <CreateCategoryTag
                  subTopik
                  tagName={data.subTopikName && data.subTopikName}
                />
              </Row>
            </div>
          </a>
        </Link>
      </div>
    </Col>
  );
};

export default IdeaCatalogCard;
