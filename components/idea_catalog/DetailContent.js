import DOMPurify from "isomorphic-dompurify";
import dynamic from "next/dynamic";
import Image from "next/image";
import Moment from "react-moment";
import Link from "next/link";
// const DOMPurify = dynamic(() => import("dompurify"), {
//   ssr: false,
// });

const DetailContent = ({ data }) => {
  if (!data) {
    return <p>Loading..</p>;
  }

  const component = decodeURI(data[0].description);
  // const sanitizer = dompurify.sanitize;
  // return <div dangerouslySetInnerHTML={{__html: sanitizer(namacomponent)}}

  return (
    <>
      <Link href={`/profile/${data[0].createdBy}/ideas`}>
        <a>
          <div className="mb-4">
            <div className="d-flex align-items-center">
              <img
                src="/images/profile-picture.jpg"
                className="mr-2 mb-0 border-radius-50"
                style={{
                  objectFit: "cover",
                  height: "2.75em",
                  width: "2.75em",
                }}
              />
              <div className="d-flex flex-column">
                <p className="font body-copy font-weight-bold">
                  {data[0].createdBy}
                </p>
                {/* <p className="font detail-text text-kalbe-darkGrey">
                LV.7 - Innovator
              </p> */}
              </div>
            </div>
          </div>
        </a>
      </Link>
      <div>
        <p className="font detail-text text-kalbe-darkGrey">
          {" "}
          <Moment format="DD MMM YYYY">{data.createdOn}</Moment>
        </p>
        <h1 className="font heading1 mb-3">{data[0].title}</h1>
        <div
          dangerouslySetInnerHTML={{
            __html: DOMPurify.sanitize(component, {
              ADD_TAGS: ["iframe"],
              ADD_ATTR: [
                "allow",
                "allowfullscreen",
                "frameborder",
                "scrolling",
              ],
            }),
          }}
          className="font body-copy pb-2"
          style={{ minHeight: 400 }}
        />
        <p className="font body-copy text-kalbe-darkGrey"></p>
        {data[0].ideaCatalogTags.map((tag) => {
          return (
            <p className="font body-copy text-kalbe-darkGrey" key={tag.tagId}>
              #{tag.tagName}
            </p>
          );
        })}
      </div>
    </>
  );
};
export default DetailContent;
