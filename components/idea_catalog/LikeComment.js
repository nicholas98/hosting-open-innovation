import Image from "next/image";
import { Chat, Show } from "react-iconly";
import { Col, Input, Row, Modal, ModalBody, ModalHeader } from "reactstrap";
import Moment from "react-moment";
import { useState, useEffect } from "react";
import React from "react";
import {
  getAllComments,
  submitComment,
  submitLike,
  deleteLike,
  editComment,
  deleteComment,
  getAllLikes,
} from "helper";
import { useRouter } from "next/router";
import Spinner from "components/shared/Spinner";
import Link from "next/link";

const Comments = ({ comments, getName, setState }) => {
  const [editId, setEditId] = useState("");
  const [resetValue, setResetValue] = useState(false);
  const [loading, setLoading] = useState(false);

  const router = useRouter();
  const refreshData = () => {
    router.replace(router.asPath);
  };

  const refreshDataEdit = () => {
    router.replace(router.asPath);
    setResetValue(true);
  };

  function editButton(comment) {
    setResetValue(false);
    setEditId(comment.id);
  }

  function editFunction(e) {
    const allData = {
      id: editId,
      commentData: {
        op: "replace",
        path: "/comment",
        value: e,
      },
    };
    setLoading(true);
    editComment(allData)
      .then((allData) => {
        if (!allData || allData.message) {
          alert(`token not found, refreshing the page`);
          router.reload();
          setLoading(false);
        } else {
          // setResetValue(true);
          refreshDataEdit();
          setLoading(false);
          setResetValue(true);
        }
        // setResetValue(false);
        // setLoading(false);
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
    setState(true);
  }

  function delCommentFunction(id) {
    if (confirm("are you sure to delete the comment? ")) {
      setLoading(true);

      deleteComment(id)
        .then((data) => {
          if (!data || data.message) {
            alert(`token not found, refreshing the page`);
            router.reload();
            setLoading(false);
          } else {
            alert(`delete succesful`);
            refreshData();
            setLoading(false);
          }
          // refreshData();
          // if (data.status < 300) {
          //   refreshData();
          // }
        })
        .catch((err) => {
          console.log(err);
        });
      setState(true);
    } else {
    }
  }

  if (loading) {
    return <Spinner />;
  }

  return (
    <React.Fragment>
      {comments &&
        comments.map((comment) => {
          return (
            <div className="d-flex align-items-start mb-3" key={comment.id}>
              <div>
                <div
                  className="image-container image-ratio-1by1"
                  style={{ width: "2.5em" }}
                >
                  <Image
                    src="/images/profile-picture.jpg"
                    layout="fill"
                    className="image-inside border-radius-50 "
                  />
                </div>
              </div>
              <div className="mx-2"></div>
              <div className="w-100 bg-kalbe-lightGrey border-radius-8 p-3">
              <Link href={`/profile/${comment.createdBy}/ideas`}>
                <a>
                  <div className="d-flex">
                    <p className="detail-text text-kalbe-darkGrey mb-0">
                      {comment.createdName}
                    </p>
                    <p className="detail-text text-kalbe-darkGrey mb-0 ml-auto">
                      <Moment format="DD MMM YYYY">{comment.createdOn}</Moment>
                    </p>
                  </div>
                </a>
              </Link>
                <p className="body-copy text-kalbe-black mb-0">
                  {comment.comment}
                </p>
                {getName === comment.createdBy && (
                  <React.Fragment>
                    <button
                      onClick={() => editButton(comment)}
                      className="detail-text text-kalbe-black font-weight-bold m-0"
                      style={{
                        border: "none",
                        background: "none",
                        color: "inherit",
                        padding: "0",
                        font: "inherit",
                        outline: "inherit",
                      }}
                    >
                      Edit
                    </button>
                    <button
                      onClick={() => delCommentFunction(comment.id)}
                      className="detail-text text-kalbe-black font-weight-bold m-0"
                      style={{
                        border: "none",
                        background: "none",
                        color: "inherit",
                        padding: "0",
                        font: "inherit",
                        outline: "inherit",
                        paddingLeft: "10px",
                      }}
                    >
                      Delete
                    </button>
                  </React.Fragment>
                )}

                {editId === comment.id && !resetValue && (
                  <Input
                    type="text"
                    name="name"
                    className="border-radius-8 height-44"
                    id="nameInput"
                    placeholder="Write a comment..."
                    onKeyPress={(e) => {
                      if (e.key === "Enter") {
                        editFunction(e.target.value);
                      }
                    }}
                  />
                )}
                {editId === comment.id && resetValue && (
                  <React.Fragment></React.Fragment>
                )}
              </div>
            </div>
          );
        })}

      {!comments && (
        <div className="d-flex align-items-start mb-3">
          <div></div>
          <div className="mx-2"></div>
          <div className="w-100 bg-kalbe-lightGrey border-radius-8 p-3">
            <p className="body-copy text-kalbe-black mb-0">
              there is no comment
            </p>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

const LikeComment = (props) => {
  const [showAll, setShowAll] = useState(false);
  const [commentsAll, setCommentsAll] = useState([]);
  const [like, setLike] = useState(false);
  const [stateRef, setStateRef] = useState(false);
  const [commentInput, setCommentInput] = useState("");
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [likeAll, setLikeAll] = useState([]);

  const toggle = () => {
    setModal(!modal);
  };

  useEffect(() => {
    if (modal) {
      getAllLikes(props.ideaId).then((data) => {
        setLikeAll(data);
      });
    }
  }, [modal]);

  const router = useRouter();

  const refreshData = () => {
    router.replace(router.asPath);
    setLoading(true);
  };

  useEffect(() => {
    if (showAll) {
      getAllComments(props.ideaId).then((data) => {
        setCommentsAll(data);
      });
      setStateRef(false);
    }
  }, [showAll, stateRef]);

  function toggleCommentsHandler() {
    if (props.countUnshowComment === 0) {
      return null;
    }
    setShowAll((prevStatus) => !prevStatus);
    // setShowAll(true);
  }

  function submitFunction(e) {
    const data = {
      comment: e,
      ideaCatalogId: props.ideaId,
    };
    setLoading(true);

    submitComment(data)
      .then((data) => {
        if (!data || data.message) {
          alert(`token not found, refreshing the page`);
          router.reload();
          setLoading(false);
        } else {
          refreshData();
          setLoading(false);
        }
        // refreshData();
        // setLoading(false);
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
    // Router.push(`/idea_catalog/detail/${props.ideaId}`);
    setStateRef(true);
  }

  function likeFunction(e) {
    const data = {
      ideaCatalogId: props.ideaId,
    };

    setLoading(true);

    submitLike(data)
      .then((data) => {
        if (!data || data.message) {
          alert(`token not found, refreshing the page`);
          router.reload();
          setLoading(false);
        } else {
          refreshData();
          setLoading(false);
        }
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
    // setLike(true);
  }

  function unLikeFunction(e) {
    // const data = {
    //   ideaCatalogId: props.ideaId,
    // };

    setLoading(true);

    deleteLike(props.likeIdea[0].id)
      .then((data) => {
        if (!data || data.message) {
          alert(`token not found, refreshing the page`);
          router.reload();
          setLoading(false);
        } else {
          refreshData();
          setLoading(false);
        }
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
    // setLike(false);
  }
  if (loading) {
    return <Spinner />;
  }

  return (
    <div>
      <div className="d-flex align-items-center">
        <div className="d-flex mt-2">
          <div className="d-flex align-items-center mr-2" onClick={toggle}>
            <Modal isOpen={modal} toggle={toggle}>
              <ModalHeader toggle={toggle}>
                People Who Like This Post
              </ModalHeader>
              {likeAll.map((like) => {
                return <ModalBody key={like.id}>{like.createdName}</ModalBody>;
              })}
            </Modal>
            <img
              src="/images/like-icon.svg"
              width={14}
              style={{ width: "25px" }}
              className="text-kalbe-darkGrey mr-1"
            />
            <p className="tags-text text-kalbe-darkGrey m-0">
              {props.totalLike}
            </p>
          </div>
          <div className="d-flex align-items-center">
            <Show
              set="light"
              size="medium"
              className="text-kalbe-darkGrey mr-1"
            />
            <p className="tags-text text-kalbe-darkGrey m-0">
              {props.totalView}
            </p>
          </div>
        </div>
        <div className="ml-auto">
          <p className="tags-text text-kalbe-darkGrey m-0">{`${props.totalComment} comments`}</p>
        </div>
      </div>
      <div className="border-top border-bottom py-2 my-3">
        <Row noGutters>
          <Col
            xs="6"
            className="d-flex justify-content-center align-items-center"
          >
            {props.likeIdea.length >= 1 && (
              <img
                src="/images/like-icon.svg"
                width={20}
                className="text-kalbe-black mr-1"
                style={{
                  transform: "rotateX(180deg)",
                  WebkitTransform: "rotateX(180deg)",
                }}
              />
            )}
            {props.likeIdea.length === 0 && (
              <img
                src="/images/like-icon.svg"
                width={20}
                className="text-kalbe-black mr-1"
              />
            )}

            {props.likeIdea.length >= 1 && (
              <button
                onClick={unLikeFunction}
                className="detail-text text-kalbe-black font-weight-bold m-0"
                style={{
                  border: "none",
                  background: "none",
                  color: "inherit",
                  padding: "0",
                  font: "inherit",
                  outline: "inherit",
                }}
              >
                Unlike
              </button>
            )}
            {props.likeIdea.length === 0 && (
              <button
                onClick={likeFunction}
                className="detail-text text-kalbe-black font-weight-bold m-0"
                style={{
                  border: "none",
                  background: "none",
                  color: "inherit",
                  padding: "0",
                  font: "inherit",
                  outline: "inherit",
                }}
              >
                Like
              </button>
            )}
          </Col>
          <Col
            xs="6"
            className="d-flex justify-content-center align-items-center"
          >
            <Chat set="light" width={20} className="text-kalbe-black mr-1" />
            <p className="detail-text text-kalbe-black font-weight-bold m-0">
              Comment
            </p>
          </Col>
        </Row>
      </div>
      <div>
        <button
          onClick={toggleCommentsHandler}
          className="detail-text text-kalbe-black font-weight-bold"
          style={{
            border: "none",
            background: "none",
            color: "inherit",
            padding: "0",
            font: "inherit",
            outline: "inherit",
          }}
        >
          {(props.totalComment === 0 && <p>no comments yet</p>) ||
            (props.totalComment >= 1 && props.totalComment <= 3 && (
              <p>All Comments</p>
            )) ||
            (props.totalComment >= 4 && showAll
              ? "All Comments"
              : `View ${props.countUnshowComment} more comments`)}
        </button>
        {!showAll && (
          <Comments
            comments={props.commentLast}
            getName={props.nameCurrentUser}
            setState={setStateRef}
          />
        )}

        {showAll && (
          <Comments
            comments={commentsAll}
            getName={props.nameCurrentUser}
            setState={setStateRef}
          />
        )}

        <div className="d-flex align-items-start mb-4">
          <div>
            <div
              className="image-container image-ratio-1by1"
              style={{ width: "2.5em" }}
            >
              <Image
                src="/images/profile-picture.jpg"
                layout="fill"
                className="image-inside border-radius-50"
              />
            </div>
          </div>
          <div className="mx-2"></div>
          <Input
            type="text"
            name="name"
            className="border-radius-8 height-44"
            id="nameInput"
            placeholder="Write a comment..."
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                submitFunction(e.target.value);
              }
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default LikeComment;
