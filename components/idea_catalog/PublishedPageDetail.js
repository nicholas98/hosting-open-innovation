import LikeComment from "components/idea_catalog/LikeComment";
import { Download } from "react-iconly";
import {
  Col, Row
} from "reactstrap";
import DetailContent from "./DetailContent";
import ProgressDetail from "./ProgressDetail";
import TeamMember from "./TeamMember";
import TeamMemberCard from "./TeamMemberCard";

const PublishedPageDetail = (props) => {
  const { CreateDownloadItem, CreateCategoryTag, teamMemberData } = props;
    return (
      <>
        <div className="">
          <Row className="match-height">
            <Col lg="8">
              <div className="mb-4">
                <div className="d-flex align-items-center">
                  <img
                    src="/images/profile-picture.jpg"
                    className="mr-2 mb-0 border-radius-50"
                    style={{
                      objectFit: "cover",
                      height: "2.75em",
                      width: "2.75em",
                    }}
                  />
                  <div className="d-flex flex-column">
                    <p className="body-copy text-kalbe-black font-weight-bold m-0">
                      Winda Halim
                    </p>
                    <p className="tags-text text-kalbe-darkGrey m-0 text-left">
                      LV.7 - Innovator
                    </p>
                  </div>
                </div>
              </div>
              <DetailContent /> 
            </Col>
            <Col lg="4">
              <div className="mb-2">
                <h3 className="heading3 text-kalbe-black">Topics</h3>
                <Row noGutters>
                  <CreateCategoryTag tagName="Product/Service" />
                  <CreateCategoryTag tagName="Chat bot" />
                </Row>
              </div>
              <div className="mb-4">
                <div className="d-flex align-items-center mb-2">
                  <h3 className="heading3 text-kalbe-black m-0">Attachment</h3>
                  <div className="d-flex ml-auto">
                    <Download set="curved" className="text-kalbe-black mr-2" />
                    <p className="body-copy font-weight-bold text-kalbe-black m-0">
                      Download all
                    </p>
                  </div>
                </div>
                <CreateDownloadItem fileName="Chatbot Explanation.pptx" />
                <CreateDownloadItem fileName="Video chat.mp4" />
                <CreateDownloadItem fileName="Gambar peraga.jpg" />
              </div>
              <div className="mb-4">
              {teamMemberData.map((data) => {
                return (
                  <TeamMember
                    state={data.state}
                    data={data}
                    key={data.TeamName}
                  />
                );
              })}
              </div>
              <div>
                <ProgressDetail condition={"published"} />
              </div>
            </Col>
          </Row>
          <Row>
            <Col lg="8">
              <LikeComment />
            </Col>
          </Row>
        </div>
      </>
    );
};

export default PublishedPageDetail;
