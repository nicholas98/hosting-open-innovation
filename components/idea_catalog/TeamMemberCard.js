import { Button } from "reactstrap";
import Link from "next/link";
import { useRouter } from "next/router";

const CreateMemberItem = ({ name, jobDesc, upn }) => (
  <Link href={`/profile/${upn}/ideas`}>
    <a>
      <div className="d-flex align-items-center mb-4">
        <img
          src="/images/profile-picture.jpg"
          className="mr-3 mb-0 border-radius-50"
          style={{
            objectFit: "cover",
            height: "2.75em",
            width: "2.75em",
          }}
        />
        <div className="d-flex flex-column">
          <p className="font body-copy font-weight-bold">{name}</p>
          <p className="font detail-text text-kalbe-darkGrey">{jobDesc}</p>
        </div>
      </div>
    </a>
  </Link>
);

const TeamMemberCard = ({
  otherUser,
  data,
  teamName,
  nameValid,
  dataNameAuthor,
  title,
}) => {
  if (!data) {
    return <p>Loading</p>;
  }
  const router = useRouter();

  function reqJoin() {
    const dataReq = {
      name: dataNameAuthor,
      title: title,
    };
    router.push({
      pathname: "/inbox/request_join",
      query: { data: JSON.stringify(dataReq) },
    });
  }

  function chatAuthor() {
    router.push({
      pathname: "/inbox/chat_author",
      query: { dataAuthor: JSON.stringify(dataNameAuthor) },
    });
  }

  return (
    <div className="card border-radius-12 pt-3 pb-1 px-3">
      <h3 className="heading3 text-kalbe-black mb-3">{`Team Member - ${teamName}`}</h3>
      {data.map((singleData) => {
        return (
          <CreateMemberItem
            upn={singleData.upn}
            name={singleData.name}
            jobDesc={singleData.jobTitle}
            key={singleData.id}
          />
        );
      })}

      {otherUser && (
        <>
          {!nameValid && (
            <Button
              className="button button-secondary button-small-full mb-3"
              onClick={() => reqJoin()}
            >
              Request to Join Team
            </Button>
          )}
          {!nameValid && (
            <Button
              className="button button-primary button-small-full"
              onClick={() => chatAuthor()}
            >
              Chat Author
            </Button>
          )}
        </>
      )}
    </div>
  );
};

export default TeamMemberCard;
