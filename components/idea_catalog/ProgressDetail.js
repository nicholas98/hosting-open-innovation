import { loadGetInitialProps } from "next/dist/next-server/lib/utils";
import { Button } from "reactstrap";

const handleOnClick = () => {
  alert("Sedang dalam tahap Development");
};

const ProgressDetail = (props) => {
  return (
    <>
      {props.condition === 1 || props.condition === 4 ? (
        <div className="card border-radius-12 mt-4">
          <div className="flex-row-between-center p-3">
            <div className="font body-copy">Idea Status</div>
            <div className="font detail-text card-badge">
              Verifying process...
            </div>
          </div>
          <hr className="m-0" />
          <div className="p-3">
            <p className="font body-copy">
              The verification process will take up to 2 working day. When
              verified, it will be shown publicly. In meantime, you can’t edit
              or delete this idea until the verification process are done.
            </p>
          </div>
        </div>
      ) : props.condition === 5 && props.getSameName ? (
        <Button
          className="button button-primary button-small-full mt-3"
          onClick={handleOnClick}
        >
          Submit to Konvensi Inovasi
        </Button>
      ) : props.condition === 3 ? (
        <div className="card border-radius-12 mt-4">
          <div className="flex-row-between-center p-3">
            <div className="font body-copy">Idea Status</div>
            <div className="font detail-text card-badge">Draft</div>
          </div>
          <hr className="m-0" />
        </div>
      ) : props.condition === 6 || props.condition === 7 ? (
        <div className="card border-radius-12 mt-4">
          <div className="flex-row-between-center p-3">
            <div className="font body-copy">Idea Status</div>

            {props.condition === 6 && (
              <div className="font detail-text card-badge">Rejected</div>
            )}
            {props.condition === 7 && (
              <div className="font detail-text card-badge">Revise</div>
            )}
          </div>
          <hr className="m-0" />
          <div className="p-3">
            <p className="font body-copy">{props.notes && props.notes}</p>
          </div>
        </div>
      ) : null}
      <div id="msgbox-area" className="msgbox-area"></div>
    </>
  );
};

export default ProgressDetail;
