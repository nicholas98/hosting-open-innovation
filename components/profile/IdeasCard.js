import {
  Row,
  Col,
  Badge,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Show, Chat } from "react-iconly";
import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import Moment from "react-moment";
import { FILE_PROFILEPICTURE } from "constant";
import { stripHtml } from "string-strip-html";
import { getImage } from "helper";

const CreateCategoryTag = ({ tagName, subTopik }) => (
  <Col xs="auto" className={`${!subTopik && "mr-2"} mb-2`}>
    <p className="border font detail-text text-center border-radius-8 p-2 truncate-one-line">
      {tagName}
    </p>
  </Col>
);

const IdeasCard = ({ currentState, data, profilePicture }) => {
  const [dropdownOpen, setOpen] = useState(false);

  const [image, setImage] = useState([]);

  const idImage =
    data.ideaCatalogThumbnails !== null ? data.ideaCatalogThumbnails.fileId : 0;

  useEffect(() => {
    return getImage(idImage).then((data) => {
      const urlImage = URL.createObjectURL(data);
      setImage(urlImage);
    });
  }, []);

  const toggleDropdown = () => setOpen(!dropdownOpen);
  return (
    <Col xl="4" lg="6" md="12">
      {/* <Link href={`/idea_catalog/detail/${data.id}`}>
        <a className="text-decoration-none"> */}
      <div className="card mb-4" style={{ borderRadius: "12px" }}>
        <div className="ideas-card-header">
          <div className="d-flex align-items-center">
            <Image
              src={
                profilePicture === null || profilePicture === ""
                  ? "/images/profile-picture.jpg"
                  : profilePicture.startsWith("/MDATA")
                  ? `${FILE_PROFILEPICTURE}${profilePicture}`
                  : profilePicture
              }
              width={32}
              height={32}
              className="card-header-photo"
            />
            <div className="mx-1"></div>
            <div className="d-flex flex-column">
              <p className="font detail-text font-weight-bold">
                {data.createdName}
                {/* {data.createdBy} */}
              </p>
              <p className="font sub-detail-text text-kalbe-darkGrey">
                <Moment format="DD MMM YYYY">{data.createdOn}</Moment>
              </p>
            </div>
          </div>
          {currentState === "0" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Inactive
            </Badge>
          )}
          {currentState === "1" && (
            <ButtonDropdown
              direction="left"
              isOpen={dropdownOpen}
              toggle={toggleDropdown}
              className="border-radius-50"
            >
              <DropdownToggle className="bg-kalbe-white border-kalbe-white m-0 p-0 border-radius-50 flex-row-center-center">
                <Image src="/images/more-icon.png" width={20} height={20} />
              </DropdownToggle>
              <DropdownMenu className="dropdown-card">
                <Link href={`/idea_catalog/detail/${data.id}`}>
                  <DropdownItem>Edit</DropdownItem>
                </Link>
              </DropdownMenu>
            </ButtonDropdown>
          )}
          {currentState === "2" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Deleted
            </Badge>
          )}
          {currentState === "3" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Draft
            </Badge>
          )}
          {currentState === "4" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Submitted
            </Badge>
          )}

          {currentState === "5" && (
            <ButtonDropdown
              direction="left"
              isOpen={dropdownOpen}
              toggle={toggleDropdown}
              className="border-radius-50"
            >
              <DropdownToggle className="bg-kalbe-white border-kalbe-white m-0 p-0 border-radius-50 flex-row-center-center">
                <Image src="/images/more-icon.png" width={20} height={20} />
              </DropdownToggle>
              <DropdownMenu className="dropdown-card">
                <Link href={`/idea_catalog/detail/${data.id}`}>
                  <DropdownItem>Edit</DropdownItem>
                </Link>
              </DropdownMenu>
            </ButtonDropdown>
          )}
          {currentState === "6" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Rejected
            </Badge>
          )}
          {currentState === "7" && (
            <Badge
              className="font detail-text card-badge"
              style={{
                width: "100%",
                maxWidth: 60,
                padding: "0.2rem 0.2rem",
                fontSize: "13px",
                color: "black",
                textAlign: "center",
              }}
            >
              Revise
            </Badge>
          )}
          {/* {currentState !== "5" ? (
                <Badge
                  className="font detail-text card-badge"
                  style={{
                    width: "100%",
                    maxWidth: 60,
                    padding: "0.2rem 0.2rem",
                    fontSize: "13px",
                    color:"black",
                  }}
                >
                  Process...
                </Badge>
              ) : (
                <ButtonDropdown
                  direction="left"
                  isOpen={dropdownOpen}
                  toggle={toggleDropdown}
                  className="border-radius-50"
                >
                  <DropdownToggle className="bg-kalbe-white border-kalbe-white m-0 p-0 border-radius-50 flex-row-center-center">
                    <Image src="/images/more-icon.png" width={20} height={20} />
                  </DropdownToggle>
                  <DropdownMenu className="dropdown-card">
                  <Link href={`/idea_catalog/detail/${data.id}`}>
                    <DropdownItem>Edit</DropdownItem>
                  </Link>
                  </DropdownMenu>
                </ButtonDropdown>
              )} */}
        </div>
        <Link href={`/idea_catalog/detail/${data.id}`}>
          <a className="text-decoration-none">
            <div className="ideas-card-image-container pt-0">
              <img
                src={image !== null ? image : "/images/profile-picture.jpg"}
                layout="fill"
                style={{
                  width: "100%",
                  maxHeight: "140px",
                  maxWidth: "1148px",
                }}
                className="object-fit-cover"
              />
            </div>

            <div className="p-3">
              <p className="font body-copy font-weight-bold truncate-one-line mb-1">
                {data.title}
              </p>
              <p className="font detail-text truncate-two-line">
                {/* {data.description} */}
                {/* {unescape(data.description)} */}
                {stripHtml(unescape(data.description)).result}
              </p>
              <div className="d-flex align-items-center">
                {/* <div className="d-flex align-items-center">
                  <Show
                    set="light"
                    size="small"
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="font detail-text text-kalbe-darkGrey">{212}</p>
                </div> */}
                <div className="mx-0"></div>
                <div className="d-flex align-items-center">
                  <img
                    src="/images/like-icon.svg"
                    width={14}
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="tags-text text-kalbe-darkGrey m-0">
                    {data.totalLike}
                  </p>
                </div>
                <div className="mx-2"></div>
                <div className="d-flex align-items-center">
                  <Chat
                    set="light"
                    size="small"
                    className="text-kalbe-darkGrey mr-1"
                  />
                  <p className="font detail-text text-kalbe-darkGrey">
                    {data.totalComment}
                  </p>
                </div>
              </div>
            </div>
            <hr className="m-0" />
            <div className="px-3 pt-2">
              <Row noGutters>
                <CreateCategoryTag tagName={data.topikName && data.topikName} />
                <CreateCategoryTag
                  subTopik
                  tagName={data.subTopikName && data.subTopikName}
                />
              </Row>
            </div>
            <hr className="m-0" />
            <div className="px-3 py-2">
              <Link href="#">
                <a>
                  <div className="d-flex align-items-center">
                    <Image src="/images/link-icon.png" width={12} height={12} />
                    <div className="mx-1"></div>
                    <p className="font detail-text truncate-one-line">
                      {data.title}
                    </p>
                  </div>
                </a>
              </Link>
            </div>
          </a>
        </Link>
      </div>
      {/*  </a>
      </Link> */}
    </Col>
  );
};

export default IdeasCard;
