import Link from "next/link";


const handleOnClick = () => {
  alert("Sedang dalam tahap Development");
};

const CreateNavItem = ({ href, navText, currentPosition, bottomBorder, otherUser, dataUPN }) => (
  <Link href={otherUser ? `/profile/[upn]/${href}` : `/profile/${href}`} as={otherUser && `/profile/${dataUPN && dataUPN}/${href}`} replace={true} scroll={false}>
    <a
      className={`font body-copy ${
        currentPosition === href
          && "current-profile-section"
      } py-2 px-4 ${bottomBorder !== 0 && `mb-${bottomBorder}`}`}
    >
      {navText}
    </a>
  </Link>
);

const CreateNavItemSementara = ({ href, navText, currentPosition, bottomBorder, otherUser, dataUPN, alert }) => (
  <a
    className={`font body-copy ${
      currentPosition === href
        && "current-profile-section"
    } py-2 px-4 ${bottomBorder !== 0 && `mb-${bottomBorder}`}`}
    onClick={handleOnClick}
  >
    {navText}
  </a>
);

const ProfileNavCard = ({ currentPosition, otherUser, dataUPN }) => (
  <div className="card" style={{ borderRadius: "12px" }}>
    <p
      className={`font body-copy text-kalbe-darkGrey font-weight-bold py-2 px-3`}
    >
      Menu
    </p>
    <hr className="mx-3 my-0 py-0" />
    <CreateNavItem otherUser={otherUser} dataUPN={dataUPN && dataUPN} href="ideas" navText="Ideas" currentPosition={currentPosition} bottomBorder={0} />
    <hr className="mx-3 my-0 py-0" />
    <CreateNavItemSementara otherUser={otherUser} dataUPN={dataUPN && dataUPN} href="konvensi_inovasi" navText="Konvensi Inovasi" currentPosition={currentPosition} bottomBorder={0} />
    <hr className="mx-3 my-0" />
    <CreateNavItem otherUser={otherUser} dataUPN={dataUPN && dataUPN} href="achievements" navText="Achievements" currentPosition={currentPosition} bottomBorder={0} />
    <hr className="mx-3 my-0" />
    <CreateNavItemSementara otherUser={otherUser} dataUPN={dataUPN && dataUPN} href="stats" navText="Stats" currentPosition={currentPosition} bottomBorder={2} />
  </div>
);

export default ProfileNavCard;
