import { Row, Col, Progress } from "reactstrap";
import Link from "next/link";
import Image from "next/image";

const LevelCard = ({ otherUser, userData }) => (
  <Link href={otherUser ? "#" : "/profile/level_details"}>
    <a className="text-decoration-none">
      <div className="card border-radius-12">
        <div className="my-2 mx-3">
          <div className="d-flex align-items-center">
            <Image src="/badge-rank/4-developer.png" width={36} height={36} />
            <div className="ml-2">
              <p className="font detail-text text-kalbe-darkGrey">
              {otherUser
                    ? `${userData && userData.name}'s level`
                    : "Your level"}
              </p>
              <p className="font body-copy font-weight-bold">
                LV.0 - The Innovator
              </p>
            </div>
          </div>
        </div>
        <hr className="m-0" />
        <div className="my-2 mx-3">
          <Row noGutters>
            <Col>
              <Progress
                value={0}
                min={0}
                max={30000}
                className="profile-progress-bar"
                barClassName="inside-progress-bar"
              />
            </Col>
            <Col>
              <p className="font body-copy text-kalbe-darkGrey ml-3">
                <span className="text-kalbe-orange font-weight-bold">
                  {`26,733`}
                </span>
                {`/${`30,000`}`}
              </p>
            </Col>
          </Row>
        </div>
      </div>
    </a>
  </Link>
);

export default LevelCard;
