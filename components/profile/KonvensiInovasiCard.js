import Link from "next/link";
import { useState } from "react";
import {
  Row,
  Col,
  Badge,
  ButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
} from "reactstrap";
import Image from "next/image";

const CreateCategoryTag = ({ tagName }) => (
  <Col xs="auto" className="mr-2 mb-2">
    <p className="border font detail-text text-center border-radius-8 p-2">
      {tagName}
    </p>
  </Col>
);

const KonvensiInovasiCard = ({ currentState }) => {
  const [dropdownOpen, setOpen] = useState(false);

  const toggleDropdown = () => setOpen(!dropdownOpen);

  return (
    <Col xl="4" lg="6" md="12">
      <div className="card mb-4 border-radius-12">
        <div className="konvensi-card-image-container">
          <Image
            src="https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80"
            layout="fill"
            className="konvensi-card-image-child"
          />
          {currentState !== "Finished" ? (
            <Badge className="font detail-text card-badge konvensi-badge-position">
              Process...
            </Badge>
          ) : (
            <ButtonDropdown
              direction="left"
              isOpen={dropdownOpen}
              toggle={toggleDropdown}
              className="border-radius-50"
              style={{
                position: "absolute",
                top: "0.75em",
                right: "0.75em",
                height: "2.5em",
                width: "2.5em",
              }}
            >
              <DropdownToggle className="bg-kalbe-white border-kalbe-white border-radius-50 m-0 p-0 flex-row-center-center">
                <Image src="/images/more-icon.png" width={20} height={20} />
              </DropdownToggle>
              <DropdownMenu className="dropdown-card">
                <DropdownItem>Action 1</DropdownItem>
                <DropdownItem>Action 2</DropdownItem>
                <DropdownItem>Action 3</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          )}
        </div>
        <div className="p-3">
          <p className="font body-copy font-weight-bold truncate-one-line mb-1">
            Kalbe Chatbot untuk mempercepat response time
          </p>
          <p className="font detail-text truncate-two-line">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
            facilisis purus a lacus suscipit, ut tempor ex molestie.
          </p>
        </div>
        <hr className="m-0" />
        <div className="px-3 py-2">
          <Row>
            <Col xs="6" className="border-right pr-0">
              <p className="font detail-text text-kalbe-darkGrey">Theme</p>
              <p className="font detail-text">Revenue Growth</p>
            </Col>
            <Col xs="6">
              <p className="font detail-text text-kalbe-darkGrey">
                Project Leader
              </p>
              <p className="font detail-text">Arya Mukti</p>
            </Col>
          </Row>
        </div>
        <hr className="m-0" />
        <div className="px-3 pt-2">
          <Row noGutters>
            <CreateCategoryTag tagName="Product/Service" />
            <CreateCategoryTag tagName="Chat Bot" />
          </Row>
        </div>
        <hr className="m-0" />
        <div className="px-3 py-2">
          <Link href="#">
            <a>
              <div className="d-flex align-items-center">
                <Image src="/images/link-icon.png" width={12} height={12} />
                <div className="mx-1"></div>
                <p className="font detail-text truncate-one-line">
                  Kalbe Chatbot sebagai alat bantu customer
                </p>
              </div>
            </a>
          </Link>
        </div>
      </div>
    </Col>
  );
};

export default KonvensiInovasiCard;
