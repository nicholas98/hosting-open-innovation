import { Row, Col, Button } from "reactstrap";
import LevelCard from "./LevelCard";
import ProfileNavTab from "./ProfileNavTab";
import Link from "next/link";
import Image from "next/image";
import { useCookies } from "react-cookie";
import { useRouter } from "next/router";
import { FILE_PROFILEPICTURE } from "constant";
const ProfileCard = ({ width, currentPosition, otherUser, userData, profilePicture }) => {
  const [cookies, setCookie, removeCookie] = useCookies();
  const router = useRouter();
  function editProfiles() {
    router.push({
      pathname: "/profile/edit_profile",
    });
  }

  function chatAuthor() {
    router.push({
      pathname: "/inbox/chat_author",
      query: { dataAuthor: JSON.stringify(userData.email.toLowerCase()) },
    });
  }
  return (
    <div className={`${width >= 576 ? "card border-radius-12" : "shadow"}`}>
      <div
        className="w-100 bg-profile-gradient"
        style={{
          position: "relative",
          paddingTop: "35%",
          borderRadius: `${width >= 576 ? "12px 12px 0 0" : "0"}`,
        }}
      ></div>
      <div className="card-body p-3">
        <div className="d-flex justify-content-between">
          <div className="profile-photo-container mb-0">
            <Image
              src={profilePicture === null || profilePicture === "" ? "/images/profile-picture.jpg" : (profilePicture.startsWith("/MDATA") === true ? `${FILE_PROFILEPICTURE}${profilePicture}` : profilePicture)}
              width={72}
              height={72}
              className="profile-photo-inside"
            />
          </div>
          <div>
            {/* <Link
             href={otherUser ? "/inbox/new_message" : "/profile/edit_profile"}  >*/}
           
              <Button className="button button-secondary button-small p-0" onClick={otherUser? () => chatAuthor() : () => editProfiles()}>
                {otherUser ? "Chat" : "Edit Profile"}
              </Button>
            {/* </Link> */}
          </div>
        </div>
        <h3 className="font heading3">
        {otherUser
            ? userData && userData.name
            : (!cookies.Data && `Hi, cannot get name`) ||
              (cookies && `${cookies.Data.name}`)}
        </h3>
        <p className="font body-copy text-kalbe-darkGrey truncate-one-line">
        {otherUser
            ? userData && userData.jobTtlName
            : (!cookies.Data && `Hi, cannot get bu`) ||
              (cookies && `${cookies.Data.jobTitle}`)}
        </p>
        <p className="font body-copy text-kalbe-darkGrey mb-3 truncate-one-line">
        {otherUser
            ? userData && userData.email.toLowerCase()
            : (!cookies.Data && `Hi, cannot get upn`) ||
              (cookies && `${cookies.Data.upn.toLowerCase()}`)}
        </p>
        <LevelCard otherUser={otherUser} userData={userData} />
      </div>
      {width >= 576 ? (
        <></>
      ) : (
        <ProfileNavTab currentPosition={currentPosition} />
      )}
    </div>
  );
};

export default ProfileCard;
