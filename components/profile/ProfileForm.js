import { Form, FormGroup, Label, Input, FormText, Button } from "reactstrap";
import { useState } from "react";
import { Camera, ChevronDown } from "react-iconly";
import { useCookies } from "react-cookie";
import Router from "next/router";
import { useFormik } from "formik";
import * as yup from "yup";
import { onEditProfile } from "helper";
import { FILE_PROFILEPICTURE } from "constant";

const schema = yup.object().shape({
  content: yup.string(),
});

const ProfileForm = ({ userData, profilePicture }) => {
  const [cookies, setCookie, removeCookie] = useCookies();
  const [userMessage, setUserMessage] = useState("");
  const [avatar, setAvatar] = useState(
    profilePicture !== "" ? profilePicture : "/images/profile-picture.jpg"
  );

  const formik = useFormik({
    initialValues: {
      Content: "",
    },
    validationSchema: schema,
    onSubmit: () => submitProfile(avatar),
  });

  const changePicture = (event) => {
    const reader = new FileReader();
    const files = event.target.files;

    reader.onload = () => {
      setAvatar(reader.result);
    };

    reader.readAsDataURL(files[0]);
  };

  const submitProfile = () => {
    const avatarString = avatar.substring(23);

    const pictureData = {
      Content: avatarString,
    };

    // if (userData) {
    onEditProfile(pictureData)
      .then((data) => {
        if (data !== null) {
          setCookie("profilePicturePath", data.profilePicturePath);
          Router.push("/profile/ideas");
        } else {
          setUserMessage("Edit profile error, please try again!");
        }
      })
      .catch((err) => console.log(err));
    // }
  };

  return (
    <Form onSubmit={formik.handleSubmit}>
      <FormGroup>
        <Label className="label" for="profilePicture">
          Profile Picture
        </Label>
        <div className="form-image-container">
          <div className="form-image-background"></div>
          <img src={avatar} className="profile-form-image-child" />
          <Button tag={Label} className="image-input-button">
            <Camera set="light" className="font" />
            <Input
              hidden
              onChange={changePicture}
              type="file"
              name="avatar"
              id="profilePicture"
              accept="image/*"
            />
          </Button>
        </div>
        <FormText className="sublabel">Use 1:1 for best size</FormText>
      </FormGroup>
      <FormGroup>
        <Label className="label" for="nameInput">
          Name
        </Label>
        <Input
          type="text"
          name="name"
          className="input"
          id="nameInput"
          disabled
          value={cookies.Data?.name}
          onChange={() => {}}
        />
      </FormGroup>
      <FormGroup>
        <Label className="label" for="emailInput">
          Email
        </Label>
        <Input
          type="email"
          name="email"
          className="input"
          disabled
          id="emailInput"
          value={cookies.Data?.username}
          onChange={() => {}}
        />
      </FormGroup>
      <FormGroup>
        <Label className="label" for="phoneInput">
          Phone Number
        </Label>
        <Input
          type="text"
          name="phone"
          className="input"
          disabled
          id="phoneInput"
          value={cookies.Data?.mobileNo}
          onChange={() => {}}
        />
      </FormGroup>
      {/* <FormGroup>
        <Label className="label" for="postSetting">
          Post Setting
        </Label>
        <Input
          disabled
          type="select"
          name="select"
          className="input"
          id="postSetting"
        >
          <ChevronDown set="light" />
          <option>Everyone can view my post</option>
          <option>Only friends can view my post</option>
          <option>Hide my post from everyone</option>
        </Input>
      </FormGroup> */}
      <Button
        type="submit"
        className="button button-primary button-large-full mt-3"
      >
        Save Changes
      </Button>
      {userMessage && (
        <Alert color="danger" className="mt-3">
          {userMessage}
        </Alert>
      )}
    </Form>
  );
};

export default ProfileForm;
