import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import { useFormik } from "formik";
import * as yup from "yup";
import { useEffect } from 'react';
import { getTopicwithSubtopic, submitHastag, submitSubTopic, submitTopic } from 'helper';
import { useRouter } from 'next/router';

const schema = yup.object().shape({
    name: yup.string().required("Name Is Required"),
});

export default function DialogAddNewItem(props){
    const { open, handleClose, dialogName, data } = props;
    const router = useRouter()
    const formik = useFormik({
      initialValues:{
        name: '',
      },
      validationSchema: schema,
      onSubmit: (value)=>handleOnSubmit(value),
    })
    useEffect(()=>{
      if(open !== true){
        formik.values.name = ''
      }
    },[open, data])
    const handleOnSubmit = (value) => {
      if(dialogName === "Topic"){
        submitTopic(value)
        .then((data) => {
              if (data.status === 404) {
                // setWrongUser(true);
              } else {
                handleClose()
                router.replace('/idea_catalog/submit_idea')
              }
            })
            .catch((err) => {
              console.log(err);
              // setWrongUser(true);
            });
      } else if(dialogName === "Sub Topic"){
        const submitSubTopicValue = {
          topikId: parseInt(data.topic),
          ...value
        }
         submitSubTopic(submitSubTopicValue)
        .then((data) => {
              if (data.status === 404) {
                // setWrongUser(true);
              } else {
                handleClose()
                router.replace('/idea_catalog/submit_idea')
              }
            })
            .catch((err) => {
              console.log(err);
              // setWrongUser(true);
            });
      } else if(dialogName === "Hastag"){
         submitHastag(value)
        .then((data) => {
              if (data.status === 404) {
                // setWrongUser(true);
              } else {
                handleClose()
                router.replace('/idea_catalog/submit_idea')
              }
            })
            .catch((err) => {
              console.log(err);
              // setWrongUser(true);
            });
      }
    }
    return(
        <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{`Add New ${dialogName}`}</DialogTitle>
        <DialogContent>
            <Form onSubmit={formik.handleSubmit}>
                <FormGroup>
                <Label className="label">
                    {`Name ${dialogName}`}
                </Label>
                <Input
                    type="text"
                    name="name"
                    className="input"
                    id="name"
                    onChange={formik.handleChange}
                    value={formik.values.name}
                />
                </FormGroup>
                <Button className="button" type={"submit"}>
                    Submit
                </Button>
                <Button className="button" onClick={handleClose}>
                    Cancel
                </Button>
            </Form>
        </DialogContent>
      </Dialog>
    )
}