import SunEditor, { buttonList } from "suneditor-react";

import * as yup from "yup";
import Router from 'next/router';

export default function SunEditors(props) {
  return (
    <SunEditor
        onBlur={props.handleBlur}
        value={props.value}
        name={props.name}
        id={props.id}
        setOptions={{
            buttonList: buttonList.complex,
            videoHeight: '400px',
        }}
        {...props}
    />
  );
}

