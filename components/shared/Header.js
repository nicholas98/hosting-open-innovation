// import {
//   DropdownItem,
//   DropdownMenu,
//   DropdownToggle,
//   Progress,
//   Dropdown,
// } from "reactstrap";
// import React, { useContext, useEffect, useState } from "react";
// import Link from "next/link";
// import { Notification, ChevronDown } from "react-iconly";
// import Image from "next/image";
// import NotificationBadge from "react-notification-badge";
// import { Stores } from "store";
// import { useCookies } from "react-cookie";
// //connect menu header
// import { getMenuWithSubMenu } from "helper";
// import Spinner from "components/shared/Spinner";
// //....
// const CreateNavItem = ({
//   vuexySkin,
//   href,
//   navText,
//   currentPosition,
//   last,
//   withNotif,
// }) => (
//   <a
//     href={`${href}`}
//     className={`font body-copy ${
//       currentPosition === href &&
//       `$mary"  "text-kalbe-green"} $ght-boder" : "font-weight-bold"}`
//     } $ "py-1} ${!last && vuexySkin ? "mr-2" : "mr-4"}`}
//   >
//     {withNotif && (
//       <NotificationBadge
//         count={4}
//         effect={[null]}
//         containerStyle={{ left: "1.625em" }}
//         style={
//           vuexySkin
//             ? { top: "-1em", border: "3px solid #ffffff" }
//             : { top: "-0.75em", border: "3px solid #ffffff" }
//         }
//       />
//     )}
//     {navText}
//   </a>
// );

// const CreateDropdownItem = ({ vuexySkin, logout, href, itemName }) => (
//   <>
//     <DropdownItem
//       divider
//       className={`my-0 $"mx-3"`}
//     ></DropdownItem>
//     <Link href={href}>
//       <DropdownItem
//         className={`$ropdow-item-vuexySkin" : "header-dropdown-item"} w-100 ${
//           logout &&
//           `dropdown-logout-item ${
//             vuexySkin ? "font-weight-bolder" : "font-weight-bold"
//           }`
//         }`}
//       >
//         {itemName}
//       </DropdownItem>
//     </Link>
//   </>
// );

// const Header = ({ width, currentPosition, vuexySkin }) => {
//   const { state, dispatch, actions } = useContext(Stores);
//   const [isKonvensiOpen, setKonvensiOpen] = useState(false);
//   const [isProfileOpen, setProfileOpen] = useState(false);
//   const [isNotifOpen, setNotifOpen] = useState(false);
//   const [cookies, setCookie, removeCookie] = useCookies();
//   const toggleKonvensi = () => setKonvensiOpen(!isKonvensiOpen);
//   const onHoverKonvensi = () => setKonvensiOpen(true);
//   const onLeaveKonvensi = () => setKonvensiOpen(false);

//   const toggleProfile = () => setProfileOpen(!isProfileOpen);
//   const onHoverProfile = () => setProfileOpen(true);
//   const onLeaveProfile = () => setProfileOpen(false);

//   const toggleNotif = () => setNotifOpen(!isNotifOpen);

//   //connect menu header
//   const [menuData, setMenuData] = useState(null);
//   const [loading, setLoading] = useState(true);
//   useEffect(() => {
//     getMenuWithSubMenu()
//       .then((data) => {
//         setMenuData(data);
//         setLoading(false);
//       })
//       .catch((err) => {
//         console.log(err);
//       });
//   }, []);

//   if (loading) {
//     return <Spinner />;
//   }
//   const errorMessage = menuData.message;
//   //.....

//   return (
//     <div className={`bg-kalbe-white border-bottom $"py-3"`}>
//       <div className="container flex-row-between-center">
//         <div className="d-flex align-items-center">
//           {(errorMessage && <div>error</div>) ||
//               (menuData &&
//                 menuData.map((data) => {
//                   if (data.subMenus.length){
//                     return <Dropdown
//                     isOpen={isKonvensiOpen}
//                     toggle={toggleKonvensi}
//                     onMouseOver={onHoverKonvensi}
//                     onMouseLeave={onLeaveKonvensi}
//                     direction="down"
//                     className="mr-4"
//                     >
//                       <DropdownToggle
//                         color={vuexySkin && "flat-dark"}
//                         className={`font body-copy ${
//                           currentPosition === "/konvensi_inovasi" &&
//                           `text-kalbe-green ${
//                             vuexySkin ? "font-weight-bolder" : "font-weight-bold"
//                           }`
//                         } text-decoration-none bg-kalbe-white border-kalbe-white px-0 ${
//                           vuexySkin ? "py-25" : "py-1"
//                         }`}
//                       >
//                         {data.name}
//                         <ChevronDown
//                           set="light"
//                           size="small"
//                           className= "ml-1}
//                         />
//                       </DropdownToggle>
//                       <DropdownMenu className="dropdown-card header-dropdown-card">
//                         <DropdownItem
//                           disabled
//                           className=-1" : py-2 px-3"}
//                         >
//                           {data.name}
//                         </DropdownItem>
//                         {data.subMenus &&
//                           data.subMenus.map((subMenu) => (
//                             <CreateDropdownItem
//                               vuexySkin={vuexySkin}
//                               href={subMenu.link}
//                               itemName={subMenu.name}
//                             />
//                           ))}
//                       </DropdownMenu>
//                     </Dropdown>
//                   }
//                 else{
//                   return <CreateNavItem
//                   vuexySkin={vuexySkin}
//                   href={data.link}
//                   navText={data.name}
//                   currentPosition={currentPosition}
//                 />
//                 }
//                 }))}
//         </div>
//         <div className="d-flex align-items-center">
//           <Dropdown
//             isOpen={isProfileOpen}
//             toggle={toggleProfile}
//             onMouseOver={onHoverProfile}
//             onMouseLeave={onLeaveProfile}
//             direction="down"
//             className="mr-3"
//           >
//             <DropdownToggle
//               color={vuexySkin && "flat-dark"}
//               className="bg-kalbe-white border-kalbe-white d-flex align-items-center p-0"
//             >
//               <div className="image-container">
//                 <Image
//                   src="/images/profile-picture.jpg"
//                   width={36}
//                   height={36}
//                   className="image-inside border-radius-50"
//                 />
//               </div>
//               <div className= "mx-1}></div>
//               <p
//                 className={`font body-copy ${
//                   vuexySkin ? "font-weight-bolder" : "font-weight-bold"
//                 }`}
//               >
//                 {(!cookies.Data && `Hi, cannot get name`) ||
//                   (cookies && `Hi, ${cookies.Data.name}`)}
//               </p>
//             </DropdownToggle>
//             <DropdownMenu className="dropdown-card header-dropdown-card">
//               <DropdownItem
//                 disabled
//                 className=-1" : py-2 px-3"}
//               >
//                 Profile
//               </DropdownItem>
//               <CreateDropdownItem
//                 vuexySkin={vuexySkin}
//                 href="/profile/ideas"
//                 itemName="Ideas"
//               />
//               <CreateDropdownItem
//                 vuexySkin={vuexySkin}
//                 href="/profile/konvensi_inovasi"
//                 itemName="Konvensi Inovasi"
//               />
//               <CreateDropdownItem
//                 vuexySkin={vuexySkin}
//                 href="/profile/achievements"
//                 itemName="Achievements"
//               />
//               <CreateDropdownItem
//                 vuexySkin={vuexySkin}
//                 href="/profile/stats"
//                 itemName="Stats"
//               />
//               <CreateDropdownItem
//                 vuexySkin={vuexySkin}
//                 logout
//                 href="#"
//                 itemName="Logout"
//               />
//             </DropdownMenu>
//           </Dropdown>
//           <div
//             className={`d-flex align-items-center border-left border-right ${
//               vuexySkin ? "px-1" : "px-3"
//             }`}
//           >
//             <Image src="/badge-rank/4-developer.png" width={30} height={30} />
//             <div className= "ml-2}>
//               <div className={`d-flex $ "mb-1}`}>
//                 <p className={`font detail-text $"mr-4"`}>
//                   LV.4 - Developer
//                 </p>
//                 <p className="font detail-text text-kalbe-darkGrey">
//                   <span
//                     className={`text-kalbe-orange ${
//                       vuexySkin ? "font-weight-bolder" : "font-weight-bold"
//                     }`}
//                   >{`26,733`}</span>
//                   {`/${"30,000"}`}
//                 </p>
//               </div>
//               <Progress
//                 value={26733}
//                 min={0}
//                 max={30000}
//                 className="header-progress-bar"
//                 barClassName="inside-progress-bar"
//               />
//             </div>
//           </div>
//           <div className="ml-3">
//             <Dropdown
//               isOpen={isNotifOpen}
//               toggle={toggleNotif}
//               direction="down"
//               className={`bg-kalbe-white $"mr-4"`}
//             >
//               <DropdownToggle
//                 color={vuexySkin && "flat-dark"}
//                 className={`bg-kalbe-white border-kalbe-white px-0 ${
//                   vuexySkin ? "py-25" : "py-1"
//                 }`}
//               >
//                 <NotificationBadge
//                   count={12}
//                   effect={[null]}
//                   containerStyle={
//                     vuexySkin ? { left: "1.125em" } : { left: "0.875em" }
//                   }
//                   style={
//                     vuexySkin
//                       ? { top: "-1em", border: "3px solid #ffffff" }
//                       : { top: "-0.75em", border: "3px solid #ffffff" }
//                   }
//                 />
//                 <Notification
//                   set={isNotifOpen ? "bold" : "light"}
//                   className="font"
//                 />
//               </DropdownToggle>
//               <DropdownMenu
//                 right
//                 className={`shadow border-kalbe-white border-radius-12 ${
//                   vuexySkin ? "mt-25 pb-50" : "mt-1 pb-2"
//                 }`}
//               >
//                 <DropdownItem
//                   header
//                   className={`font app-header ${
//                     vuexySkin ? "py-50 px-1" : "py-2 px-3"
//                   }`}
//                 >
//                   Notification
//                 </DropdownItem>
//               </DropdownMenu>
//             </Dropdown>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Header;

// HEADER HARDCODE SEMENTARA
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Dropdown,
} from "reactstrap";
import React, { useContext, useState } from "react";
import Link from "next/link";
import {
  Notification,
  ChevronDown,
  Chat,
  People,
  TickSquare,
  CloseSquare,
  Message,
  Danger,
} from "react-iconly";
import Image from "next/image";
import NotificationBadge from "react-notification-badge";
import { useCookies } from "react-cookie";
import {
  API_URL,
  API_MENUWITHSUBMENUS,
  API_KEY,
  FILE_PROFILEPICTURE,
  API_NOTIFICATIONS,
} from "constant";
import { onReadNotif } from "helper";
import { ThumbsUp } from "react-feather";
import router from "next/router";
import Truncate from "react-truncate";

const CreateNavItem = ({
  vuexySkin,
  href,
  navText,
  currentPosition,
  last,
  withNotif,
}) => (
  <Link href={href}>
    <a
      className={`font body-copy ${
        currentPosition === href
          ? `${!vuexySkin && "text-kalbe-green"} font-weight-bold`
          : `${vuexySkin && "text-kalbe-darkGrey"}`
      }
    } ${!last && "mr-4"}`}
    >
      {withNotif && (
        <NotificationBadge
          count={4}
          effect={[null]}
          containerStyle={{ left: "1.625em" }}
          style={{ top: "-0.75em", border: "3px solid #ffffff" }}
        />
      )}
      {navText}
    </a>
  </Link>
);

const CreateDropdownItem = ({ logout, href, itemName }) => {
  const [cookies, setCookie, removeCookie] = useCookies();

  const logoutFunction = () => {
    if (typeof window !== "undefined") {
      window.localStorage.removeItem("accessToken");
      window.localStorage.clear();
      removeCookie("Data");
      removeCookie("profilePicturePath")
      router.push("/login");
    }
  };

  return (
    <>
      <DropdownItem divider className={`my-0 mx-3`}></DropdownItem>
      <Link href={href}>
        <a className="text-decoration-none">
          <DropdownItem
            className={`header-dropdown-item w-100 ${
              logout && "dropdown-logout-item font-weight-bold"
            }`}
            onClick={logout && logoutFunction}
          >
            {itemName}
          </DropdownItem>
        </a>
      </Link>
    </>
  );
};

const CreateDropdownItemSementara = ({ logout, href, itemName }) => {
  const handleOnClick = () => {
    alert("Sedang dalam tahap Development");
  };
  return (
    <>
      <DropdownItem divider className={`my-0 mx-3`}></DropdownItem>
        <a className="text-decoration-none">
          <DropdownItem
            className={`header-dropdown-item w-100`}
            onClick={handleOnClick}
          >
            {itemName}
          </DropdownItem>
        </a>
    </>
  );
};

const CreateNotifItem = ({ last, dataNotif }) => {
  const IconType = () =>
    dataNotif.type === "Comment" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-orange">
        <Chat set="light" size="small" className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "RequestToJoin" ? (
      <div className="flex-row-center-center bg-kalbe-purple">
        <People set="curved" size="small" className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "ApprovedToJoin" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-green">
        <TickSquare set="curved" size="small" className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "Like" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-fuchsia">
        <ThumbsUp size={16} className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "Inbox" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-darkGrey">
        <Message set="curved" size="small" className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "IdeaCatalogAccept" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-green">
        <TickSquare set="curved" size="small" className="text-kalbe-white" />
      </div>
    ) : dataNotif.type === "IdeaCatalogRevise" ? (
      <div className="flex-row-center-center notif-type-badge bg-kalbe-orange">
        <Danger set="curved" size="small" className="text-kalbe-white" />
      </div>
    ) : (
      dataNotif.type === "IdeaCatalogReject" && (
        <div className="flex-row-center-center notif-type-badge bg-kalbe-red">
          <CloseSquare set="curved" size="small" className="text-kalbe-white" />
        </div>
      )
    );

  const ImageType = () => (
    <div
      className="flex-row-center-center ml-2 mr-3"
      style={{ width: "3em", height: "3em" }}
    >
      <Image
        src={
          dataNotif.creatorProfilePicture !== ""
            ? dataNotif.creatorProfilePicture
            : "/images/profile-picture.jpg"
        }
        width={36}
        height={36}
        className="image-inside border-radius-50"
      />
      <IconType />
    </div>
  );

  const TimeDifference = () => {
    var notifTime = new Date(dataNotif.createdOn);
    var currentTime = new Date();

    var timeDiff = currentTime - notifTime;

    var minutesDiff = timeDiff / (1000 * 60);
    var hoursDiff = timeDiff / (1000 * 3600);
    var daysDiff = timeDiff / (1000 * 3600 * 24);
    var weeksDiff = timeDiff / (1000 * 3600 * 24 * 7);

    return minutesDiff < 60 ? (
      <p className="font detail-text text-right text-kalbe-darkGrey w-100">
        {parseInt(minutesDiff)}m
      </p>
    ) : hoursDiff < 24 ? (
      <p className="font detail-text text-right text-kalbe-darkGrey w-100">
        {parseInt(hoursDiff)}h
      </p>
    ) : daysDiff < 7 ? (
      <p className="font detail-text text-right text-kalbe-darkGrey w-100">
        {parseInt(daysDiff)}d
      </p>
    ) : (
      <p className="font detail-text text-right text-kalbe-darkGrey w-100">
        {parseInt(weeksDiff)}w
      </p>
    );
  };

  return (
    <Link href={dataNotif.link}>
      <a
        className="text-decoration-none"
        onClick={() => onReadNotif(dataNotif.id)}
      >
        <DropdownItem
          className={`d-flex align-items-center m-0 p-0 ${
            !dataNotif.isRead && "notif-item-unread"
          }`}
        >
          <ImageType />
          <div className={`d-flex ${!last && "border-bottom"}`}>
            <div className="font detail-text my-3" style={{ width: "15.4em", height: "3em" }}>
              <Truncate lines={2} ellipsis={<span>...</span>}>
                {/* <b>{dataNotif.message.substring(0, dataNotif.createdName.length)}</b>
              {dataNotif.message.substring(dataNotif.createdName && dataNotif.createdName.length)} */}
                {dataNotif.message}
              </Truncate>
            </div>
            <div className="ml-2 mt-3 pr-3" style={{ width: "3.4em" }}>
              <TimeDifference />
            </div>
          </div>
        </DropdownItem>
      </a>
    </Link>
  );
};

const Header = ({
  width,
  currentPosition,
  vuexySkin,
  dataNotif,
  notifCount,
  menuSubMenu,
  profilePicture,
}) => {
  const [cookies, setCookie, removeCookie] = useCookies();

  const [isKonvensiOpen, setKonvensiOpen] = useState(false);
  const toggleKonvensi = () => setKonvensiOpen(!isKonvensiOpen);
  const onHoverKonvensi = () => setKonvensiOpen(true);
  const onLeaveKonvensi = () => setKonvensiOpen(false);

  const [isAdminOpen, setAdminOpen] = useState(false);
  const toggleAdmin = () => setAdminOpen(!isAdminOpen);
  const onHoverAdmin = () => setAdminOpen(true);
  const onLeaveAdmin = () => setAdminOpen(false);

  const [isProfileOpen, setProfileOpen] = useState(false);
  const toggleProfile = () => setProfileOpen(!isProfileOpen);
  const onHoverProfile = () => setProfileOpen(true);
  const onLeaveProfile = () => setProfileOpen(false);

  const [isNotifOpen, setNotifOpen] = useState(false);
  const toggleNotif = () => setNotifOpen(!isNotifOpen);

  return (
    <div className="bg-kalbe-white border-bottom py-3">
      <div
        className={`${
          vuexySkin ? "container-vuexy" : "container"
        } flex-row-between-center`}
      >
        <div className="d-flex align-items-center">
          {/* {(!MenuSubMenus && `Sorry, cannot get menu`) ||
            (cookies && MenuSubMenus && MenuSubMenus.map((data)=>{ */}
          {menuSubMenu &&
            menuSubMenu[0].map((data) => {
              if (data.subMenus.length) {
                return (
                  <Dropdown
                    isOpen={
                      data.name === "Admin" ? isAdminOpen : isKonvensiOpen
                    }
                    toggle={
                      data.name === "Admin" ? toggleAdmin : toggleKonvensi
                    }
                    onMouseOver={
                      data.name === "Admin" ? onHoverAdmin : onHoverKonvensi
                    }
                    onMouseLeave={
                      data.name === "Admin" ? onLeaveAdmin : onLeaveKonvensi
                    }
                    direction="down"
                    className="mr-4 py-2"
                  >
                    <DropdownToggle
                      color={vuexySkin && "flat-dark"}
                      className={`font body-copy ${
                        data.link === currentPosition
                          ? `${
                              !vuexySkin && "text-kalbe-green"
                            } font-weight-bold`
                          : `${vuexySkin && "text-kalbe-darkGrey"}`
                        // : `${vuexySkin && "text-kalbe-darkGrey"}`
                      } text-decoration-none bg-kalbe-white border-kalbe-white px-0 py-1
                    `}
                    >
                      {data.name}
                      <ChevronDown set="light" size="small" className="ml-1" />
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-card header-dropdown-card">
                      <DropdownItem disabled className="py-2 px-3">
                        {data.name}
                      </DropdownItem>
                      {data.subMenus &&
                        data.subMenus.map((subMenu) => (
                          <CreateDropdownItem
                            vuexySkin={vuexySkin}
                            href={subMenu.link}
                            itemName={subMenu.name}
                          />
                        ))}
                    </DropdownMenu>
                  </Dropdown>
                );
              } else {
                return (
                  <CreateNavItem
                    vuexySkin={vuexySkin}
                    href={data.link}
                    navText={data.name}
                    currentPosition={currentPosition}
                  />
                );
              }
            })}
          {/* }))} */}
        </div>
        <div className="d-flex align-items-center">
          <Dropdown
            isOpen={isProfileOpen}
            toggle={toggleProfile}
            onMouseOver={onHoverProfile}
            onMouseLeave={onLeaveProfile}
            direction="down"
            className="mr-3 py-2"
          >
            <DropdownToggle className="bg-kalbe-white border-kalbe-white d-flex align-items-center p-0">
              <div className="image-container">
                <Image
                  src={
                    profilePicture !== ""
                      ? profilePicture
                      : "/images/profile-picture.jpg"
                  }
                  width={36}
                  height={36}
                  className="image-inside border-radius-50"
                />
              </div>
              <div className="mx-1"></div>
              <p className="font body-copy font-weight-bold">
                {(!cookies.Data && `Hi, cannot get name`) ||
                  (cookies && `Hi, ${cookies.Data.name}`)}
              </p>
            </DropdownToggle>
            <DropdownMenu className="dropdown-card header-dropdown-card">
              <DropdownItem disabled className="py-2 px-3">
                Profile
              </DropdownItem>
              <CreateDropdownItem
                vuexySkin={vuexySkin}
                href="/profile/ideas"
                itemName="Ideas"
              />
              <CreateDropdownItemSementara
                vuexySkin={vuexySkin}
                href="/profile/konvensi_inovasi"
                itemName="Konvensi Inovasi"
              />
              <CreateDropdownItem
                vuexySkin={vuexySkin}
                href="/profile/achievements"
                itemName="Achievements"
              />
              <CreateDropdownItemSementara
                vuexySkin={vuexySkin}
                href="/profile/stats"
                itemName="Stats"
              />
              <CreateDropdownItem
                vuexySkin={vuexySkin}
                logout
                href="#"
                itemName="Logout"
              />
            </DropdownMenu>
          </Dropdown>
          <div className="d-flex align-items-center border-left border-right px-3">
            <Image src="/badge-rank/4-developer.png" width={30} height={30} />
            <div className="ml-2">
              <div className="d-flex mb-1">
                <p className="font detail-text mr-4">LV.0 - Developer</p>
                <p className="font detail-text text-kalbe-darkGrey">
                  <span className="text-kalbe-orange font-weight-bold">{`0`}</span>
                  {`/${"30,000"}`}
                </p>
              </div>
              <Progress
                value={0}
                min={0}
                max={30000}
                className="header-progress-bar"
                barClassName="inside-progress-bar"
              />
            </div>
          </div>
          <div className="ml-3">
            <Dropdown
              isOpen={isNotifOpen}
              toggle={toggleNotif}
              direction="down"
              className={`bg-kalbe-white $"mr-4"`}
            >
              <DropdownToggle className="bg-kalbe-white border-kalbe-white px-0 py-1">
                <NotificationBadge
                  count={notifCount}
                  effect={[null]}
                  containerStyle={{ left: "0.875em" }}
                  style={{ top: "-0.75em", border: "3px solid #ffffff" }}
                />
                <Notification
                  set={isNotifOpen ? "bold" : "light"}
                  className="font"
                />
              </DropdownToggle>
              <DropdownMenu
                right
                className="notif-dropdown-card shadow border-kalbe-white border-radius-12 pb-2"
              >
                <DropdownItem
                  header
                  className="font app-header font-weight-bold mt-1 mb-2 px-3"
                >
                  Notification
                </DropdownItem>
                <DropdownItem divider className={`m-0`}></DropdownItem>
                {dataNotif &&
                  dataNotif.map((notif, index) => (
                    <CreateNotifItem
                      last={index === dataNotif.length - 1}
                      dataNotif={notif}
                    />
                  ))}
              </DropdownMenu>
            </Dropdown>
          </div>
        </div>
      </div>
    </div>
  );
};

/* export async function getServerSideProps(ctx) {
  const { req, res } = ctx;
  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuWithSubMenu: null,
      },
    };
  }
  const getToken = cookies.split(",")[8];
  const token = getToken.substr(9).slice(0, -2);

  const response = await fetch(
    `${API_URL}${API_MENUWITHSUBMENUS}`,
    {
      headers: {
        OIAuthorization: `Bearer ${token}`,
        apiKey:
          "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
      },
    }
  );
  const data = await response.json();
  console.log(data);
  const menuWithSubMenu = [];
  menuWithSubMenu.push([...data]);
  
  if (!menuWithSubMenu) {
    return { notFound: true };
  }
  return {
    props: {
      menuWithSubMenu: menuWithSubMenu,
    },
  };
} */
export default Header;
