import Image from "next/image";
import Link from "next/link";
import { Col, Container, Row } from "reactstrap";

const CreateFooterItem = ({ last, href, itemName }) => (
  <Link href={href}>
    <a className={`font body-copy text-kalbe-teal ${!last && "mb-2"}`}>
      {itemName}
    </a>
  </Link>
);

const CreateSocialItem = ({ href, socialName }) => (
  <div className="mr-2">
    <Link href={href}>
      <a>
        <Image src={`/logo/${socialName}-footer.png`} width={36} height={36} />
      </a>
    </Link>
  </div>
);

const Footer = () => (
  <div className="pt-4">
    <div className="bg-kalbe-lightGrey">
      <Container className="d-flex py-4">
        <Row className="w-100">
          <Col className="d-flex flex-column">
            <h3 className="font heading3 text-kalbe-teal mb-2">Home</h3>
            <CreateFooterItem href="/#trending-ideas" itemName="Trending Ideas" />
            <CreateFooterItem href="/#top-10" itemName="Top 10 Ideas Giver" />
            <CreateFooterItem href="/" itemName="Learn More" />
            <CreateFooterItem href="/inbox" itemName="Inbox" />
            <CreateFooterItem last href="/" itemName="Notifications" />
          </Col>
          <Col className="d-flex flex-column">
            <h3 className="font heading3 text-kalbe-teal mb-2">Idea Catalog</h3>
            <CreateFooterItem href="/idea_catalog" itemName="All Topics" />
            <CreateFooterItem href="/idea_catalog/submit_idea" itemName="Submit Idea" />
            <CreateFooterItem last href="/" itemName="Download Guide" />
          </Col>
          <Col className="d-flex flex-column">
            <h3 className="font heading3 text-kalbe-teal mb-2">
              Konvensi Inovasi
            </h3>
            <CreateFooterItem href="/" itemName="Participant List" />
            <CreateFooterItem last href="/" itemName="Join Competition" />
          </Col>
          <Col className="d-flex flex-column">
            <h3 className="font heading3 text-kalbe-teal mb-2">Profile</h3>
            <CreateFooterItem href="/profile/ideas" itemName="My Ideas" />
            <CreateFooterItem
              href="/profile/konvensi_inovasi"
              itemName="My Konvensi Inovasi"
            />
            <CreateFooterItem
              href="/profile/achievements"
              itemName="Achievements"
            />
            <CreateFooterItem last href="/profile/stats" itemName="Stats" />
          </Col>
          <Col>
            <img src="/logo/kalbe-farma-footer.png" className="mb-3" />
            <div className="d-flex">
              <CreateSocialItem href="https://www.facebook.com/KalbeFarma.Tbk/" socialName="facebook" />
              <CreateSocialItem href="https://twitter.com/kalbefarma" socialName="twitter" />
              <CreateSocialItem href="https://www.instagram.com/kalbegroup/?hl=id" socialName="instagram" />
              <CreateSocialItem href="https://www.youtube.com/channel/UCXRtm-9QUhVSkOZNqGOVTzQ" socialName="youtube" />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
    <div className="bg-kalbe-teal">
      <div className="container d-flex py-3">
        <p className="font body-copy text-kalbe-white">
          © 2021 Hak Cipta Terpelihara PT Kalbe Farma Tbk
        </p>
      </div>
    </div>
  </div>
);

export default Footer;
