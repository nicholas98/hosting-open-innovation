import { ArrowLeft, ArrowRight, ChevronDown, Delete, Edit } from "react-iconly";
import { useRouter } from "next/router";
import {
  FormGroup,
  Label,
  Input,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
  Row,
  Col,
} from "reactstrap";
import Header from "components/shared/Header";
import Footer from "components/shared/Footer";
import Link from "next/link";
import { useEffect, useState } from "react";
import axios from "axios";
import { getIdeaTopics, getTopicwithSubtopic, deleteIdea } from "helper";
import Spinner from "components/shared/Spinner";
import Router from "next/router";

// const CreateDropdownItem = ({ href, itemName }) => (
//   <>
//     <DropdownItem divider className="my-0 mx-3"></DropdownItem>
//     <Link href={href}>
//       <DropdownItem className="header-dropdown-item">{itemName}</DropdownItem>
//     </Link>
//   </>
// );

const CreateSubtopicItem = ({ href, name, topikID, subtopikID }) => (
  <Link
    href={href}
    as={
      href === "/idea_catalog/[topik]"
        ? `/idea_catalog/${topikID}`
        : `/idea_catalog/${topikID}/${subtopikID}`
    }
  >
    <a className="font body-copy">{name}</a>
  </Link>
);

const TopicsDropdown = ({ data }) => {
  const [isDropdownOpen, setDropdownOpen] = useState(false);

  const toggleDropdown = () => setDropdownOpen(!isDropdownOpen);
  const onHoverFilter = () => setDropdownOpen(true);
  const onLeaveFilter = () => setDropdownOpen(false);

  return (
    <div className="w-100 flex-row-between-center py-3">
      <div className="d-flex align-item-center">
        <h2 className="font heading2 text-kalbe-black">Idea Catalog</h2>
        <Dropdown
          isOpen={isDropdownOpen}
          toggle={toggleDropdown}
          onMouseOver={onHoverFilter}
          onMouseLeave={onLeaveFilter}
          direction="down"
          className="bg-kalbe-white ml-3"
        >
          <DropdownToggle
            className={`font body-copy ${
              isDropdownOpen && "text-kalbe-green"
            } font-weight-bold text-decoration-none bg-kalbe-white border-kalbe-white px-0 py-2`}
          >
            Browse Topics
            <ChevronDown set="light" size="small" className="ml-1" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-card filter-dropdown-card py-2">
            <Link href="/idea_catalog">
              <a className="text-decoration-none d-flex align-items-center">
                <p className="font body-copy font-weight-bold mr-2">
                  See all topics
                </p>
                <ArrowRight set="light" size="small" className="font" />
              </a>
            </Link>
            <hr className="w-100 my-3 mx-0" />
            <div className="filter-dropdown-item">
              {data &&
                data.map((topik) => (
                  <div
                    className="width-topics d-flex flex-column mr-4"
                    key={topik.id}
                  >
                    <h3 className="font heading3 truncate-one-line mb-2">
                      {topik.name}
                    </h3>
                    <CreateSubtopicItem
                      href="/idea_catalog/[topik]"
                      name={`All ${topik.name}`}
                      topikID={topik.id}
                      key={topik.id}
                    />
                    {topik.subTopiks &&
                      topik.subTopiks.map((subTopik) => (
                        <CreateSubtopicItem
                          href="/idea_catalog/[topik]/[subtopik]"
                          name={subTopik.name}
                          topikID={subTopik.topikId}
                          subtopikID={subTopik.id}
                          key={subTopik.id}
                        />
                      ))}
                  </div>
                ))}
            </div>
          </DropdownMenu>
        </Dropdown>
      </div>
      <div className="d-flex">
        <Input
          type="text"
          name="search"
          placeholder={"Search"}
          className="border-radius-10 height-48"
          id="search"
          // value="statik"
          onChange={() => {}}
        />
        <div className="mx-2"></div>
        <Link href="/idea_catalog/submit_idea">
          <Button className="button button-primary button-large-full">
            Submit Your Idea
          </Button>
        </Link>
      </div>
    </div>
  );
};

const InboxHeader = () => {
  return (
    <div className="flex-row-between-center">
      <h2 className="font heading2 text-kalbe-black py-3">All Inbox</h2>
      <Link href="/inbox/new_message">
        <Button className="button button-primary button-large">
          Send New Message
        </Button>
      </Link>
    </div>
  );
};
const IdeaCatalogDetail = (props) => {
  const router = useRouter();
  const refreshData = () => router.replace("/idea_catalog");
  const refreshToken = () => router.replace(router.asPath);

  const test = () => {
    if (confirm("are you sure ? ")) {
      // router.reload(router.asPath);
      deleteIdea(props.idIdea)
        .then((data) => {
          if (data.status < 300) {
            refreshData();
            router.push("/idea_catalog");
          } else if (data.status > 400) {
            alert("failed please refresh page");
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
    }
  };

  return (
    <Row>
      <Col>
        <a
          href="#"
          className="font app-header d-flex align-items-center text-decoration-none py-3"
          onClick={() => router.back()}
        >
          <ArrowLeft set="light" className="text-kalbe-black mr-2" />
          Back
        </a>
      </Col>
      <Col>
        {props.data && (
          <a
            className="font app-header d-flex align-items-right text-decoration-none py-3"
            style={{ marginLeft: "300px" }}
          >
            <Button
              className="button button-secondary button-small p-0"
              style={{
                marginRight: "10px",
                fontSize: "1rem",
                width: "154px",
                border: "1px solid #DFDFDF",
              }}
              onClick={test}
            >
              <Delete
                set="bold"
                primaryColor="black"
                size={15}
                style={{ marginRight: "5px" }}
              />
              Delete
            </Button>
            <Button
              className="button button-secondary button-small p-0"
              style={{
                fontSize: "1rem",
                width: "154px",
                border: "1px solid #DFDFDF",
              }}
              onClick={() => router.push(`/idea_catalog/edit/${props.idIdea}`)}
            >
              <Edit
                set="bold"
                primaryColor="black"
                size={15}
                style={{ marginRight: "5px" }}
              />
              Edit
            </Button>
          </a>
        )}
      </Col>
    </Row>
  );
};
const AdditionalHeader = (props) => {
  if (props.mode === "SubHeaderMenu") {
    return <TopicsDropdown data={props.data[0]} />;
  } else if (props.mode === "InboxMenu") {
    return <InboxHeader />;
  } else if (props.mode === "IdeaCatalogDetail" && props.buttonProps) {
    return (
      <IdeaCatalogDetail
        data={props.buttonProps}
        linkIdea={props.linkIdea}
        idIdea={props.idIdea}
      />
    );
  } else {
    return (
      <a
        href="#"
        className="font app-header d-flex align-items-center text-decoration-none py-3"
        onClick={() => Router.back()}
      >
        <ArrowLeft set="light" className="text-kalbe-black mr-2" />
        Back
      </a>
    );
  }
};

const BaseLayoutFull = (props) => {
  return (
    <>
      <Header
        currentPosition={props.currentPosition}
        menuSubMenu={props.menuSubMenu}
        dataNotif={props.dataNotif}
        notifCount={props.notifCount}
        profilePicture={props.profilePicture}
      />
      <div>
        <div className="container">
          <AdditionalHeader
            mode={props.mode}
            data={props.data}
            otherUser={props.otherUser}
            buttonProps={props.nameValid}
            linkIdea={props.linkIdea}
            idIdea={props.idIdea}
          />
        </div>
        <hr className="m-0" />
        <div className={props.withContainer && "container py-4"}>
          {props.children}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default BaseLayoutFull;
