import ProfileCard from "components/profile/ProfileCard";
import ProfileNavCard from "components/profile/ProfileNavCard";
import { Col, Row } from "reactstrap";
import ReactResizeDetector from "react-resize-detector";
import Header from "components/shared/Header";
import Footer from "components/shared/Footer";
import BaseLayoutFull from "./BaseLayoutFull";
import CommunicationStayCurrentLandscape from "material-ui/svg-icons/communication/stay-current-landscape";

const ProfileLayout = (props) => {
  return (
    <ReactResizeDetector handleWidth>
      {({ width }) => {
        return props.otherUser ? (
          <BaseLayoutFull
            otherUser={props.otherUser}
            menuSubMenu={props.menuSubMenu}
            dataNotif={props.dataNotif}
            notifCount={props.notifCount}
            profilePicture={props.profilePicture}
          >
            <div
              className={`container ${width >= 576 && "mt-4"} ${
                props.border && "mb-4"
              }`}
            >
              <Row className="align-items-start">
                <Col
                  xl="3"
                  lg="4"
                  md="6"
                  xs="12"
                  className={`m-0 ${width < 576 && "px-0"}`}
                >
                  <ProfileCard
                    otherUser={props.otherUser}
                    userData={props.userData}
                    width={width}
                    currentPosition={props.currentPosition}
                    profilePicture={props.otherUserPic}
                  />
                  {width >= 576 ? <br /> : <></>}
                  {width >= 576 ? (
                    <ProfileNavCard
                      otherUser={props.otherUser}
                      dataUPN={props.upn}
                      currentPosition={props.currentPosition}
                    />
                  ) : (
                    <></>
                  )}
                </Col>
                <Col
                  xl="9"
                  lg="8"
                  md="6"
                  xs="12"
                  className={`${props.border && width >= 576 && "border"}`}
                  style={{ borderRadius: `${width >= 576 && "12px"}` }}
                >
                  <div
                    className={`${props.border && "container-fluid"} ${
                      props.border && width >= 576 && "my-4"
                    } ${width < 576 && "mt-4"}`}
                  >
                    {props.children}
                  </div>
                </Col>
              </Row>
            </div>
          </BaseLayoutFull>
        ) : (
          <>
            <Header
              menuSubMenu={props.menuSubMenu}
              dataNotif={props.dataNotif}
              notifCount={props.notifCount}
              profilePicture={props.profilePicture}
            />
            <div
              className={`container ${width >= 576 && "mt-4"} ${
                props.border && "mb-4"
              }`}
            >
              <Row className="align-items-start">
                <Col
                  xl="3"
                  lg="4"
                  md="6"
                  xs="12"
                  className={`m-0 ${width < 576 && "px-0"}`}
                >
                  <ProfileCard
                    width={width}
                    currentPosition={props.currentPosition}
                    profilePicture={props.profilePicture}
                  />
                  {width >= 576 ? <br /> : <></>}
                  {width >= 576 ? (
                    <ProfileNavCard
                      otherUser={props.otherUser}
                      dataUPN={props.upn}
                      currentPosition={props.currentPosition}
                    />
                  ) : (
                    <></>
                  )}
                </Col>
                <Col
                  xl="9"
                  lg="8"
                  md="6"
                  xs="12"
                  className={`${props.border && width >= 576 && "border"}`}
                  style={{ borderRadius: `${width >= 576 && "12px"}` }}
                >
                  <div
                    className={`${props.border && "container-fluid"} ${
                      props.border && width >= 576 && "my-4"
                    } ${width < 576 && "mt-4"}`}
                  >
                    {props.children}
                  </div>
                </Col>
              </Row>
            </div>
            <Footer />
          </>
        );
      }}
    </ReactResizeDetector>
  );
};

export default ProfileLayout;
