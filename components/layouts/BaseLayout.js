import ReactResizeDetector from "react-resize-detector";
import Header from "components/shared/Header";
import Footer from "components/shared/Footer";

const BaseLayout = (props) => {
  return (
    <ReactResizeDetector handleWidth>
      {({ width }) => (
        <div className="min-vh-100" style={props.vuexySkin && {backgroundColor: "#f8f8f8"}}>
          <Header vuexySkin={props.vuexySkin} width={width} currentPosition={props.currentPosition} menuSubMenu={props.menuSubMenu} dataNotif={props.dataNotif} notifCount={props.notifCount} profilePicture={props.profilePicture} />
          <div className={props.withContainer && `${props.vuexySkin ? "container-vuexy" : "container"} py-4`}>
            {props.children}
          </div>
          {!props.vuexySkin && <Footer />}
        </div>
      )}
    </ReactResizeDetector>
  );
};

export default BaseLayout;
