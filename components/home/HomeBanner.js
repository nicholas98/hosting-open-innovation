import Link from "next/link";
import React from "react";
import { Container, Row, Col, Button } from "reactstrap";

const HomeBanner = () => (
  <div className="w-100 bg-kalbe-lightGrey">
    <Container>
      <Row className="d-flex align-items-center pt-3 pb-5">
        <Col lg="7">
          <img src="/images/home-banner.png" className="w-100 m-0" />
        </Col>
        <Col lg="5">
          <h1 className="font heading1 mb-3">Kalbe Idea Platform</h1>
          <p className="font body-copy mb-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis
            lectus sed mi hendrerit suscipit. Proin luctus lacus diam, vitae
            porttitor odio consectetur vitae. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.
          </p>
          <div className="d-flex">
            <Link href="/idea_catalog/submit_idea">
              <Button className="button button-primary button-large">
                Submit Ideas
              </Button>
            </Link>
            <div className="mx-2"></div>
            <Button className="button button-secondary button-large">
              Learn More
            </Button>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
);

export default HomeBanner;
