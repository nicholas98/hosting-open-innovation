import { Col, Row } from "reactstrap";

const KalbeStatistics = () => (
  <Row noGutters>
    <Col
      lg={{ size: 3, order: 1 }}
      xs={{ size: 6, order: 1 }}
      className="bg-kalbe-teal py-5"
    >
      <h1 className="font heading1 text-kalbe-white text-center">30+</h1>
      <p className="font detail-text text-kalbe-white text-center">
        Company from all around
        <br />
        Kalbe group
      </p>
    </Col>
    <Col
      lg={{ size: 3, order: 2 }}
      xs={{ size: 6, order: 2 }}
      className="bg-kalbe-green py-5"
    >
      <h1 className="font heading1 text-kalbe-white text-center">400+</h1>
      <p className="font detail-text text-kalbe-white text-center">
        Author who submitted
        <br />
        their ideas
      </p>
    </Col>
    <Col
      lg={{ size: 3, order: 3 }}
      xs={{ size: 6, order: 4 }}
      className="bg-kalbe-teal py-5"
    >
      <h1 className="font heading1 text-kalbe-white text-center">92%</h1>
      <p className="font detail-text text-kalbe-white text-center">
        Challenges solved
      </p>
    </Col>
    <Col
      lg={{ size: 3, order: 4 }}
      xs={{ size: 6, order: 3 }}
      className="bg-kalbe-green py-5"
    >
      <h1 className="font heading1 text-kalbe-white text-center">100+</h1>
      <p className="font detail-text text-kalbe-white text-center">
        Company challenge
      </p>
    </Col>
  </Row>
);

export default KalbeStatistics;
