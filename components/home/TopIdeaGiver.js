import Image from "next/image";
import { Col } from "reactstrap";

const TopIdeaGiver = ({ data }) => (
  <Col className="d-flex flex-column align-items-center" style={{maxHeight:"140px", maxWidth:"120px"}}>
    <div className="width-60 mb-1 image-container">
      <div className="image-container image-ratio-1by1">
        {/* <Image
          src="/images/profile-picture.jpg"
          layout="fill"
          className="image-inside border-radius-50 "
        /> */}
        <Image
          src={
            data.picturePath !== null
              ? data.picturePath
              : "/images/profile-picture.jpg"
          }
          layout="fill"
          className="image-inside border-radius-50 "
        />
      </div>
    </div>
    <p className="detail-text text-kalbe-black text-center m-0">{data.name}</p>
    <p className="detail-text text-kalbe-darkGrey text-center m-0">{`${data.amountCreated} ideas`}</p>
  </Col>
);

export default TopIdeaGiver;
