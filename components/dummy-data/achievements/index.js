import mock from "../mock";

const achievements = {
  earned: [
  ],
  locked: [
    {
      title: "Inventor",
      id: "1",
      image:
        "/badge-rank/1-inventor.png",
      points: "5000 pts",
      imageAltText: "image-1",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "7 Februari 2021",
      action: "Menang Konvensi Inovasi",
    },
    {
      title: "Idea Pioneer",
      id: "2",
      image:
        "/badge-rank/2-ideapioneer.png",
      points: "5000 pts",
      imageAltText: "image-2",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "4 April 2021",
      action: "Submit ide di konvensi inovasi",
    },
    {
      title: "Thingking Hat",
      id: "3",
      image:
        "/badge-rank/3_thingkinghat.png",
      points: "5000 pts",
      imageAltText: "image-3",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "9 Maret 2021",
      action: "Completing Idea catalog tutorial",
    },
    {
      title: "Light Bulb",
      id: "4",
      image:
        "/badge-rank/4-lightbulb.png",      
      points: "5000 pts",
      imageAltText: "image-1",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "7 Februari 2021",
      action: "Submit first idea di idea catalog",
    },
    {
      title: "Idea Generator",
      id: "5",
      image:
        "/badge-rank/5-ideagenerator.png",      
      points: "5000 pts",
      imageAltText: "image-2",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "4 April 2021",
      action: "Submit ide di Idea catalog",
    },
    {
      title: "Support Team",
      id: "6",
      image:
        "/badge-rank/6-supportteam.png",      
      points: "5000 pts",
      imageAltText: "image-3",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "9 Maret 2021",
      action: "Menjadi member di project idea catalog/ konvensi inovasi",
    },
    {
      title: "One Kalbe Team",
      id: "7",
      image:
        "/badge-rank/7-onekalbe.png",      
      points: "5000 pts",
      imageAltText: "image-1",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "7 Februari 2021",
      action: "Participating in cross-SBU projects",
    },
    {
      title: "Inspiration searcher",
      id: "8",
      image:
        "/badge-rank/8-inspiration.png",      
      points: "5000 pts",
      imageAltText: "image-2",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "4 April 2021",
      action: "Viewing ideas (unique)",
    },
    {
      title: "Influencer",
      id: "9",
      image:
        "/badge-rank/9-influencer.png",      
      points: "5000 pts",
      imageAltText: "image-3",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "9 Maret 2021",
      action: "Getting view for your idea",
    },
    {
      title: "Supporter",
      id: "10",
      image:
        "/badge-rank/10-supporter.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Like a comment/ idea",
    },
    {
      title: "Idea Celebrity",
      id: "11",
      image:
        "/badge-rank/11-ideacelebrity.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Getting idea post liked",
    },
    {
      title: "Refiner",
      id: "12",
      image:
        "/badge-rank/12-refiner.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Commenting",
    },
    {
      title: "Crowd Soucers",
      id: "13",
      image:
        "/badge-rank/13-crowdsourcers.png",      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Getting comments for your idea",
    },
    {
      title: "Stakeholders",
      id: "14",
      image:
        "/badge-rank/14-stakeholders.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Contribution (commenting) on projects that won konvensi inovasi",
    },
    {
      title: "Competitive",
      id: "15",
      image:
        "/badge-rank/15-competitive.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Event participation badge",
    },
    {
      title: "Forerunner",
      id: "16",
      image:
        "/badge-rank/16-forerunner.png",      
      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action:"Event winner Badge",
    },
    {
      title: "Life long learner",
      id: "17",
      image:
        "/badge-rank/17-lifelong.png",      points: "5000 pts",
      imageAltText: "image-4",
      modalText:
        "Donec dapibus pretium volutpat. Vivamus mollis ultrices leo. Aenean a nisi tincidunt, lacinia purus quis, posuere sem. Cras ipsum sapien.",
      achievedDate: "13 April 2021",
      action: "First login of the day",
    },
  ],
};

mock
  .onGet("/api/v1/achievements/earned")
  .reply(() => [200, achievements.earned]);
mock
  .onGet("/api/v1/achievements/locked")
  .reply(() => [200, achievements.locked]);
