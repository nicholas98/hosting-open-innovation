import Image from "next/image";
import React, { useState, useContext, useEffect } from "react";
import styles from "../styles/Home.module.css";
import {
  getBanner,
  getIdeaCatalogs,
  getMenu,
  getTrendingIdea,
} from "../helper";
import HomeBanner from "components/home/HomeBanner";
import { Row, Button, Col, Container } from "reactstrap";
import IdeaCatalogCard from "components/idea_catalog/IdeaCatalogCard";
import Carousel from "components/home/Carousel";
import Link from "next/link";
import BaseLayout from "components/layouts/BaseLayout";
import TopIdeaGiver from "components/home/TopIdeaGiver";
import KalbeStatistics from "components/home/KalbeStatistics";
import Spinner from "components/shared/Spinner";
import {
  API_URL,
  API_IDEACATALOGS,
  API_KEY,
  API_NOTIFICATIONS,
  API_MENUWITHSUBMENUS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_TOPIDEAGIVER,
} from "../constant";
import { useCookies } from "react-cookie";
import lscache from "lscache";

const carouselData = [
  {
    src: "https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80",
    alt: "img1",
    btnText: "a1",
  },
  {
    src: "https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80",
    alt: "img2",
    btnText: "a1",
  },
  {
    src: "https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80",
    alt: "img3",
    btnText: "a1",
  },
  {
    src: "https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80",
    alt: "img4",
    btnText: "a1",
  },
  {
    src: "https://images.unsplash.com/photo-1619446851981-064779c84cc5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=666&q=80",
    alt: "img5",
  },
];

const Home = (props) => {
  const [banner, setBanner] = useState();
  const [trendingIdea, setTrendingIdea] = useState(null);
  const [loading, setLoading] = useState(true);
  const [cookies, setCookie, removeCookie] = useCookies();
  const { ideaTrending, menuDatas, notifications, notifCount, profilePicture, topTenDatas } =
    props;

  /*   useEffect(() => {
    // const userData = JSON.parse(localStorage.getItem("user"));
    // if (userData) {
    // actions.userLogin(userData);
    // console.log(cookies)
    getBanner()
      .then((data) => {
        setBanner(data);
      })
      .catch((err) => {
        console.log(err);
      });
    getMenu()
      .then((data) => {
        // setBanner(data);
      })
      .catch((err) => {
        console.log(err);
      });
    // getIdeaCatalogs()
    //   .then((data) => {
    //     // setBanner(data);
    //     console.log(data);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    getTrendingIdea()
      .then((data) => {
        setTrendingIdea(data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });

    // } else {
    //   Router.push("/");
    // }
  }, []);
  if (loading) {
    return <Spinner />;
  } */

  return (
    <BaseLayout
      currentPosition={"/"}
      menuSubMenu={menuDatas}
      dataNotif={notifications[0]}
      notifCount={notifCount}
      profilePicture={profilePicture}
    >
      <HomeBanner />
      <Container className="mt-5">
        {/* <div className="mb-5">
          <h3 className="font heading3 mb-3">Company Update</h3>
          <div className="w-100">
            <Carousel show={2} loop={true}>
              {carouselData.map((data) => {
                return (
                  <div key={data.alt}>
                    <div style={{ padding: 8 }} className={styles.container}>
                      <Image
                        src={data.src}
                        alt={data.alt}
                        width={1500}
                        height={600}
                        layout="responsive"
                      />
                      {data.btnText && (
                        <Button
                          color="success"
                          block
                          className={styles.carouselBtn}
                        >
                          <p className={styles.btnTextCarousel}>{data.alt}</p>
                        </Button>
                      )}
                    </div>
                  </div>
                );
              })}
            </Carousel>
          </div>
        </div> */}
        <div className="mb-5">
          <div className="flex-row-between-center mb-3">
            <h3 id="trending-ideas" className="font heading3">
              Trending Ideas
            </h3>
            <Link href="/idea_catalog">
              <a className="font body-copy">See all ideas</a>
            </Link>
          </div>
          <Row>
            {ideaTrending &&
              ideaTrending[0].map((data) => {
                return <IdeaCatalogCard data={data} key={data.id} />;
              })}
          </Row>
        </div>
        <div className="mb-5">
          <h3 id="top-10" className="font heading3 mb-3">
            Top 10 Idea Giver
          </h3>
          <Row>
            {topTenDatas &&
              topTenDatas[0].map((data) => {
                return <TopIdeaGiver data={data}/> 
              })}
            {/* <TopIdeaGiver name="Joko Susanto" ideasCount={289} />
            <TopIdeaGiver name="Winda Halim" ideasCount={270} />
            <TopIdeaGiver name="Naura Shinta" ideasCount={264} />
            <TopIdeaGiver name="Maharni" ideasCount={289} />
            <TopIdeaGiver name="Joko Susanto" ideasCount={289} />
            <TopIdeaGiver name="Winda Halim" ideasCount={270} />
            <TopIdeaGiver name="Naura Shinta" ideasCount={264} />
            <TopIdeaGiver name="Maharni" ideasCount={289} />
            <TopIdeaGiver name="Naura Shinta" ideasCount={264} /> */}
          </Row>
        </div>
        <div className="mb-5">
          <KalbeStatistics />
        </div>
      </Container>
    </BaseLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;

  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        ideaTrending: null,
        menuDatas: null,
      },
    };
  }

  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const response = await fetch(
    `${API_URL}${API_IDEACATALOGS}?PageSize=4&OrderBy=trendingScore desc`,
    {
      headers: {
        OIAuthorization: `Bearer ${token}`,
        apiKey: API_KEY,
      },
    }
  );
  const data = await response.json();
  const ideaTrending = [];
  ideaTrending.push([...data]);

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;
  // const profilePicture = req.cookies.profilePicturePath

  const profilePicture = req.cookies.profilePicturePath;

  // var profilePicture = { profilePicture: null };

  // if (!upn.includes("user.")) {
  //   const userResponse = await fetch(`${API_USERPROFILE}/GetByUPN?UPN=${upn}`, {
  //     headers: {
  //       apikey: `${USERPROFILE_APIKEY}`,
  //     },
  //   });
  //   const userData = await userResponse.json();
  //   const profPicResponse = await fetch(
  //     `${API_PROFILEPICTURE}/${userData.data.clusterCode}-${userData.data.nik}`,
  //     {
  //       headers: {
  //         Authorization: PROFILEPICTURE_KEY,
  //       },
  //     }
  //   );
  //   if (profPicResponse.ok) profilePicture = await profPicResponse.json();
  // }

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const responseTopTen = await fetch(`${API_URL}${API_TOPIDEAGIVER}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataTopTen = await responseTopTen.json();
  const topTenDatas = [];
  topTenDatas.push([...dataTopTen]);

  if (!ideaTrending) {
    return { notFound: true };
  }
  return {
    props: {
      ideaTrending: ideaTrending,
      notifications: notifications,
      notifCount: notifCountData,
      menuDatas: menuDatas,
      profilePicture: profilePicture,
      menuDatas: menuDatas,
      topTenDatas: topTenDatas,
    },
  };
}

export default Home;
