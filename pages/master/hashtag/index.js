import {
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
} from "reactstrap";
import {
  API_URL,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_ADMINTAGS,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
} from "constant";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { MoreVertical, CheckCircle, Trash2 } from "react-feather";
import { useState, useRef } from "react";
import ReactPaginate from "react-paginate";
import BaseLayout from "components/layouts/BaseLayout";
import { deleteTag, postTag, updateTag } from "helper";
import { useRouter } from "next/router";
import TextField from "@material-ui/core/TextField";

const TableItem = ({ no, dataItem, group }) => {
  const router = useRouter();
  const filter = createFilterOptions();

  const [actionModal, setActionModal] = useState({ data: null, action: null });
  const [editTagData, setEditTag] = useState(dataItem.name);
  const [editTagGroup, setEditGroup] = useState(null);
  const [deleteMessage, setDeleteMessage] = useState("Confirmation");

  const statusInput = useRef();

  const showAction = (id, action) =>
    setActionModal({ data: id, action: action });
  const resetAction = () => {
    setActionModal({ data: null, action: null });
  };

  const refreshData = () => {
    router.replace(router.asPath);
  };

  const updateStatus = (e) => {
    e.preventDefault();

    const statusData = statusInput.current.value;

    const updateData = {
      id: dataItem.id,
      name: editTagData.includes("#") ? editTagData : `#${editTagData}`,
      status: parseInt(statusData),
      group:
        editTagGroup === null
          ? dataItem.group
          : editTagGroup.groupName.includes("#")
          ? editTagGroup.groupName
          : `#${editTagGroup.groupName}`,
    };

    updateTag(updateData, dataItem.id)
      .then(() => {
        resetAction();
        refreshData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deleteItem = (e) => {
    e.preventDefault();

    deleteTag(dataItem.id)
      .then((data) => {
        if (data.status === 2) {
          resetAction();
          refreshData();
          alert("Data has been deleted successfully");
        } else {
          resetAction();
          alert("Data failed to be deleted");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <tr>
      <td
        className="text-center px-2 align-middle font-weight-bold"
        style={{ color: "#ff9f43" }}
      >
        {no}
      </td>
      <td className="px-2 align-middle truncate-one-line">{dataItem.group}</td>
      <td className="px-2 align-middle truncate-one-line">{dataItem.name}</td>
      <td className="text-center px-2 align-middle">
        {dataItem.status.toString() === "0" && (
          <Badge pill className="badge-vuexy-secondary">
            Inactive
          </Badge>
        )}
        {dataItem.status.toString() === "1" && (
          <Badge pill className="badge-vuexy-success">
            Active
          </Badge>
        )}
      </td>
      <td className="text-center px-2 align-middle">
        <UncontrolledDropdown>
          <DropdownToggle
            className="icon-btn hide-arrow"
            color="transparent"
            size="sm"
            caret
          >
            <MoreVertical size={15} />
          </DropdownToggle>
          <DropdownMenu right className="border-0 border-radius-6">
            <DropdownItem
              className="action-vuexy-item py-2 w-100"
              onClick={() => showAction(dataItem.id, "Edit")}
            >
              <CheckCircle className="mr-2" size={15} />{" "}
              <span className="align-middle">Edit</span>
              <Modal
                centered
                isOpen={
                  actionModal.data === dataItem.id &&
                  actionModal.action === "Edit"
                }
                toggle={resetAction}
                className="mx-auto"
                contentClassName="border-radius-20"
              >
                <ModalBody>
                  <Form onSubmit={updateStatus}>
                    <FormGroup className="mb-3">
                      <label>Hashtag</label>
                      <Input
                        id="editTagName"
                        placeholder="Enter Hashtag Name"
                        onChange={(e) => setEditTag(e.target.value)}
                        defaultValue={editTagData}
                      />
                    </FormGroup>
                    <FormGroup>
                      <label>Group</label>
                      <Autocomplete
                        value={dataItem.group}
                        onChange={(event, newValue) => {
                          if (typeof newValue === "string") {
                            setEditGroup({
                              groupName: newValue,
                            });
                          } else if (newValue && newValue.inputValue) {
                            setEditGroup({
                              groupName: newValue.inputValue,
                            });
                          } else {
                            setEditGroup(newValue);
                          }
                        }}
                        filterOptions={(options, params) => {
                          const filtered = filter(options, params);

                          if (params.inputValue !== "") {
                            filtered.push({
                              inputValue: params.inputValue,
                              groupName: `Add "${params.inputValue}"`,
                            });
                          }

                          return filtered;
                        }}
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                        options={group}
                        getOptionLabel={(option) => {
                          if (typeof option === "string") {
                            return option;
                          }

                          if (option.inputValue) {
                            return option.inputValue;
                          }

                          return option.groupName;
                        }}
                        renderOption={(option) => option.groupName}
                        freeSolo
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField {...params} variant="outlined" />
                        )}
                      />
                    </FormGroup>
                    <FormGroup>
                      <label>Status</label>
                      <select
                        className="form-control"
                        type="select"
                        id="statusInput"
                        defaultValue={dataItem.status.toString()}
                        ref={statusInput}
                      >
                        <option value="0">Inactive</option>
                        <option value="1">Active</option>
                      </select>
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large"
                      >
                        Edit
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetAction}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </DropdownItem>
            <DropdownItem
              className="action-vuexy-item py-2 w-100"
              onClick={() => showAction(dataItem.id, "Delete")}
            >
              <Trash2 className="mr-2" size={15} />{" "}
              <span className="align-middle">Delete</span>
              <Modal
                centered
                isOpen={
                  actionModal.data === dataItem.id &&
                  actionModal.action === "Delete"
                }
                toggle={resetAction}
                className="mx-auto"
                contentClassName="border-radius-20"
              >
                <ModalBody>
                  <p className="font app-header mb-3 text-center">
                    Are you sure you want to delete this tag?
                  </p>
                  <div className="d-flex justify-content-center w-100">
                    <Button
                      type="submit"
                      className="button button-primary button-large bg-kalbe-red"
                      onClick={deleteItem}
                    >
                      Delete
                    </Button>
                    <div className="mx-2"></div>
                    <Button
                      className="button button-secondary button-large"
                      onClick={resetAction}
                    >
                      Cancel
                    </Button>
                  </div>
                </ModalBody>
              </Modal>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
};

const MasterHashtag = (props) => {
  const router = useRouter();
  const filter = createFilterOptions();

  const {
    menuDatas,
    hashtagMaster,
    notifications,
    notifCount,
    profilePicture,
  } = props;

  const [PageSize, setPageSize] = useState(10);

  const [currentPage, setCurrentPage] = useState(0);

  const [searchQuery, setQuery] = useState("");

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  const refreshData = () => {
    router.replace(router.asPath);
  };

  let PER_PAGE = 0;
  let pageCount = 0;

  if (PageSize == 25) {
    PER_PAGE = 25;
    pageCount = Math.ceil(hashtagMaster[0].length / PER_PAGE);
  } else if (PageSize == 50) {
    PER_PAGE = 50;
    pageCount = Math.ceil(hashtagMaster[0].length / PER_PAGE);
  } else {
    PER_PAGE = 10;
    pageCount = Math.ceil(hashtagMaster[0].length / PER_PAGE);
  }

  const offset = currentPage * PER_PAGE;

  const [newTagModal, setTagModal] = useState(false);
  const [newTagGroup, setNewGroup] = useState(null);

  const [newTagData, setTagData] = useState("");
  const showNew = () => setTagModal(true);
  const resetNew = () => {
    setTagModal(false);
    setTagData("");
  };

  const distinctGroup = [
    ...new Set(hashtagMaster[0].map((data) => data.group)),
  ];
  distinctGroup.splice(distinctGroup.indexOf(null), 1);
  const distGroupObj = [];
  distinctGroup.map((data) => distGroupObj.push({ groupName: data }));
  console.log(distGroupObj);

  const addNew = (e) => {
    e.preventDefault();

    const hashtagData = {
      name: newTagData.includes("#") ? newTagData : `#${newTagData}`,
      status: 1,
      group: newTagGroup.groupName.includes("#")
        ? newTagGroup.groupName
        : `#${newTagGroup.groupName}`,
    };

    postTag(hashtagData)
      .then(() => {
        resetNew();
        refreshData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/master"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Card className="card-vuexy py-4 px-5">
          <div className="flex-row-between-center">
            <h3 className="font heading2">Master Hashtag</h3>
            <div className="d-flex align-items-center">
              <Button
                className="button button-large button-primary"
                onClick={showNew}
              >
                Add New
              </Button>
              <Modal
                centered
                isOpen={newTagModal}
                toggle={resetNew}
                contentClassName="border-radius-20"
              >
                <ModalHeader>Submit Hashtag</ModalHeader>
                <ModalBody>
                  <Form onSubmit={addNew}>
                    <FormGroup className="mb-3">
                      <label>Hashtag</label>
                      <Input
                        id="acceptNote"
                        placeholder="Enter Hashtag Name"
                        onChange={(e) => setTagData(e.target.value)}
                        defaultValue=""
                      />
                    </FormGroup>
                    <FormGroup>
                      <label>Group</label>
                      <Autocomplete
                        value=""
                        onChange={(event, newValue) => {
                          if (typeof newValue === "string") {
                            setNewGroup({
                              groupName: newValue,
                            });
                          } else if (newValue && newValue.inputValue) {
                            setNewGroup({
                              groupName: newValue.inputValue,
                            });
                          } else {
                            setNewGroup(newValue);
                          }
                        }}
                        filterOptions={(options, params) => {
                          const filtered = filter(options, params);

                          if (params.inputValue !== "") {
                            filtered.push({
                              inputValue: params.inputValue,
                              groupName: `Add "${params.inputValue}"`,
                            });
                          }

                          return filtered;
                        }}
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                        options={distGroupObj}
                        getOptionLabel={(option) => {
                          if (typeof option === "string") {
                            return option;
                          }

                          if (option.inputValue) {
                            return option.inputValue;
                          }

                          return option.groupName;
                        }}
                        renderOption={(option) => option.groupName}
                        defaultValue={newTagGroup}
                        freeSolo
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField {...params} variant="outlined" />
                        )}
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large"
                      >
                        Submit
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetNew}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </div>
          </div>
        </Card>
        <Card className="card-vuexy">
          <div className="flex-row-between-center py-4 px-5">
            <div className="ml-auto">
              <Input
                id="search-invoice"
                name="search"
                placeholder={"Search (Hashtag Name)"}
                type="text"
                defaultValue=""
                onChange={(event) => {
                  setQuery(event.target.value);
                }}
              />
            </div>
          </div>
          <Table
            className="table border-bottom"
            style={{ tableLayout: "fixed" }}
          >
            <thead>
              <tr>
                <th className="text-center px-2 align-middle" width="5%">
                  No
                </th>
                <th className="px-2 align-middle" width="10%">
                  Group
                </th>
                <th className="px-2 align-middle" width="10%">
                  Hashtag
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Status
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>
              {hashtagMaster[0]
                .slice(offset, offset + PER_PAGE)
                .map((datas, index) => {
                  if (offset > 0) {
                    index = index + offset;
                  }
                  index++;
                  return searchQuery === "" ? (
                    <TableItem
                      no={index}
                      dataItem={datas}
                      group={distGroupObj}
                      key={index}
                    />
                  ) : (
                    datas.name &&
                      datas.name
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase()) && (
                        <TableItem
                          no={index}
                          dataItem={datas}
                          group={distGroupObj}
                          key={index}
                        />
                      )
                  );
                })}
            </tbody>
          </Table>
          <div className="flex-row-between-center py-4 px-5">
            <p className="mb-0" style={{ color: "#b9b9c3" }}>
              Showing 1 to {PER_PAGE} of {pageCount} entries
            </p>
            <ReactPaginate
              pageCount={pageCount}
              onPageChange={handlePageClick}
              nextLabel={""}
              breakLabel={"..."}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              activeClassName={"active"}
              pageClassName={"page-item"}
              previousLabel={""}
              nextLinkClassName={"page-link"}
              nextClassName={"page-item next-item"}
              previousClassName={"page-item prev-item"}
              previousLinkClassName={"page-link"}
              pageLinkClassName={"page-link"}
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName={"pagination react-paginate m-0"}
            />
          </div>
        </Card>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath;

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const response = await fetch(`${API_URL}${API_ADMINTAGS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const data = await response.json();
  const hashtagMaster = [];
  hashtagMaster.push([...data]);

  if (!hashtagMaster) {
    return { notFound: true };
  }

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      hashtagMaster: hashtagMaster,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}
export default MasterHashtag;
