// import vuexyStyles from "styles/vuexy/Vuexy.module.scss"
import {
  Label,
  CustomInput,
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Collapse,
} from "reactstrap";
import {
  API_URL,
  API_GROUPUSERS,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  API_GROUPUSER_SBU,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_TEAMMEMBERLIST,
} from "constant";
import {
  MoreVertical,
  Edit,
  Edit2,
  Trash,
  CheckCircle,
  XCircle,
  EyeOff,
  CornerUpLeft,
  RefreshCw,
  Clipboard,
  Trash2,
} from "react-feather";
import { useState } from "react";
import ReactPaginate from "react-paginate";
import BaseLayout from "components/layouts/BaseLayout";
import Moment from "react-moment";
import moment from "moment";
import { getDetailTeamMember } from "helper";
import { useRouter } from "next/router";
import { useRef } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useFormik } from "formik";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Chip from "@material-ui/core/Chip";

// const TableItem = ({ no, dataItem }) => {
//   const router = useRouter();

//   const [notesModal, setNotesModal] = useState({ data: null, status: null });
//   const [topikName, setTopikName] = useState("");

//   const topikInput = useRef();
//   const statusInput = useRef();

//   const showNotes = (id, status) =>
//     setNotesModal({ data: id, status: status.toString() });
//   const resetNotes = () => {
//     setNotesModal({ data: null, status: null });
//   };

//   const refreshData = () => {
//     router.replace(router.asPath);
//   };

//   const updateStatus = (e) => {
//     e.preventDefault();

//     const statusData = statusInput.current.value;

//     const updateData = {
//       id: dataItem.id,
//       name: topikName,
//       status: parseInt(statusData),
//       subTopiks: dataItem.subTopiks,
//     };

//     updateTopiks(updateData)
//       .then((data) => {
//         console.log(data);
//         // if (!data || data.message) {
//         //   alert(`please refresh the page`);
//         // } else {
//         resetNotes();
//         refreshData();
//         // }
//       })
//       .catch((err) => {
//         console.log(err);
//       });
//   };

//   const deleteTopikFunc = (e) => {
//     e.preventDefault();

//     if (confirm("are you sure to delete the comment? ")) {
//       deleteTopik(dataItem.id)
//         .then((data) => {
//           if (!data || data.message) {
//             alert(`token not found, refreshing the page`);
//             router.reload();
//           } else {
//             alert(`delete succesful`);
//             refreshData();
//           }
//           // refreshData();
//           // if (data.status < 300) {
//           //   refreshData();
//           // }
//         })
//         .catch((err) => {
//           console.log(err);
//         });
//     } else {
//     }
//   };

//   return (
//     <tr>
//       <td
//         className="text-center px-2 align-middle font-weight-bold"
//         style={{ color: "#ff9f43" }}
//       >
//         {no}
//       </td>
//       <td className="px-2 align-middle truncate-one-line">{dataItem.name}</td>
//       <td className="text-center px-2 align-middle">
//         {dataItem.status.toString() === "0" && (
//           <Badge pill className="badge-vuexy-secondary">
//             Inactive
//           </Badge>
//         )}
//         {dataItem.status.toString() === "1" && (
//           <Badge pill className="badge-vuexy-success">
//             Active
//           </Badge>
//         )}
//       </td>
//       <td className="text-center px-2 align-middle">
//         <UncontrolledDropdown>
//           <DropdownToggle
//             className="icon-btn hide-arrow"
//             color="transparent"
//             size="sm"
//             caret
//           >
//             <MoreVertical size={15} />
//           </DropdownToggle>
//           <DropdownMenu right className="border-0 border-radius-6">
//             <DropdownItem
//               className="action-vuexy-item py-2 w-100"
//               onClick={() => showNotes(dataItem.id, "1")}
//             >
//               <CheckCircle className="mr-2" size={15} />{" "}
//               <span className="align-middle">Edit</span>
//               <Modal
//                 centered
//                 isOpen={
//                   notesModal.data === dataItem.id && notesModal.status === "1"
//                 }
//                 toggle={resetNotes}
//                 className="mx-auto"
//                 contentClassName="border-radius-20"
//               >
//                 <ModalBody>
//                   {/* <Form onSubmit={() => updateStatus}> */}
//                   <Form onSubmit={updateStatus}>
//                     <FormGroup className="mb-3">
//                       <label>Topik</label>
//                       {/* <select
//                           className="form-control"
//                           type="select"
//                           id="year-input"
//                           ref={topikInput}
//                           disabled
//                         >
//                           <option value={dataItem.id} defaultValue>
//                             {dataItem.name}
//                           </option>
//                         </select> */}
//                       <Input
//                         defaultValue={dataItem.name}
//                         onChange={(event) => {
//                           setTopikName(event.target.value);
//                         }}
//                       />
//                       <label>Status</label>
//                       <select
//                         className="form-control"
//                         type="select"
//                         id="year-input"
//                         ref={statusInput}
//                       >
//                         <option value="" defaultValue hidden>
//                           Status
//                         </option>
//                         <option value="0">Inactive</option>
//                         <option value="1">Active</option>
//                       </select>
//                     </FormGroup>
//                     <div className="d-flex justify-content-center w-100">
//                       <Button
//                         type="submit"
//                         className="button button-primary button-large"
//                       >
//                         Accept
//                       </Button>
//                       <div className="mx-2"></div>
//                       <Button
//                         className="button button-secondary button-large"
//                         onClick={resetNotes}
//                       >
//                         Cancel
//                       </Button>
//                     </div>
//                   </Form>
//                 </ModalBody>
//               </Modal>
//             </DropdownItem>
//             <DropdownItem
//               className="action-vuexy-item py-2 w-100"
//               onClick={() => showNotes(dataItem.id, "2")}
//             >
//               <Trash2 className="mr-2" size={15} />{" "}
//               <span className="align-middle">Delete</span>
//               <Modal
//                 centered
//                 isOpen={
//                   notesModal.data === dataItem.id && notesModal.status === "2"
//                 }
//                 toggle={resetNotes}
//                 className="mx-auto"
//                 contentClassName="border-radius-20"
//               >
//                 <ModalBody>
//                   <div className="d-flex justify-content-center w-100">
//                     <Button
//                       type="submit"
//                       className="button button-primary button-large"
//                       onClick={deleteTopikFunc}
//                     >
//                       Delete
//                     </Button>
//                     <div className="mx-2"></div>
//                     <Button
//                       className="button button-secondary button-large"
//                       onClick={resetNotes}
//                     >
//                       Cancel
//                     </Button>
//                   </div>
//                 </ModalBody>
//               </Modal>
//             </DropdownItem>
//           </DropdownMenu>
//         </UncontrolledDropdown>
//       </td>
//     </tr>
//   );
// };

const IdeaVerification = (props) => {
  const router = useRouter();

  const {
    menuDatas,
    usersMaster,
    userSBUs,
    notifications,
    notifCount,
    profilePicture,
  } = props;
  const formik = useFormik({
    initialValues: {
      teamName: "",
      teamMember: [],
    },

    onSubmit: (e, value) => handleOnSubmit(e, value),
  });

  const [PageSize, setPageSize] = useState(10);

  const [currentPage, setCurrentPage] = useState(0);
  const [selectedTeamMember, setSelectedTeamMember] = useState([]);
  const [teamMemberList, setTeamMemberList] = useState([]);
  const [holdSubmit, setHoldSubmit] = useState(false);

  const [searchQuery, setQuery] = useState("");

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  const refreshData = () => {
    router.replace(router.asPath);
  };

  // let PER_PAGE = 0;
  // let pageCount = 0;

  // if (PageSize == 25) {
  //   PER_PAGE = 25;
  //   pageCount = Math.ceil(topiksMaster[0].length / PER_PAGE);
  // } else if (PageSize == 50) {
  //   PER_PAGE = 50;
  //   pageCount = Math.ceil(topiksMaster[0].length / PER_PAGE);
  // } else {
  //   PER_PAGE = 10;
  //   pageCount = Math.ceil(topiksMaster[0].length / PER_PAGE);
  // }

  // const offset = currentPage * PER_PAGE;

  const [modal, setModal] = useState(false);
  const [newTopikModal, setTopikModal] = useState(false);
  const [data, setData] = useState([]);

  const toggle = () => {
    setModal(!modal);
  };

  const nameInput = useRef();

  const showNew = () => setTopikModal(true);
  const resetNew = () => {
    setTopikModal(false);
  };

  const addNew = (event) => {
    event.preventDefault();
    const dataPush = {
      name: data.name,
      email: data.email,
    };
    const buildPath = `${JSON.stringify(dataPush)}`;

    router.push(`PICBu/AddNew/${buildPath}`);

    // const topikData = {
    //   name: topik,
    // };
    // postTopik(topikData)
    //   .then((topikData) => {
    //     if (!topikData || topikData.message) {
    //       alert(`please refresh the page`);
    //     } else {
    //       refreshData();
    //     }
    //     // setLoading(false);
    //     // if (data.status < 300) {
    //     //   refreshData();
    //     // }
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  };

  // console.log(usersMaster);

  // const uniqueName = [];
  // usersMaster[0].map((item) => {
  //   var findItem = uniqueName.find((x) => x.name === item.name);
  //   if (!findItem) uniqueName.push(item);
  // });
  // console.log(uniqueName);

  //   console.log(data);
  console.log(data);

  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/admin/idea_verification"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Card className="card-vuexy py-4 px-5">
          <div className="flex-row-between-center">
            <h3 className="font heading2">Master Group User BU</h3>
            <div className="d-flex align-items-center">
              <Button
                className="button button-large button-primary"
                onClick={showNew}
              >
                Add New
              </Button>

              <Modal
                centered
                isOpen={newTopikModal}
                toggle={resetNew}
                contentClassName="border-radius-20"
              >
                <ModalHeader>Submit New</ModalHeader>
                <ModalBody>
                  <Form onSubmit={addNew}>
                    {/* <Form> */}
                    <FormGroup className="mb-3">
                      <Autocomplete
                        id="multiple-limit-tags"
                        options={usersMaster}
                        getOptionLabel={(option) => option.name}
                        onChange={(event, value) => setData(value)}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant="outlined"
                            label="Name"
                            placeholder="Name"
                          />
                        )}
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large"
                      >
                        Submit
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetNew}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </div>
          </div>
        </Card>
        <Card className="card-vuexy">
          <div className="flex-row-between-center py-4 px-5">
            <div className="ml-auto">
              <Input
                id="search-invoice"
                name="search"
                placeholder={"Search (Topik Name)"}
                type="text"
                defaultValue=""
                onChange={(event) => {
                  setQuery(event.target.value);
                }}
              />
            </div>
          </div>

          <Table
            className="table border-bottom"
            style={{ tableLayout: "fixed" }}
          >
            <thead>
              <tr>
                <th className="text-center px-2 align-middle" width="5%">
                  No
                </th>
                <th className="px-2 align-middle" width="10%">
                  Topic
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Status
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>
              {/* {topiksMaster[0]
                      .slice(offset, offset + PER_PAGE)
                      .map((datas, index) => {
                        if (offset > 0) {
                          index = index + offset;
                        }
                        index++;
                        return searchQuery === "" ? (
                          <TableItem no={index} dataItem={datas} key={index} />
                        ) : (
                          datas.name &&
                            datas.name
                              .toLowerCase()
                              .includes(searchQuery.toLowerCase()) && (
                              <TableItem no={index} dataItem={datas} key={index} />
                            )
                        );
                      })} */}
            </tbody>
          </Table>
          {/* <div className="flex-row-between-center py-4 px-5">
                  <p className="mb-0" style={{ color: "#b9b9c3" }}>
                    Showing 1 to {PER_PAGE} of {pageCount} entries
                  </p>
                  <ReactPaginate
                    pageCount={pageCount}
                    onPageChange={handlePageClick}
                    nextLabel={""}
                    breakLabel={"..."}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={2}
                    activeClassName={"active"}
                    pageClassName={"page-item"}
                    previousLabel={""}
                    nextLinkClassName={"page-link"}
                    nextClassName={"page-item next-item"}
                    previousClassName={"page-item prev-item"}
                    previousLinkClassName={"page-link"}
                    pageLinkClassName={"page-link"}
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName={"pagination react-paginate m-0"}
                  />
                </div> */}
        </Card>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  const profilePicture = tokenJSON.profilePicturePath

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const response = await fetch(`${API_TEAMMEMBERLIST}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const data = await response.json();

  const usersMaster = [];
  usersMaster.push(...data.data);

  const responseSBU = await fetch(`${API_URL}${API_GROUPUSER_SBU}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataSBU = await responseSBU.json();
  const userSBUs = [];
  userSBUs.push([...dataSBU]);

  if (!usersMaster) {
    return { notFound: true };
  }

  if (!userSBUs) {
    return { notFound: true };
  }

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      usersMaster: usersMaster,
      userSBUs: userSBUs,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}
export default IdeaVerification;
