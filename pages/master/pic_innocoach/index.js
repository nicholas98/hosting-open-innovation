import {
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
} from "reactstrap";
import {
  API_URL,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_ADMINTAGS,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
} from "constant";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { MoreVertical, CheckCircle, Trash2, CheckSquare } from "react-feather";
import { useState, useRef } from "react";
import ReactPaginate from "react-paginate";
import BaseLayout from "components/layouts/BaseLayout";
import { deleteTag, postTag, updateTag } from "helper";
import { useRouter } from "next/router";
import TextField from "@material-ui/core/TextField";
import Link from "next/link";

const NewUserTableItem = ({ no, newUserItem }) => {
  // const router = useRouter()

  // const selectNewUser = (e) => {
  //   e.preventDefault()

  const userData = `${JSON.stringify({
    name: newUserItem.name,
    email: newUserItem.email,
  })}`;

  //   router.push(`committee/add_new/${userData}`)
  // }

  return (
    <tr>
      <td
        className="text-center px-2 align-middle font-weight-bold"
        style={{ color: "#ff9f43" }}
      >
        {no}
      </td>
      <td className="px-2 align-middle truncate-one-line">
        {newUserItem.name}
      </td>
      <td className="px-2 align-middle truncate-one-line">
        {newUserItem.email && newUserItem.email.toLowerCase()}
      </td>
      <td className="text-center px-2 align-middle truncate-one-line">
        <Link
          href={"/pic_innocoach/add_new/[...slug]"}
          as={`/pic_innocoach/add_new/${userData}`}
        >
          <a className="text-decoration-none">
            <CheckSquare size={20} className="text-kalbe-darkGrey" />
          </a>
        </Link>
      </td>
    </tr>
  );
};

const PICInnoCoachGroupUser = (props) => {
  const {
    menuDatas,
    // hashtagMaster,
    notifications,
    notifCount,
    profilePicture,
  } = props;

  const [NewPageSize, setNewPageSize] = useState(10);
  const [currentNewPage, setCurrentNewPage] = useState(0);
  const [ListPageSize, setListPageSize] = useState(10);
  const [currentListPage, setCurrentListPage] = useState(0);

  const [searchUserQuery, setUserQuery] = useState("");
  const [searchResult, setSearchResult] = useState(null);

  function handleNewPageClick({ selected: selectedPage }) {
    setCurrentNewPage(selectedPage);
  }

  const refreshData = () => {
    router.replace(router.asPath);
  };

  let NEW_PER_PAGE = 0;
  let newPageCount = 0;

  if (searchResult !== null) {
    NEW_PER_PAGE = 10;
    newPageCount = Math.ceil(searchResult.data.totalItem / NEW_PER_PAGE);
  }

  const offsetNew = currentNewPage * NEW_PER_PAGE;

  const [newCommitteeModal, setCommitteeModal] = useState(false);
  const showNew = () => setCommitteeModal(true);
  const resetNew = () => {
    setCommitteeModal(false);
    setUserQuery("");
    setSearchResult(null);
  };

  const getUserList = async () => {
    const userData = await fetch(
      `${API_USERPROFILE}/GetData/1/500?searchValue=${searchUserQuery}`,
      {
        headers: {
          apikey: USERPROFILE_APIKEY,
        },
      }
    );
    const listUserData = await userData.json();
    setSearchResult(listUserData);
  };

  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/admin"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Card className="card-vuexy py-4 px-5">
          <div className="flex-row-between-center">
            <h3 className="font heading2">Group User - Committee</h3>
            <div className="d-flex align-items-center">
              <Button
                className="button button-large button-primary"
                onClick={showNew}
              >
                Add New
              </Button>
              <Modal
                centered
                size="lg"
                isOpen={newCommitteeModal}
                toggle={resetNew}
                contentClassName="border-radius-20"
              >
                <ModalHeader toggle={resetNew}>Browse User</ModalHeader>
                <ModalBody className="p-0">
                  <div className="d-flex justify-content-center w-100 py-4 px-5">
                    <Input
                      id="acceptNote"
                      placeholder="Enter User Name"
                      className="border-radius-10 height-48"
                      onChange={(e) => setUserQuery(e.target.value)}
                      defaultValue={searchUserQuery}
                    />
                    <div className="mx-2"></div>
                    <Button
                      type="submit"
                      className="button button-primary button-large"
                      onClick={getUserList}
                    >
                      Search
                    </Button>
                  </div>
                  {searchResult && (
                    <>
                      <Table
                        className="table border-bottom mb-0"
                        style={{ tableLayout: "fixed" }}
                      >
                        <thead>
                          <tr>
                            <th
                              className="text-center px-2 align-middle"
                              width="5%"
                            >
                              No
                            </th>
                            <th className="px-2 align-middle" width="10%">
                              Name
                            </th>
                            <th className="px-2 align-middle" width="10%">
                              Email
                            </th>
                            <th
                              className="text-center px-2 align-middle"
                              width="5%"
                            >
                              Select
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {searchResult.data.items
                            .slice(offsetNew, offsetNew + NEW_PER_PAGE)
                            .map((data, index) => {
                              if (offsetNew > 0) index = index + offsetNew;

                              index++;

                              return (
                                <NewUserTableItem
                                  no={index}
                                  newUserItem={data}
                                  key={index}
                                />
                              );
                            })}
                        </tbody>
                      </Table>
                      <div className="flex-row-between-center py-4 px-5">
                        <p className="mb-0" style={{ color: "#b9b9c3" }}>
                          Showing 1 to {NEW_PER_PAGE} of{" "}
                          {searchResult.data.totalItem} entries
                        </p>
                        <ReactPaginate
                          pageCount={newPageCount}
                          onPageChange={handleNewPageClick}
                          nextLabel={""}
                          breakLabel={"..."}
                          pageRangeDisplayed={5}
                          marginPagesDisplayed={2}
                          activeClassName={"active"}
                          pageClassName={"page-item"}
                          previousLabel={""}
                          nextLinkClassName={"page-link"}
                          nextClassName={"page-item next-item"}
                          previousClassName={"page-item prev-item"}
                          previousLinkClassName={"page-link"}
                          pageLinkClassName={"page-link"}
                          breakClassName="page-item"
                          breakLinkClassName="page-link"
                          containerClassName={"pagination react-paginate m-0"}
                        />
                      </div>
                    </>
                  )}
                </ModalBody>
              </Modal>
            </div>
          </div>
        </Card>
        <Card className="card-vuexy">
          <div className="flex-row-between-center py-4 px-5">
            <div className="ml-auto">
              <Input
                id="search-invoice"
                name="search"
                placeholder={"Search (Committee Name)"}
                type="text"
                defaultValue=""
                onChange={(event) => {
                  setQuery(event.target.value);
                }}
              />
            </div>
          </div>
          <Table
            className="table border-bottom"
            style={{ tableLayout: "fixed" }}
          >
            <thead>
              <tr>
                <th className="text-center px-2 align-middle" width="5%">
                  No
                </th>
                <th className="px-2 align-middle" width="10%">
                  Name
                </th>
                <th className="px-2 align-middle" width="10%">
                  Email
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Actions
                </th>
              </tr>
            </thead>
            {/* <tbody>
              {hashtagMaster[0]
                .slice(offset, offset + PER_PAGE)
                .map((datas, index) => {
                  if (offset > 0) {
                    index = index + offset;
                  }
                  index++;
                  return searchQuery === "" ? (
                    <TableItem
                      no={index}
                      dataItem={datas}
                      group={distGroupObj}
                      key={index}
                    />
                  ) : (
                    datas.name &&
                      datas.name
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase()) && (
                        <TableItem
                          no={index}
                          dataItem={datas}
                          group={distGroupObj}
                          key={index}
                        />
                      )
                  );
                })}
            </tbody> */}
          </Table>
          <div className="flex-row-between-center py-4 px-5">
            {/* <p className="mb-0" style={{ color: "#b9b9c3" }}>
              Showing 1 to {PER_PAGE} of {pageCount} entries
            </p> */}
            {/* <ReactPaginate
              pageCount={pageCount}
              onPageChange={handlePageClick}
              nextLabel={""}
              breakLabel={"..."}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              activeClassName={"active"}
              pageClassName={"page-item"}
              previousLabel={""}
              nextLinkClassName={"page-link"}
              nextClassName={"page-item next-item"}
              previousClassName={"page-item prev-item"}
              previousLinkClassName={"page-link"}
              pageLinkClassName={"page-link"}
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName={"pagination react-paginate m-0"}
            /> */}
          </div>
        </Card>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  // const response = await fetch(`${API_URL}${API_ADMINTAGS}`, {
  //   headers: {
  //     OIAuthorization: `Bearer ${token}`,
  //     apiKey: API_KEY,
  //   },
  // });

  // const data = await response.json();
  // const hashtagMaster = [];
  // hashtagMaster.push([...data]);

  // if (!hashtagMaster) {
  //   return { notFound: true };
  // }

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      // hashtagMaster: hashtagMaster,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}

export default PICInnoCoachGroupUser;
