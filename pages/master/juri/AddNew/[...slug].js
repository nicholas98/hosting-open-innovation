import React, { useState } from "react";
import {
  API_URL,
  API_ADMINIDEACATALOGS,
  API_TOPIKSWITHSUBTOPIKS,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
} from "constant";
import {
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Collapse,
  CardBody,
  Container,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import BaseLayout from "components/layouts/BaseLayout";

const data = [
  {
    name: "Capsicum",
  },
  {
    name: "Paneer",
  },
  {
    name: "Red Paprika",
  },
  {
    name: "Onions",
  },
  {
    name: "Extra Cheese",
  },
  {
    name: "Baby Corns",
  },
  {
    name: "Mushroom",
  },
];

const AddNew = (props) => {
  const { menuDatas, notifications, notifCount, profilePicture, dataUser } =
    props;
  // const [checkedState, setCheckedState] = useState(
  //   new Array(data.length).fill(false)
  // );

  const [check, setCheck] = useState(new Array(data.length).fill(false));

  const [active, setActive] = useState("SBU/BU");
  const toggleTab = (tab) => {
    active !== tab && setActive(tab);
  };

  const handleOnChange = (position) => {
    const updatedCheckedState = check.map((item, index) =>
      index === position ? !item : item
    );

    setCheck(updatedCheckedState);
  };
  console.log(check);

  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/idea_catalog"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Container className="my-2">
          <Card>
            <Nav tabs>
              <NavItem>
                <NavLink
                  active={active === "SBU/BU"}
                  className="py-1 h5"
                  onClick={() => toggleTab("SBU/BU")}
                  style={{
                    color: "black",
                    fontFamily: "'Montserrat'",
                    fontSize: "14px",
                  }}
                >
                  SBU / BU
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={active === "categories"}
                  className="py-1 h5"
                  onClick={() => toggleTab("categories")}
                  style={{
                    color: "black",
                    fontFamily: "'Montserrat'",
                    fontSize: "14px",
                  }}
                >
                  Category / Title
                </NavLink>
              </NavItem>
            </Nav>
            <CardBody>
              <TabContent activeTab={active}>
                <TabPane tabId="SBU/BU">
                  <Form>
                    <FormGroup className="mb-3">
                      <label>Name</label>
                      <Input
                        id="editTagName"
                        placeholder="Enter Name"
                        value={dataUser.name}
                        disabled
                      />
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <label>Email</label>
                      <Input
                        id="editTagName"
                        placeholder="Enter Email"
                        value={dataUser.email}
                        disabled
                      />
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <label>UPN</label>
                      <Input
                        id="editTagName"
                        placeholder="Enter UPN"
                        value={dataUser.email}
                        disabled
                      />
                    </FormGroup>
                    <FormGroup>
                      <label>Year</label>
                      <select
                        className="form-control"
                        type="select"
                        id="year-input"
                      >
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                      </select>
                    </FormGroup>
                    <FormGroup>
                      <label>Type</label>
                      <select
                        className="form-control"
                        type="select"
                        id="year-input"
                      >
                        <option value="0">Konvensi Inovasi</option>
                        <option value="1">Idea Catalog</option>
                      </select>
                    </FormGroup>
                    <FormGroup>
                      <ul className="toppings-list">
                        {data.map(({ name, price }, index) => {
                          return (
                            <li key={index}>
                              <div className="toppings-list-item">
                                <div className="left-section">
                                  <input
                                    type="checkbox"
                                    id={`custom-checkbox-${index}`}
                                    name={name}
                                    value={name}
                                    checked={check[index]}
                                    onChange={() => handleOnChange(index)}
                                  />
                                  <label htmlFor={`custom-checkbox-${index}`}>
                                    {name}
                                  </label>
                                </div>
                                <div className="right-section"></div>
                              </div>
                            </li>
                          );
                        })}
                      </ul>
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large"
                      >
                        Accept
                      </Button>
                      <div className="mx-2"></div>
                      <Button className="button button-secondary button-large">
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </TabPane>
                <TabPane tabId="categories">
                  <Card className="card-vuexy py-4 px-5">
                    <h3 className="font heading2 mb-1">Filter</h3>
                    <form /* onSubmit={inputDatas} */>
                      <div className="flex-row-between-center">
                        {/* <select
                                        className="form-control"
                                        type="select"
                                        id="year-input"
                                        ref={yearInput}
                                        >
                                        <option value="" selected>
                                            Year
                                        </option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                        </select> */}
                        <div className="mx-2"></div>
                        <select
                          className="form-control"
                          type="select"
                          id="topik-input"
                          /* ref={topikInput} */
                        >
                          <option value="" selected>
                            Category
                          </option>
                          {/* {topikWithSubtopiks &&
                                            topikWithSubtopiks[0].map((datas) => {
                                            return (
                                                <option value={`&TopikName=${datas.name}`} key={datas.id}>
                                                {datas.name}
                                                </option>
                                            );
                                            })} */}
                        </select>
                        <div className="mx-2"></div>
                        <select
                          className="form-control"
                          type="select"
                          id="subTopik-input"
                          /* ref={subTopikInput} */
                        >
                          <option value="" selected>
                            Sub Category
                          </option>
                          {/* {topikWithSubtopiks &&
                                            topikWithSubtopiks[0].map((datas) => {
                                            return datas.subTopiks.map((data) => {
                                                return (
                                                <option
                                                    value={`&SubtopikName=${data.name}`}
                                                    key={data.id}
                                                >
                                                    {data.name}
                                                </option>
                                                );
                                            });
                                            })} */}
                        </select>
                        <div className="mx-2"></div>
                        <select
                          className="form-control"
                          type="select"
                          id="status-input"
                          /* ref={statusInput} */
                        >
                          <option value="" selected hidden>
                            Title
                          </option>
                          {/* <option value="">All Status</option>
                                        <option value={`&Status=4`}>Submitted</option>
                                        <option value={`&Status=5`}>Accepted</option>
                                        <option value={`&Status=6`}>Rejected</option>
                                        <option value={`&Status=7`}>Revise</option> */}
                        </select>
                      </div>
                      <div className="w-100 d-flex justify-content-end mt-3">
                        <Button className="button button-small button-primary">
                          Apply
                        </Button>
                      </div>
                    </form>
                    <div className="w-100 d-flex justify-content-end mt-3">
                      <Button className="button button-small button-primary">
                        Add
                      </Button>
                    </div>
                  </Card>
                  <Card className="card-vuexy">
                    <Table
                      className="table border-bottom"
                      style={{ tableLayout: "fixed" }}
                    >
                      <thead>
                        <tr>
                          <th
                            className="text-center px-2 align-middle"
                            width="5%"
                          >
                            No
                          </th>
                          <th className="px-2 align-middle" width="10%">
                            Categories
                          </th>
                          <th className="px-2 align-middle" width="10%">
                            SubCategories
                          </th>
                          <th className="px-2 align-middle" width="22%">
                            Title
                          </th>
                          <th
                            className="text-center px-2 align-middle"
                            width="8%"
                          >
                            Actions
                          </th>
                        </tr>
                      </thead>
                      {/* <tbody>
                                        {ideaVerif[0]
                                        .slice(offset, offset + PER_PAGE)
                                        .map((datas, index) => {
                                            if (offset > 0) {
                                            index = index + offset;
                                            }
                                            index++;
                                            return searchQuery === "" ? (
                                            <CreateTableItem no={index} dataItem={datas} key={index} />
                                            ) : (
                                            datas.title &&
                                                datas.title
                                                .toLowerCase()
                                                .includes(searchQuery.toLowerCase()) && (
                                                <CreateTableItem
                                                    no={index}
                                                    dataItem={datas}
                                                    key={index}
                                                />
                                                )
                                            );
                                        })}
                                    </tbody> */}
                    </Table>
                    {/* <div className="flex-row-between-center py-4 px-5">
                                    <p className="mb-0" style={{ color: "#b9b9c3" }}>
                                        Showing 1 to {PER_PAGE} of {pageCount} entries
                                    </p>
                                    <ReactPaginate
                                        pageCount={pageCount}
                                        onPageChange={handlePageClick}
                                        nextLabel={""}
                                        breakLabel={"..."}
                                        pageRangeDisplayed={5}
                                        marginPagesDisplayed={2}
                                        activeClassName={"active"}
                                        pageClassName={"page-item"}
                                        previousLabel={""}
                                        nextLinkClassName={"page-link"}
                                        nextClassName={"page-item next-item"}
                                        previousClassName={"page-item prev-item"}
                                        previousLinkClassName={"page-link"}
                                        pageLinkClassName={"page-link"}
                                        breakClassName="page-item"
                                        breakLinkClassName="page-link"
                                        containerClassName={"pagination react-paginate m-0"}
                                    />
                                    </div> */}
                  </Card>
                </TabPane>
              </TabContent>
            </CardBody>
          </Card>
        </Container>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res, params } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const getLink = params.slug;
  const parseJSON = JSON.parse(getLink);

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  var profilePicture = { profilePicture: null };

  if (!upn.includes("user.")) {
    const userResponse = await fetch(`${API_USERPROFILE}?UPN=${upn}`, {
      headers: {
        apikey: `${USERPROFILE_APIKEY}`,
      },
    });
    const userData = await userResponse.json();
    const profPicResponse = await fetch(
      `${API_PROFILEPICTURE}/${userData.data.clusterCode}-${userData.data.nik}`,
      {
        headers: {
          Authorization: PROFILEPICTURE_KEY,
        },
      }
    );
    if (profPicResponse.ok) profilePicture = await profPicResponse.json();
  }

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
      dataUser: parseJSON,
    },
  };
}

export default AddNew;
