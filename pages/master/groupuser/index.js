// import vuexyStyles from "styles/vuexy/Vuexy.module.scss"
import {
  Label,
  CustomInput,
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Collapse,
} from "reactstrap";
import {
  API_URL,
  API_ADMIN_TOPIKS,
  API_TOPIKSWITHSUBTOPIKS,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  API_TOPIKS,
  API_GROUPUSER,
  USERPROFILE_APIKEY,
} from "constant";
import {
  MoreVertical,
  Edit,
  Edit2,
  Trash,
  CheckCircle,
  XCircle,
  EyeOff,
  CornerUpLeft,
  RefreshCw,
  Clipboard,
} from "react-feather";
import { useState } from "react";
import ReactPaginate from "react-paginate";
import BaseLayout from "components/layouts/BaseLayout";
import Moment from "react-moment";
import moment from "moment";
import { deleteUsers, postUsers, updateTopiks } from "helper";
import { useRouter } from "next/router";
import { useRef } from "react";

const TableItem = ({ no, dataItem }) => {
  const router = useRouter();

  const [notesModal, setNotesModal] = useState({ data: null, status: null });
  const [topikName, setTopikName] = useState("");

  const topikInput = useRef();
  const statusInput = useRef();

  const showNotes = (id, status) =>
    setNotesModal({ data: id, status: status.toString() });
  const resetNotes = () => {
    setNotesModal({ data: null, status: null });
  };

  const refreshData = () => {
    router.replace(router.asPath);
  };

  const updateStatus = (e) => {
    e.preventDefault();

    const statusData = statusInput.current.value;

    const updateData = {
      id: dataItem.id,
      name: topikName,
      status: parseInt(statusData),
      subTopiks: dataItem.subTopiks,
    };

    updateTopiks(updateData)
      .then((data) => {
        console.log(data);
        // if (!data || data.message) {
        //   alert(`please refresh the page`);
        // } else {
        resetNotes();
        refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deleteUsersFunc = (e) => {
    e.preventDefault();

    if (confirm("are you sure to delete this user? ")) {
      deleteUsers(dataItem.id)
        .then((data) => {
          if (!data || data.message) {
            alert(`token not found, refreshing the page`);
            router.reload();
          } else {
            alert(`delete succesful`);
            refreshData();
          }
          // refreshData();
          // if (data.status < 300) {
          //   refreshData();
          // }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
    }
  };

  return (
    <tr>
      <td
        className="text-center px-2 align-middle font-weight-bold"
        style={{ color: "#ff9f43" }}
      >
        {no}
      </td>
      <td className="px-2 align-middle truncate-one-line">{dataItem.name}</td>
      <td className="text-center px-2 align-middle">{dataItem.email}</td>
      <td className="text-center px-2 align-middle">
        <div className="py-2 w-100" onClick={() => showNotes(dataItem.id, "2")}>
          <span className="align-middle">Delete</span>
          <Modal
            centered
            isOpen={
              notesModal.data === dataItem.id && notesModal.status === "2"
            }
            toggle={resetNotes}
            className="mx-auto"
            contentClassName="border-radius-20"
          >
            <ModalBody>
              <div className="d-flex justify-content-center w-100">
                <Button
                  type="submit"
                  className="button button-primary button-large"
                  onClick={deleteUsersFunc}
                >
                  Delete
                </Button>
                <div className="mx-2"></div>
                <Button
                  className="button button-secondary button-large"
                  onClick={resetNotes}
                >
                  Cancel
                </Button>
              </div>
            </ModalBody>
          </Modal>
        </div>
      </td>
    </tr>
  );
};

const IdeaVerification = (props) => {
  const router = useRouter();

  const {
    menuDatas,
    GroupUsers,
    topikWithSubtopiks,
    notifications,
    notifCount,
    profilePicture,
  } = props;

  const [PageSize, setPageSize] = useState(10);

  const [currentPage, setCurrentPage] = useState(0);

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  const refreshData = () => {
    router.replace(router.asPath);
  };

  let PER_PAGE = 0;
  let pageCount = 0;

  if (PageSize == 25) {
    PER_PAGE = 25;
    pageCount = Math.ceil(GroupUsers[0].length / PER_PAGE);
  } else if (PageSize == 50) {
    PER_PAGE = 50;
    pageCount = Math.ceil(GroupUsers[0].length / PER_PAGE);
  } else {
    PER_PAGE = 10;
    pageCount = Math.ceil(GroupUsers[0].length / PER_PAGE);
  }

  const offset = currentPage * PER_PAGE;

  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const topikInput = useRef();

  const [newUserModal, setUserModal] = useState(false);

  const [username, setUsername] = useState("");

  const [userEmail, setUserEmail] = useState("");

  const showNew = () => setUserModal(true);
  const resetNew = () => {
    setUserModal(false);
    setUsername("");
    setUserEmail("");
  };

  const addNew = (e) => {
    const userData = {
      name: username,
      email: userEmail,
    };

    postUsers(userData)
      .then((userData) => {
        if (!userData || userData.message) {
          alert(`please refresh the page`);
        } else {
          refreshData();
        }
        // setLoading(false);
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/admin/idea_verification"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Card className="card-vuexy py-4 px-5">
          <div className="flex-row-between-center">
            <h3 className="font heading2">Admin Master Group User</h3>
          </div>
        </Card>
        <Card className="card-vuexy">
          <div className="d-flex align-items-center py-4 px-5">
            <Button
              className="button button-large button-primary"
              onClick={showNew}
            >
              Add New
            </Button>

            <Modal
              centered
              isOpen={newUserModal}
              toggle={resetNew}
              contentClassName="border-radius-20"
            >
              <ModalHeader>Add New User</ModalHeader>
              <ModalBody>
                <Form onSubmit={addNew}>
                  <FormGroup className="mb-3">
                    <Input
                      id="acceptName"
                      placeholder="Enter User Name"
                      onChange={(e) => setUsername(e.target.value)}
                      defaultValue=""
                      required
                    />
                    <Input
                      id="acceptEmail"
                      type="email"
                      placeholder="Enter User Email"
                      onChange={(e) => setUserEmail(e.target.value)}
                      defaultValue=""
                      required
                    />
                  </FormGroup>
                  <div className="d-flex justify-content-center w-100">
                    <Button
                      type="submit"
                      className="button button-primary button-large"
                    >
                      Submit
                    </Button>
                    <div className="mx-2"></div>
                    <Button
                      className="button button-secondary button-large"
                      onClick={resetNew}
                    >
                      Cancel
                    </Button>
                  </div>
                </Form>
              </ModalBody>
            </Modal>
          </div>

          <Table
            className="table border-bottom"
            style={{ tableLayout: "fixed" }}
          >
            <thead>
              <tr>
                <th className="text-center px-2 align-middle" width="5%">
                  No
                </th>
                <th className="px-2 align-middle" width="10%">
                  Name
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Email
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {GroupUsers[0]
                .slice(offset, offset + PER_PAGE)
                .map((datas, index) => {
                  if (offset > 0) {
                    index = index + offset;
                  }
                  index++;
                  return <TableItem no={index} dataItem={datas} key={index} />;
                })}
            </tbody>
          </Table>
          <div className="flex-row-between-center py-4 px-5">
            <p className="mb-0" style={{ color: "#b9b9c3" }}>
              Showing 1 to {PER_PAGE} of {pageCount} entries
            </p>
            <ReactPaginate
              pageCount={pageCount}
              onPageChange={handlePageClick}
              nextLabel={""}
              breakLabel={"..."}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              activeClassName={"active"}
              pageClassName={"page-item"}
              previousLabel={""}
              nextLinkClassName={"page-link"}
              nextClassName={"page-item next-item"}
              previousClassName={"page-item prev-item"}
              previousLinkClassName={"page-link"}
              pageLinkClassName={"page-link"}
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName={"pagination react-paginate m-0"}
            />
          </div>
        </Card>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const response = await fetch(`${API_URL}${API_GROUPUSER}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const data = await response.json();
  const GroupUsers = [];
  GroupUsers.push([...data]);

  const responseTWST = await fetch(`${API_URL}${API_TOPIKSWITHSUBTOPIKS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataTWST = await responseTWST.json();
  const topikWithSubtopiks = [];
  topikWithSubtopiks.push([...dataTWST]);

  if (!GroupUsers) {
    return { notFound: true };
  }

  if (!topikWithSubtopiks) {
    return { notFound: true };
  }

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      GroupUsers: GroupUsers,
      topikWithSubtopiks: topikWithSubtopiks,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}
export default IdeaVerification;
