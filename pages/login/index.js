import { useFormik } from "formik";
import Head from "next/head";
import { Button, Form, FormGroup, Input, Label, Alert } from "reactstrap";
import * as yup from "yup";
import styles from "../../styles/Login.module.css";
import { onLogin, getMenuWithSubMenu } from "../../helper";
import { useContext, useState } from "react";
// import Router from 'next/router';
import { useCookies } from "react-cookie";
// import { useContext } from "react";
// import { Stores } from "store";
import { useRouter } from "next/router";
import lscache from "lscache";

const schema = yup.object().shape({
  email: yup.string().required("Email Is Required"),
  password: yup.string().required("Password Is Required"),
});

const MyInput = ({ field, form, ...props }) => {
  return <input {...field} {...props} />;
};

export default function Login(props) {
  const [cookies, setCookie, removeCookie] = useCookies({});
  const router = useRouter();
  const [userMessage, setUserMessage] = useState("");

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: schema,
    onSubmit: (value) => handleOnSubmit(value),
  });

  const handleOnSubmit = (values) => {
    const loginData = {
      username: values.email,
      password: values.password,
    };
    onLogin(loginData)
      .then((data) => {
        if (data.message) {
          setUserMessage(data);
        } else {
          setCookie("Data", data, { maxAge: 7200 });
          // localStorage.setItem("accessToken", data.token);
          setCookie("profilePicturePath", data.profilePicturePath, {
            maxAge: 7200,
          });
          lscache.set("accessToken", data.token, 120);
          router.push("/");
        }
      })
      /* getMenuWithSubMenu()
      .then((data) => {
        if (data!==null){
          setCookie("MenuDatas", data)
        }
      }) */
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <Head>
        <title>Login</title>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
      </Head>
      <div className={styles.grid_container}>
        <div className={styles.loginLeft}>
          <div className="hero-section">
            <img
              className="image"
              src="/images/login-banner.png"
              style={{ marginTop: "182px", marginLeft: "90px" }}
            />
          </div>
        </div>
        <div className={styles.loginRight}>
          <h1 style={{ color: "#306964" }}>Kalbe Idea Platform</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <Form onSubmit={formik.handleSubmit}>
            <FormGroup>
              <Label className="label-kalbe-form">Email</Label>
              <Input
                type="email"
                name="email"
                className="border-radius-10 height-56"
                id="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
            </FormGroup>
            <FormGroup>
              <Label className="label-kalbe-form">Password</Label>
              <Input
                type="password"
                name="password"
                className="border-radius-10 height-56"
                id="password"
                onChange={formik.handleChange}
                value={formik.values.password}
              />
            </FormGroup>
            <Button
              className="w-100 mt-3 bg-kalbe-green border-radius-10 height-48"
              type={"submit"}
            >
              Login
            </Button>
          </Form>
          {userMessage && (
            <Alert color="danger" className="mt-3">
              {userMessage.message}
            </Alert>
          )}
        </div>
      </div>
    </div>
  );
}
