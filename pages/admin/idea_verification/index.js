// import vuexyStyles from "styles/vuexy/Vuexy.module.scss"
import {
  Label,
  CustomInput,
  Input,
  Card,
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Collapse,
} from "reactstrap";
import {
  API_URL,
  API_ADMINIDEACATALOGS,
  API_ADMINTOPIKSWITHSUBTOPIKS,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
} from "constant";
import {
  MoreVertical,
  Edit,
  Edit2,
  Trash,
  CheckCircle,
  XCircle,
  EyeOff,
  CornerUpLeft,
  RefreshCw,
  Clipboard,
} from "react-feather";
import { useState } from "react";
import ReactPaginate from "react-paginate";
import BaseLayout from "components/layouts/BaseLayout";
import Moment from "react-moment";
import moment from "moment";
import { statusEdit } from "helper";
import { useRouter } from "next/router";
import { useRef } from "react";
import Link from "next/link";

const CreateTableItem = ({ no, dataItem }) => {
  const router = useRouter();

  const [notesModal, setNotesModal] = useState({ data: null, status: null });

  const [descAccept, setDescAcc] = useState("");
  const [descReject, setDescRej] = useState("");
  const [descRevise, setDescRev] = useState("");

  const showNotes = (id, status) =>
    setNotesModal({ data: id, status: status.toString() });
  const resetNotes = () => {
    setNotesModal({ data: null, status: null });
    setDescAcc("");
    setDescRej("");
    setDescRev("");
  };

  const refreshData = () => {
    router.replace(router.asPath);
  };

  const updateStatus = (id, status, notes) => {
    // e.preventDefault();

    const allData = {
      id: id,
      ideaCatalogStatus: [
        {
          op: "replace",
          path: "/status",
          value: status,
        },
        {
          op: "replace",
          path: "/notes",
          value: notes,
        },
      ],
    };

    statusEdit(allData)
      .then((allData) => {
        if (!allData || allData.message) {
          alert(`please refresh the page`);
        } else {
          refreshData();
        }
        // setLoading(false);
        // if (data.status < 300) {
        //   refreshData();
        // }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <tr>
      <td
        className="text-center px-2 align-middle font-weight-bold"
        style={{ color: "#ff9f43" }}
      >
        {no}
      </td>
      <td
        className="text-center px-2 align-middle font-weight-bold"
        style={{ color: "#ff9f43" }}
      >
        <Moment format="YYYY">{dataItem.createdOn}</Moment>
      </td>
      <td className="px-2 align-middle">{dataItem.createdName}</td>
      <td className="px-2 align-middle truncate-one-line">
        {dataItem.topikName}
      </td>
      <td className="px-2 align-middle truncate-one-line">
        {dataItem.subTopikName}
      </td>
      <td className="px-2 align-middle">{dataItem.title}</td>
      <td className="px-2 align-middle">
        {dataItem.ideaCatalogTags.map((tagDatas) => {
          return (
            <Badge
              pill
              className="badge-vuexy-tag mr-3 truncate-one-line"
              key={tagDatas.id}
            >
              {tagDatas.tagName}
            </Badge>
          );
        })}
      </td>
      <td className="text-center px-2 align-middle">
        {dataItem.viewAccessLevel.toString() === "0" && (
          <t>Everyone</t>
        )}
        {dataItem.viewAccessLevel.toString() === "1" && (
          <t>SBU</t>
        )}
        {dataItem.viewAccessLevel.toString() === "2" && (
          <t>BU</t>
        )}
        {dataItem.viewAccessLevel.toString() === "3" && (
          <t>Me</t>
        )}
      </td>
      <td className="text-center px-2 align-middle">
        {dataItem.status.toString() === "0" && (
          <Badge pill className="badge-vuexy-secondary">
            Inactive
          </Badge>
        )}
        {dataItem.status.toString() === "1" && (
          <Badge pill className="badge-vuexy-success">
            Active
          </Badge>
        )}
        {dataItem.status.toString() === "2" && (
          <Badge pill className="badge-vuexy-secondary">
            Deleted
          </Badge>
        )}
        {dataItem.status.toString() === "3" && (
          <Badge pill className="badge-vuexy-revise">
            Draft
          </Badge>
        )}
        {dataItem.status.toString() === "4" && (
          <Badge pill className="badge-vuexy-secondary">
            Submitted
          </Badge>
        )}
        {dataItem.status.toString() === "5" && (
          <Badge pill className="badge-vuexy-success">
            Accepted
          </Badge>
        )}
        {dataItem.status.toString() === "6" && (
          <Badge pill className="badge-vuexy-rejected">
            Rejected
          </Badge>
        )}
        {dataItem.status.toString() === "7" && (
          <Badge pill className="badge-vuexy-revise">
            Revise
          </Badge>
        )}
      </td>
      <td className="text-center px-2 align-middle">
        <UncontrolledDropdown>
          <DropdownToggle
            className="icon-btn hide-arrow"
            color="transparent"
            size="sm"
            caret
          >
            <MoreVertical size={15} />
          </DropdownToggle>
          <DropdownMenu right className="border-0 border-radius-6">
            <Link href={`idea_catalog/detail/${dataItem.id}`}>
              <DropdownItem
                href="/"
                className="action-vuexy-item py-2"
                // onClick={() => {
                //   router.replace(`idea_catalog/detail/${id}`);
                // }}
              >
                <Edit2 className="mr-2" size={15} />{" "}
                <span className="align-middle">View Details</span>
              </DropdownItem>
            </Link>
            <DropdownItem
              className="action-vuexy-item py-2 w-100"
              onClick={() => showNotes(dataItem.id, "5")}
            >
              <CheckCircle className="mr-2" size={15} />{" "}
              <span className="align-middle">Accept</span>
              <Modal
                centered
                isOpen={
                  notesModal.data === dataItem.id && notesModal.status === "5"
                }
                toggle={resetNotes}
                className="mx-auto"
                contentClassName="border-radius-20"
              >
                <ModalBody>
                  <Form
                    onSubmit={() =>
                      updateStatus(
                        dataItem.id,
                        parseInt(notesModal.status),
                        descAccept
                      )
                    }
                  >
                    <FormGroup className="mb-3">
                      <Input
                        type="textarea"
                        id="acceptNote"
                        rows="5"
                        placeholder="Enter description here..."
                        onChange={(e) => setDescAcc(e.target.value)}
                        defaultValue=""
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large"
                      >
                        Accept
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetNotes}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </DropdownItem>
            <DropdownItem
              className="action-vuexy-item py-2 w-100"
              onClick={() => showNotes(dataItem.id, "6")}
            >
              <XCircle className="mr-2" size={15} />{" "}
              <span className="align-middle">Reject</span>
              <Modal
                centered
                isOpen={
                  notesModal.data === dataItem.id && notesModal.status === "6"
                }
                toggle={resetNotes}
                className="mx-auto"
                contentClassName="border-radius-20"
              >
                <ModalBody>
                  <Form
                    onSubmit={() =>
                      updateStatus(
                        dataItem.id,
                        parseInt(notesModal.status),
                        descReject
                      )
                    }
                  >
                    <FormGroup className="mb-3">
                      <Input
                        type="textarea"
                        id="rejectNote"
                        rows="5"
                        placeholder="Enter description here..."
                        onChange={(e) => setDescRej(e.target.value)}
                        defaultValue=""
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large bg-kalbe-red"
                      >
                        Reject
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetNotes}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </DropdownItem>
            <DropdownItem
              className="action-vuexy-item py-2 w-100"
              onClick={() => showNotes(dataItem.id, "7")}
            >
              <RefreshCw className="mr-2" size={15} />{" "}
              <span className="align-middle">Revise</span>
              <Modal
                centered
                isOpen={
                  notesModal.data === dataItem.id && notesModal.status === "7"
                }
                toggle={resetNotes}
                className="mx-auto"
                contentClassName="border-radius-20"
              >
                <ModalBody>
                  <Form
                    onSubmit={() =>
                      updateStatus(
                        dataItem.id,
                        parseInt(notesModal.status),
                        descRevise
                      )
                    }
                  >
                    <FormGroup className="mb-3">
                      <Label for="acceptNote">Additional Notes</Label>
                      <Input
                        type="textarea"
                        id="acceptNote"
                        rows="5"
                        placeholder="Enter description here..."
                        onChange={(e) => setDescRev(e.target.value)}
                        defaultValue=""
                      />
                    </FormGroup>
                    <div className="d-flex justify-content-center w-100">
                      <Button
                        type="submit"
                        className="button button-primary button-large bg-kalbe-orange"
                      >
                        Revise
                      </Button>
                      <div className="mx-2"></div>
                      <Button
                        className="button button-secondary button-large"
                        onClick={resetNotes}
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </ModalBody>
              </Modal>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
};

const IdeaVerification = (props) => {
  const {
    menuDatas,
    ideaVerif,
    topikWithSubtopiks,
    notifications,
    notifCount,
    profilePicture,
  } = props;

  const [PageSize, setPageSize] = useState(10);

  const [currentPage, setCurrentPage] = useState(0);

  const [searchQuery, setQuery] = useState("");

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  let PER_PAGE = 0;
  let pageCount = 0;

  if (PageSize == 25) {
    PER_PAGE = 25;
    pageCount = Math.ceil(ideaVerif[0].length / PER_PAGE);
  } else if (PageSize == 50) {
    PER_PAGE = 50;
    pageCount = Math.ceil(ideaVerif[0].length / PER_PAGE);
  } else {
    PER_PAGE = 10;
    pageCount = Math.ceil(ideaVerif[0].length / PER_PAGE);
  }

  const offset = currentPage * PER_PAGE;

  const yearInput = useRef();
  const topikInput = useRef();
  const subTopikInput = useRef();
  const statusInput = useRef();
  const router = useRouter();

  function inputDatas(event) {
    event.preventDefault();

    let yearDatas = yearInput.current.value;
    if (yearDatas == "") {
      yearDatas = "Year=All";
    } else {
      yearDatas = `minCreatedOn=${yearInput.current.value}-01-01 00:00:00&maxCreatedOn=${yearInput.current.value}-12-31 23:59:59`;
    }
    const topikDatas = topikInput.current.value;
    const subTopikDatas = subTopikInput.current.value;
    const statusDatas = statusInput.current.value;

    let getPath = `idea_verification/${yearDatas}${topikDatas}${subTopikDatas}${statusDatas}`;

    router.push(getPath);
  }

  return (
    <>
      <BaseLayout
        withContainer
        vuexySkin
        currentPosition={"/admin"}
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <Card className="card-vuexy py-4 px-5">
          <h3 className="font heading2 mb-1">Filter</h3>
          <form onSubmit={inputDatas}>
            <div className="flex-row-between-center">
              <select
                className="form-control"
                type="select"
                id="year-input"
                ref={yearInput}
              >
                <option value="" selected>
                  Year
                </option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                <option value="2028">2028</option>
                <option value="2029">2029</option>
                <option value="2030">2030</option>
              </select>
              <div className="mx-2"></div>
              <select
                className="form-control"
                type="select"
                id="topik-input"
                ref={topikInput}
              >
                <option value="" selected>
                  Topic
                </option>
                {topikWithSubtopiks &&
                  topikWithSubtopiks[0].map((datas) => {
                    return (
                      <option value={`&TopikName=${datas.name}`} key={datas.id}>
                        {datas.name}
                      </option>
                    );
                  })}
              </select>
              <div className="mx-2"></div>
              <select
                className="form-control"
                type="select"
                id="subTopik-input"
                ref={subTopikInput}
              >
                <option value="" selected>
                  Sub Topic
                </option>
                {topikWithSubtopiks &&
                  topikWithSubtopiks[0].map((datas) => {
                    return datas.subTopiks.map((data) => {
                      return (
                        <option
                          value={`&SubtopikName=${data.name}`}
                          key={data.id}
                        >
                          {data.name}
                        </option>
                      );
                    });
                  })}
              </select>
              <div className="mx-2"></div>
              <select
                className="form-control"
                type="select"
                id="status-input"
                ref={statusInput}
              >
                <option value="" selected hidden>
                  Status
                </option>
                <option value="">All Status</option>
                <option value={`&Status=4`}>Submitted</option>
                <option value={`&Status=5`}>Accepted</option>
                <option value={`&Status=6`}>Rejected</option>
                <option value={`&Status=7`}>Revise</option>
              </select>
            </div>
            <div className="w-100 d-flex justify-content-end mt-3">
              <Button className="button button-small button-primary">
                Apply
              </Button>
            </div>
          </form>
        </Card>
        <Card className="card-vuexy">
          <div className="flex-row-between-center py-4 px-5">
            <div className="d-flex align-items-center">
              <Label className="mr-2 mb-0" for="rows-per-page">
                Show
              </Label>
              <CustomInput
                type="select"
                id="rows-per-page"
                onChange={(event) => {
                  setPageSize(event.target.value);
                }}
              >
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </CustomInput>
            </div>
            <div>
              <Input
                id="search-invoice"
                name="search"
                placeholder={"Search (Title)"}
                type="text"
                defaultValue=""
                onChange={(event) => {
                  setQuery(event.target.value);
                }}
              />
            </div>
          </div>
          <Table
            className="table border-bottom"
            style={{ tableLayout: "fixed" }}
          >
            <thead>
              <tr>
                <th className="text-center px-2 align-middle" width="5%">
                  No
                </th>
                <th className="text-center px-2 align-middle" width="5%">
                  Year
                </th>
                <th className="px-2 align-middle" width="12%">
                  Creator
                </th>
                <th className="px-2 align-middle" width="10%">
                  Topic
                </th>
                <th className="px-2 align-middle" width="10%">
                  Subtopic
                </th>
                <th className="px-2 align-middle" width="22%">
                  Title
                </th>
                <th className="px-2 align-middle" width="20%">
                  Tagging
                </th>
                <th className="text-center px-2 align-middle" width="20%">
                  View Access
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Status
                </th>
                <th className="text-center px-2 align-middle" width="8%">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>
              {ideaVerif[0]
                .slice(offset, offset + PER_PAGE)
                .map((datas, index) => {
                  if (offset > 0) {
                    index = index + offset;
                  }
                  index++;
                  return searchQuery === "" ? (
                    <CreateTableItem no={index} dataItem={datas} key={index} />
                  ) : (
                    datas.title &&
                      datas.title
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase()) && (
                        <CreateTableItem
                          no={index}
                          dataItem={datas}
                          key={index}
                        />
                      )
                  );
                })}
            </tbody>
          </Table>
          <div className="flex-row-between-center py-4 px-5">
            <p className="mb-0" style={{ color: "#b9b9c3" }}>
              Showing 1 to {PER_PAGE} of {pageCount} entries
            </p>
            <ReactPaginate
              pageCount={pageCount}
              onPageChange={handlePageClick}
              nextLabel={""}
              breakLabel={"..."}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              activeClassName={"active"}
              pageClassName={"page-item"}
              previousLabel={""}
              nextLinkClassName={"page-link"}
              nextClassName={"page-item next-item"}
              previousClassName={"page-item prev-item"}
              previousLinkClassName={"page-link"}
              pageLinkClassName={"page-link"}
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName={"pagination react-paginate m-0"}
            />
          </div>
        </Card>
      </BaseLayout>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
        ideaVerif: null,
        topikWithSubtopiks: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath;

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const response = await fetch(
    `${API_URL}${API_ADMINIDEACATALOGS}?PageSize=100&Status=4&orderBy=createdOn desc`,
    {
      headers: {
        OIAuthorization: `Bearer ${token}`,
        apiKey: API_KEY,
      },
    }
  );

  const data = await response.json();
  const ideaVerif = [];
  ideaVerif.push([...data]);

  const responseTWST = await fetch(
    `${API_URL}${API_ADMINTOPIKSWITHSUBTOPIKS}`,
    {
      headers: {
        OIAuthorization: `Bearer ${token}`,
        apiKey: API_KEY,
      },
    }
  );

  const dataTWST = await responseTWST.json();
  const topikWithSubtopiks = [];
  topikWithSubtopiks.push([...dataTWST]);

  if (!ideaVerif) {
    return { notFound: true };
  }

  if (!topikWithSubtopiks) {
    return { notFound: true };
  }

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      ideaVerif: ideaVerif,
      topikWithSubtopiks: topikWithSubtopiks,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}
export default IdeaVerification;
