// import "bootstrap/dist/css/bootstrap.min.css";
// // import "@/styles/main.scss";
// // import vuexyStyles from "styles/vuexy/main.scss";
// // import mainStyles from "styles/main.scss";
// import { Store } from "../store";
// import Head from "next/head";
// import "../styles/Login.module.css";
// import ResizeObserver from "resize-observer-polyfill";
// import "@/components/dummy-data";
// import { CookiesProvider } from "react-cookie";
// import { useRouter } from "next/router";
// import dynamic from "next/dynamic";
// const vuexyTheme = () => {
//   require("@/styles/vuexy/main.scss");
//   require("bootstrap/dist/css/bootstrap.min.css");
// };
// const mainTheme = () => require("@/styles/main.scss");
// import { useEffect, useState } from "react";
// // const DynamicStyling = dynamic(() =>
// //   import(`../styles/${onMonev && "vuexy/"}main.scss`)
// // );

// // const MainStyles = () => (
// //   <style jsx global>
// //     {mainStyles}
// //   </style>
// // );

// // const VuexyStyles = () => (
// //   <style jsx global>
// //     {vuexyStyles}
// //   </style>
// // );

// // const AppStyles = ({ onMonev }) => {
// //   console.log(onMonev);
// //   return onMonev ? <VuexyStyles /> : <MainStyles />;
// // };

// const App = ({ Component, pageProps }) => {
//   const router = useRouter();
//   const onAdmin = router.pathname.startsWith("/admin/");

//   useEffect(() => {
//     onAdmin ? vuexyTheme() : mainTheme();
//   }, []);

//   if (typeof window !== "undefined") {
//     if (!window.ResizeObserver) {
//       window.ResizeObserver = ResizeObserver;
//     }
//   }

//   return (
//     <>
//       <CookiesProvider>
//         <Store>
//           <Head>
//             <link
//               rel="stylesheet"
//               href="https://fonts.googleapis.com/icon?family=Material+Icons"
//             />
//           </Head>
//           <Component {...pageProps} />
//         </Store>
//       </CookiesProvider>
//     </>
//   );
// };

// export default App;

import "bootstrap/dist/css/bootstrap.min.css";
import "@/styles/main.scss";
import Head from "next/head";
import "../styles/Login.module.css";
import ResizeObserver from "resize-observer-polyfill";
import "@/components/dummy-data";
import { CookiesProvider } from "react-cookie";

const App = ({ Component, pageProps }) => {
  if (typeof window !== "undefined") {
    if (!window.ResizeObserver) {
      window.ResizeObserver = ResizeObserver;
    }
  }

  return (
    <>
      <CookiesProvider>
          <Head>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
          </Head>
          <Component {...pageProps} />
      </CookiesProvider>
    </>
  );
};

export default App;
