import BaseLayout from "components/layouts/BaseLayout";
import BaseLayoutFull from "components/layouts/BaseLayoutFull";
import Image from "next/image";
import Link from "next/link";
import { Delete, Send, ArrowLeft, ArrowRight } from "react-iconly";
import { Col, Input, Row, Button, Card } from "reactstrap";
import ReactPaginate from "react-paginate";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import {
  API_URL,
  API_KEY,
  API_INBOX,
  API_MENUWITHSUBMENUS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_NOTIFICATIONS,
} from "constant";
import { deleteMessage, getInbox } from "../../helper";
const PER_PAGE = 5;
const CreateChatItem = ({ active, name, message, time }) => (
  <Link href="#">
    <a className="text-decoration-none">
      <div className={`inbox-item ${active && "inbox-item-active"}`}>
        <Image
          src="/images/profile-picture.jpg"
          width={56}
          height={56}
          className="image-inside border-radius-50"
        />
        <div className="mx-2"></div>
        <div className="inbox-item-separator">
          <div className="d-flex justify-content-between align-items-center">
            <p className="font body-copy font-weight-bold">{name}</p>
            <p className="font detail-text text-kalbe-darkGrey">{time}</p>
          </div>
          <div className="d-flex justify-content-between align-items-center">
            <p className="font body-copy text-kalbe-darkGrey truncate-one-line">
              {message}
            </p>
          </div>
        </div>
      </div>
    </a>
  </Link>
);

const CreateChatBubble = ({ received, message, time }) => (
  <div
    className={`message-bubble ${
      received ? "message-received" : "message-sent"
    }`}
  >
    <p className="font body-copy">{message}</p>
    <p className="font detail-text text-kalbe-darkGrey">{time}</p>
  </div>
);

const CreateInboxItem = ({ data }) => (
  <div className="d-flex align-items-start mb-4">
    <div>
      <Link href={`/profile/${data.createdBy}/ideas`}>
        <a>
          <div
            className="image-container image-ratio-1by1"
            style={{ width: "4em" }}
          >
            <Image
              src="/images/profile-picture.jpg"
              layout="fill"
              className="image-inside border-radius-50 "
            />
          </div>
        </a>
      </Link>
    </div>
    <div className="mx-2"></div>
    <div className="w-100">
      <Link href={`/profile/${data.createdBy}/ideas`}>
        <a className="text-decoration-none">
          <p className="font app-header font-weight-bold">{data.from}</p>
        </a>
      </Link>
      <p className="font body-copy text-kalbe-darkGrey mb-2">
        {data.createdBy}
      </p>
      <div className="bg-kalbe-lightGrey border-radius-8 p-3 mb-2">
        <p className="body-copy text-kalbe-black mb-0">{data.message}</p>
      </div>
      <div className="d-flex align-items-center">
        <Link href={`/inbox/reply_message/${data.id}`}>
          <a className="font body-copy pr-3 border-right">
            <Send set="light" className="mr-1" />
            Reply
          </a>
        </Link>
        <Link href={`/inbox`}>
          <a
            className="font body-copy pl-3"
            onClick={() => {
              if (confirm("are you sure ? ")) {
                deleteMessage(data.id);
                router.push("/inbox");
              } else {
              }
            }}
          >
            <Delete set="light" className="mr-1" />
            Delete
          </a>
        </Link>
      </div>
    </div>
  </div>
);

const Inbox = (props) => {
  const router = useRouter();
  const refreshData = () => router.replace(router.asPath);
  const handleOnSubmit = (value) => {
    console.log(value);
  };
  const { inboxData, menuDatas, profilePicture, notifications, notifCount } =
    props;
  const [currentPage, setCurrentPage] = useState(0);

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }
  const offset = currentPage * PER_PAGE;
  const currentPageData =
    inboxData &&
    inboxData[0].slice(offset, offset + PER_PAGE).map((data) => (
      <div className="d-flex align-items-start mb-4">
        <div>
          <Link href={`/profile/${data.createdBy}/ideas`}>
            <a>
              <div
                className="image-container image-ratio-1by1"
                style={{ width: "4em" }}
              >
                <Image
                  src={
                    data.creatorProfilePicture !== ""
                      ? data.creatorProfilePicture
                      : "/images/profile-picture.jpg"
                  }
                  layout="fill"
                  className="image-inside border-radius-50 "
                />
              </div>
            </a>
          </Link>
        </div>
        <div className="mx-2"></div>
        <div className="w-100">
          <Link href={`/profile/${data.createdBy}/ideas`}>
            <a className="text-decoration-none">
              <p className="font app-header font-weight-bold">
                {data.createdName}
              </p>
            </a>
          </Link>
          <p className="font body-copy text-kalbe-darkGrey mb-2">
            {data.createdBy.toLowerCase()}
          </p>
          <div className="bg-kalbe-lightGrey border-radius-8 p-3 mb-2">
            <p className="body-copy text-kalbe-black mb-0">{data.message}</p>
          </div>
          <div className="d-flex align-items-center">
            <Link href={`/inbox/reply_message/${data.id}`}>
              <a className="font body-copy pr-3 border-right">
                <Send set="light" className="mr-1" />
                Reply
              </a>
            </Link>
            <Link href={`/inbox`}>
              <a
                className="font body-copy pl-3"
                onClick={() => {
                  if (confirm("are you sure ? ")) {
                    deleteMessage(data.id)
                      .then((data) => {
                        if (data.message === "Unauthorized") {
                          alert("Try again");
                          router.reload();
                        } else {
                          refreshData();
                        }
                        // refreshData();
                        // if (data.status < 300) {
                        //   refreshData();
                        // }
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  } else {
                  }
                }}
              >
                <Delete set="light" className="mr-1" />
                Delete
              </a>
            </Link>
          </div>
        </div>
      </div>
    ));

  const pageCount = Math.ceil(inboxData[0].length / PER_PAGE);

  return (
    <BaseLayoutFull
      withContainer
      currentPosition={"/inbox"}
      mode="InboxMenu"
      menuSubMenu={menuDatas}
      profilePicture={profilePicture}
      dataNotif={notifications[0]}
      notifCount={notifCount}
    >
      {/* {inboxData &&
        inboxData[0].map((data) => {
          return <CreateInboxItem
          name={data.from}
          email={data.createdBy}
          message={data.message}
        />;
        })} */}
      {currentPageData}
      <div className="flex-row-between-center py-2 px-3">
        <p className="mb-0" style={{ color: "#00af04" }}>
          Showing 1 to {PER_PAGE} of {pageCount} entries
        </p>
        <ReactPaginate
          pageCount={pageCount}
          nextLabel={
            <ArrowRight set="light" style={{ width: "15", height: "15" }} />
          }
          breakLabel={"..."}
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          activeClassName={"active"}
          pageClassName={"pagination-item"}
          previousLabel={
            <ArrowLeft set="light" style={{ width: "15", height: "15" }} />
          }
          nextLinkClassName={"pagination-link"}
          nextClassName={"pagination-item next-item"}
          previousClassName={"pagination-item prev-item"}
          previousLinkClassName={"pagination-link"}
          pageLinkClassName={"pagination-link"}
          breakClassName="pagination-item"
          breakLinkClassName="pagination-link"
          containerClassName={"pagination react-paginate"}
        />
      </div>
      {/* <CreateInboxItem
        name="Patrick Pool"
        email="patrick@kalbe.co.id"
        message="Halo bro, bisa diskusi lebih lanjut terkait project? Sepertinya kita bisa melakukan beberapa kolaborasi."
      />
      <CreateInboxItem
        name="Pierre Wanggai"
        email="pierre@kalbe.co.id"
        message="Selamat pagi, bisa diskusi terkait project di bawah ini?"
      />
      <CreateInboxItem
        name="Sandra Smith"
        email="sandra@kalbe.co.id"
        message="Pagi Pak, kami tertarik untuk berkolaborasi untuk project berikut ini."
      /> */}
    </BaseLayoutFull>
  );

  // return (
  //   <BaseLayout withContainer currentPosition={"inbox"}>
  //     <Row>
  //       <Col lg="4">
  //         <Card className="inbox-wrapper">
  //           <div className="inbox-header list-header">
  //             <Input
  //               type="text"
  //               name="name"
  //               className="inbox-input"
  //               id="nameInput"
  //               placeholder="Search messages"
  //               onChange={() => {}}
  //             />
  //           </div>
  //           <div className="list-body">
  //             <CreateChatItem
  //               active
  //               name="Winda Halim"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Anisa Amanda"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Eureka Safitri"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Antonio Susanto"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Michael Jared"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Belinda Naingorian"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //             <CreateChatItem
  //               name="Jeff Knapp"
  //               message="Mau dong join team kamu hehehe"
  //               time="20.58"
  //             />
  //           </div>
  //         </Card>
  //       </Col>
  //       <Col lg="8">
  //         <Card className="inbox-wrapper">
  //           <div className="inbox-header message-header">
  //             <Image
  //               src="/images/profile-picture.jpg"
  //               width={40}
  //               height={40}
  //               className="image-inside border-radius-50"
  //             />
  //             <div className="mx-2"></div>
  //             <p className="font app-header">Winda Halim</p>
  //           </div>
  //           <div className="message-body">
  //             <CreateChatBubble message="Thank you sis!" time="21.01" />
  //             <CreateChatBubble
  //               received
  //               message="Langsung request aja bro nanti gue approve"
  //               time="21.00"
  //             />
  //             <CreateChatBubble
  //               message="Sis, mau join team dong! Kamu kan jago. Ya ya ya? baik deh hehe"
  //               time="20.58"
  //             />
  //           </div>
  //           <div className="message-footer">
  //             <Input
  //               type="text"
  //               name="name"
  //               className="inbox-input message-input"
  //               id="messageInput"
  //               placeholder="Tap to write..."
  //               onChange={() => {}}
  //             />
  //             <Button className="send-message-button">
  //               <Send set="bold" />
  //             </Button>
  //           </div>
  //         </Card>
  //       </Col>
  //     </Row>
  //   </BaseLayout>
  // );
};
export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;

  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        inboxData: null,
        menuDatas: null,
      },
    };
  }

  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const upn = tokenJSON.upn;

  const response = await fetch(`${API_URL}${API_INBOX}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const data = await response.json();
  const inboxData = [];
  inboxData.push([...data]);

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const profilePicture = req.cookies.profilePicturePath;

  if (!inboxData) {
    return { notFound: true };
  }
  return {
    props: {
      inboxData: inboxData,
      menuDatas: menuDatas,
      profilePicture: profilePicture,
      notifications: notifications,
      notifCount: notifCountData,
    },
  };
}
export default Inbox;
