import IdeaCatalogCard from "components/idea_catalog/IdeaCatalogCard";
import {
  Row,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Input,
  Button,
} from "reactstrap";
import { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";
import Head from "next/head";
import { getImage } from "helper";
import Spinner from "components/shared/Spinner";
import React from "react";
import { ArrowLeft, ArrowRight, ChevronDown, Delete, Edit } from "react-iconly";
import {
  API_URL,
  API_IDEACATALOGS,
  API_TOPIKSWITHSUBTOPIKS,
  API_TOPIKS,
  API_KEY,
  API_SUBTOPIKS,
  API_NOTIFICATIONS,
  API_MENUWITHSUBMENUS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_IMAGE,
} from "constant";
import Header from "components/shared/Header";
import Footer from "components/shared/Footer";
import Link from "next/link";

const CreateSubtopicItem = ({ href, name, topik, subtopik }) => (
  <Link
    href={href}
    as={
      href === "/idea_catalog/[topik]"
        ? `/idea_catalog/${encodeURIComponent(topik)}`
        : `/idea_catalog/${encodeURIComponent(topik)}/${encodeURIComponent(
            subtopik
          )}`
    }
    scroll={false}
  >
    <a className="font body-copy">{name}</a>
  </Link>
);

const PER_PAGE = 12;

const IdeaCatalogHome = (props) => {
  const {
    ideaCatalogs,
    menuDatas,
    notifications,
    filterTag,
    profilePicture,
    notifCount,
  } = props;
  const [searchQuery, setQuery] = useState("");
  const [isDropdownOpen, setDropdownOpen] = useState(false);
  const [image, setImage] = useState([]);

  const toggleDropdown = () => setDropdownOpen(!isDropdownOpen);
  const onHoverFilter = () => setDropdownOpen(true);
  const onLeaveFilter = () => setDropdownOpen(false);

  const [currentPage, setCurrentPage] = useState(0);

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  const offset = currentPage * PER_PAGE;

  const currentPageData =
    ideaCatalogs &&
    ideaCatalogs[0].slice(offset, offset + PER_PAGE).map((data) => {
      return searchQuery === "" ? (
        <IdeaCatalogCard data={data} key={data.id} />
      ) : (
        data.title &&
          data.title.toLowerCase().includes(searchQuery.toLowerCase()) && (
            <IdeaCatalogCard data={data} key={data.id} />
          )
      );
    });

  const pageCount = Math.ceil(ideaCatalogs[0].length / PER_PAGE);

  return (
    <>
      <Header
        currentPosition="/idea_catalog"
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      />
      <div>
        <div className="container">
          <div className="w-100 flex-row-between-center py-3">
            <div className="d-flex align-item-center">
              <h2 className="font heading2 text-kalbe-black">Idea Catalog</h2>
              <Dropdown
                isOpen={isDropdownOpen}
                toggle={toggleDropdown}
                onMouseOver={onHoverFilter}
                onMouseLeave={onLeaveFilter}
                direction="down"
                className="bg-kalbe-white ml-3 mr-3 py-2"
              >
                <DropdownToggle
                  className={`font body-copy ${
                    isDropdownOpen && "text-kalbe-green"
                  } font-weight-bold text-decoration-none bg-kalbe-white border-kalbe-white px-0 py-2`}
                >
                  Browse Topics
                  <ChevronDown set="light" size="small" className="ml-1" />
                </DropdownToggle>
                <DropdownMenu className="dropdown-card filter-dropdown-card">
                  <Link href="/idea_catalog" scroll={false}>
                    <a className="text-decoration-none d-flex align-items-center">
                      <p className="font body-copy font-weight-bold mr-2">
                        See all topics
                      </p>
                      <ArrowRight set="light" size="small" className="font" />
                    </a>
                  </Link>
                  <hr className="w-100 my-3 mx-0" />
                  <div className="filter-dropdown-item">
                    {filterTag &&
                      filterTag[0].map((topik) => (
                        <div
                          className="width-topics d-flex flex-column mr-4"
                          key={topik.id}
                        >
                          <h3 className="font heading3 truncate-one-line mb-2">
                            {topik.name}
                          </h3>
                          <CreateSubtopicItem
                            href="/idea_catalog/[topik]"
                            name={`All ${topik.name}`}
                            topik={topik.name}
                            key={topik.id}
                          />
                          {topik.subTopiks &&
                            topik.subTopiks.map((subTopik) => (
                              <CreateSubtopicItem
                                href="/idea_catalog/[topik]/[subtopik]"
                                name={subTopik.name}
                                topik={topik.name}
                                subtopik={subTopik.name}
                                key={subTopik.id}
                              />
                            ))}
                        </div>
                      ))}
                  </div>
                </DropdownMenu>
              </Dropdown>
            </div>
            <div className="d-flex">
              <Input
                type="text"
                name="search"
                placeholder={"Search"}
                className="border-radius-10 height-48"
                id="search"
                defaultValue=""
                onChange={(e) => {
                  setQuery(e.target.value);
                }}
              />
              <div className="mx-2"></div>
              <Link href="/idea_catalog/submit_idea">
                <Button className="button button-primary button-large-full">
                  Submit Your Idea
                </Button>
              </Link>
            </div>
          </div>
        </div>
        <hr className="m-0" />
        <div className="container py-4">
          <Row>{currentPageData}</Row>
          <div className="flex-row-between-center py-2 px-3">
            <p className="mb-0" style={{ color: "#b9b9c3" }}>
              Showing 1 to {PER_PAGE} of {pageCount} entries
            </p>
            <ReactPaginate
              pageCount={pageCount}
              onPageChange={handlePageClick}
              nextLabel={""}
              breakLabel={"..."}
              activeClassName={"active"}
              pageClassName={"page-item"}
              previousLabel={""}
              nextLinkClassName={"page-link"}
              nextClassName={"page-item next-item"}
              previousClassName={"page-item prev-item"}
              previousLinkClassName={"page-link"}
              pageLinkClassName={"page-link"}
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName={"pagination react-paginate m-0"}
            />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        ideaCatalogs: null,
        filterTag: null,
        menuDatas: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const response = await fetch(
    // `${API_URL}${API_IDEACATALOGS}?Status=1&PageSize=40&OrderBy=CreatedOn desc`,
    `${API_URL}${API_IDEACATALOGS}?PageSize=50&OrderBy=CreatedOn desc`,
    {
      headers: {
        OIAuthorization: `Bearer ${token}`,
        apiKey: API_KEY,
      },
    }
  );
  const data = await response.json();
  const ideaCatalogs = [];
  ideaCatalogs.push([...data]);

  // console.log(data.ideaCatalogThumbnails);
  // const getImage = ideaCatalogs[0].map((idea) => {
  //   return idea.ideaCatalogThumbnails;
  // });
  // const getId = getImage.map((id) => {
  //   return id.fileId.toString();
  // });

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const responseFilter = await fetch(`${API_URL}${API_TOPIKSWITHSUBTOPIKS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataFilter = await responseFilter.json();
  const topicDatas = [];
  topicDatas.push([...dataFilter]);

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const upn = tokenJSON.upn;

  const profilePicture = tokenJSON.profilePicturePath;

  if (!ideaCatalogs) {
    return { notFound: true };
  }
  return {
    props: {
      ideaCatalogs: ideaCatalogs,
      notifications: notifications,
      notifCount: notifCountData,
      filterTag: topicDatas,
      menuDatas: menuDatas,
      profilePicture: profilePicture,
    },
  };
}

export default IdeaCatalogHome;
