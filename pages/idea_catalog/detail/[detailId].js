import BaseLayoutFull from "components/layouts/BaseLayoutFull";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import { Col, Row, FormGroup, Label, Input } from "reactstrap";

import LikeComment from "components/idea_catalog/LikeComment";
import Image from "next/image";
import TeamMemberCard from "components/idea_catalog/TeamMemberCard";
import { Download, ChevronDown } from "react-iconly";
import ProgressDetail from "components/idea_catalog/ProgressDetail";
import DetailContent from "components/idea_catalog/DetailContent";
import {
  API_URL,
  API_IDEACATALOGS,
  API_FILES,
  API_TOPIKSWITHSUBTOPIKS,
  API_MENUWITHSUBMENUS,
  API_KEY,
  API_NOTIFICATIONS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
} from "constant";
import { downloadFiles, getImage } from "helper";
import { saveAs } from "file-saver";
import { useRouter } from "next/router";

const CreateCategoryTag = ({ tagName }) => (
  <Col xs="auto" className="mr-2 mb-2">
    <p className="border tags-text text-center border-radius-8 m-0 p-2">
      {tagName}
    </p>
  </Col>
);

const CreateDownloadItem = ({ fileName, allData }) => {
  const filesDownload = () => {
    downloadFiles(allData.id).then((blob) =>
      saveAs(blob, allData.fileOriginalName)
    );
  };

  return (
    <div className="d-flex align-items-center bg-kalbe-lightGrey border-radius-8 px-3 py-2 mb-2">
      <p className="font body-copy truncate-one-line">
        {allData.fileOriginalName}
      </p>
      <a
        className="font body-copy font-weight-bold ml-auto"
        type="button"
        onClick={filesDownload}
      >
        Download
      </a>
    </div>
  );
};

const DetailIdea = (props) => {
  const {
    ideaCatalogs,
    notifications,
    notifCount,
    getFiles,
    menuDatas,
    getSameName,
    profilePicture,
    currentUser,
  } = props;

  const [image, setImage] = useState([]);

  const downloadAll = () => {
    getFiles.map((file) => {
      downloadFiles(file.id).then((blob) =>
        saveAs(blob, file.fileOriginalName)
      );
    });
  };

  // refresh 1x tiap detail biar dapet bearer token, msh belom fix
  const router = useRouter();
  const refreshData = () => router.replace(router.asPath);
  useEffect(() => {
    refreshData();
  }, []);

  const idImage =
    ideaCatalogs[0].ideaCatalogThumbnails !== null
      ? ideaCatalogs[0].ideaCatalogThumbnails.fileId
      : 0;

  useEffect(() => {
    return getImage(idImage).then((data) => {
      const urlImage = URL.createObjectURL(data);
      setImage(urlImage);
    });
  }, []);

  return (
    <BaseLayoutFull
      withContainer
      currentPosition="/idea_catalog"
      mode={"IdeaCatalogDetail"}
      dataNotif={notifications[0]}
      notifCount={notifCount}
      menuSubMenu={menuDatas}
      nameValid={getSameName}
      idIdea={ideaCatalogs[0].id}
      profilePicture={profilePicture}
    >
      <div className="w-100 image-container image-ratio-ideaDetail mb-4 pt-0">
        <img
          src={image}
          layout="fill"
          className="image-inside border-radius-20"
          style={{ width: "100%", maxHeight: "313px", maxWidth: "1148px" }}
        />
      </div>
      <div className="">
        <Row className="match-height">
          <Col lg="8">
            <DetailContent data={ideaCatalogs} />
          </Col>
          <Col lg="4">
            <div className="mb-2">
              <h3 className="heading3 text-kalbe-black">Topics</h3>
              <Row noGutters>
                {ideaCatalogs[0].topikName === "Other" && (
                  <CreateCategoryTag
                    tagName={`${ideaCatalogs[0].othersTopikName}`}
                  />
                )}
                {ideaCatalogs[0].subTopikName === "Other" && (
                  <CreateCategoryTag
                    tagName={`${ideaCatalogs[0].othersSubTopikName}`}
                  />
                )}
                {ideaCatalogs[0].topikName !== "Other" && (
                  <CreateCategoryTag tagName={ideaCatalogs[0].topikName} />
                )}
                {ideaCatalogs[0].subTopikName !== "Other" && (
                  <CreateCategoryTag tagName={ideaCatalogs[0].subTopikName} />
                )}
              </Row>
            </div>
            <div className="mb-4">
              <div className="d-flex align-items-center mb-2">
                <h3 className="heading3 text-kalbe-black m-0">Attachment</h3>
                <div className="d-flex ml-auto">
                  <Download set="curved" className="text-kalbe-black mr-2" />
                  <button
                    className="body-copy font-weight-bold text-kalbe-black m-0"
                    onClick={downloadAll}
                  >
                    Download all
                  </button>
                </div>
              </div>
              {getFiles.map((file) => {
                return (
                  <CreateDownloadItem
                    fileName={file.fileName}
                    key={file.id}
                    dataId={file.id}
                    allData={file}
                  />
                );
              })}
            </div>
            <TeamMemberCard
              otherUser
              data={ideaCatalogs[0].ideaCatalogTeamMembers}
              title={ideaCatalogs[0].title}
              teamName={ideaCatalogs[0].teamName}
              nameValid={getSameName}
              dataNameAuthor={ideaCatalogs[0].createdBy}
            />
            <ProgressDetail
              condition={ideaCatalogs[0].status}
              notes={ideaCatalogs[0].notes}
              getSameName={getSameName}
            />
          </Col>
        </Row>
        <Row>
          <Col lg="8">
            {ideaCatalogs[0].status === 5 && (
              <LikeComment
                countUnshowComment={ideaCatalogs[0].totalUnshowedComment}
                totalComment={ideaCatalogs[0].totalComment}
                commentLast={ideaCatalogs[0].lastComments}
                ideaId={ideaCatalogs[0].id}
                likeIdea={ideaCatalogs[0].ideaCatalogLikes}
                totalLike={ideaCatalogs[0].totalLike}
                totalView={ideaCatalogs[0].totalView}
                nameCurrentUser={currentUser}
              />
            )}
          </Col>
        </Row>
      </div>
    </BaseLayoutFull>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res, params } = ctx;

  const detailId = params.detailId;
  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        ideaCatalogs: null,
        filterTag: null,
        menuDatas: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const response = await fetch(`${API_URL}${API_IDEACATALOGS}/${detailId}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const data = await response.json();
  if (data.status === 404) {
    return { notFound: true };
  }
  const ideaCatalogs = [];
  ideaCatalogs.push({ ...data });

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const getFilesFromIdea = ideaCatalogs[0].ideaCatalogFiles;
  const mapFiles = getFilesFromIdea.map((data) => {
    return data.fileId.toString();
  });

  const allFilesData = await Promise.all(
    mapFiles.map(async (file) => {
      const getData = await fetch(`${API_URL}${API_FILES}/${file}`, {
        headers: {
          OIAuthorization: `Bearer ${token}`,
          apiKey: API_KEY,
        },
      });
      const dataFiles = await getData.json();
      return dataFiles;
    })
  );

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  // const getUpn = cookies.split(",", [1]);
  // const removeStr = getUpn.toString().substr(8).slice(0, -1);
  const upn = tokenJSON.upn;
  const getUpnIdeaCatalogs = ideaCatalogs[0].createdBy;
  const getSameName = upn === getUpnIdeaCatalogs;

  const profilePicture = req.cookies.profilePicturePath;

  if (!ideaCatalogs) {
    return { notFound: true };
  }

  return {
    props: {
      ideaCatalogs: ideaCatalogs,
      notifications: notifications,
      notifCount: notifCountData,
      getFiles: allFilesData,
      menuDatas: menuDatas,
      getSameName: getSameName,
      profilePicture: profilePicture,
      currentUser: upn,
    },
  };
}

export default DetailIdea;
