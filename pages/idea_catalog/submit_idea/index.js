import BaseLayoutFull from "components/layouts/BaseLayoutFull";
import { useFormik } from "formik";
import Head from "next/head";
import Image from "next/image";
import Autocomplete from "@material-ui/lab/Autocomplete";
import DeleteIcon from "@material-ui/icons/Clear";
import { useEffect, useState } from "react";
import { Camera, ChevronDown, Filter } from "react-iconly";
import {
  Button,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from "reactstrap";
import {
  getTag,
  getTopicwithSubtopic,
  postCover,
  postFileMultiple,
  getTeamMemberList,
  submitIdea,
  submitHastag,
  getDetailTeamMember,
  getImageUrl,
} from "../../../helper";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Chip from "@material-ui/core/Chip";
import dynamic from "next/dynamic";
import "suneditor/dist/css/suneditor.min.css";
// import SunEditor from "components/shared/SunEditor";
// import { buttonList } from "suneditor-react";

import * as yup from "yup";
import Router from "next/router";
import DialogAddNewItem from "components/shared/DialogAddNewItem";
import {
  API_KEY,
  API_TAGS,
  API_TEAMMEMBERLIST,
  API_TOPIKSWITHSUBTOPIKS,
  API_URL,
  API_NOTIFICATIONS,
  API_MENUWITHSUBMENUS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_IMAGEURL,
} from "constant";
import IconButton from "@material-ui/core/IconButton";

const schema = yup.object().shape({
  title: yup.string().required("Title Is Required"),
  topic: yup.string().required("Topic Is Required"),
  subTopic: yup.string().required("SubTopic Is Required"),
  description: yup.string().required("Description Is Required"),
  hastag: yup.array().min(1, "Hastag Is Required"),
});

const SunEditor = dynamic(() => import("components/shared/SunEditors"), {
  ssr: false,
});

export default function IdeaCatalogHome(props) {
  const {
    topicwithSubtopic,
    hastagDatas,
    teamMemberDatas,
    notifications,
    notifCount,
    profilePicture,
  } = props;
  const [topic, setTopic] = useState([]);
  const [holdSubmit, setHoldSubmit] = useState(false);
  const [selectedTag, setSelectedTag] = useState([]);
  const [hastagList, setHastagList] = useState(hastagDatas);
  const [selectedTeamMember, setSelectedTeamMember] = useState([]);
  const [teamMemberList, setTeamMemberList] = useState(teamMemberDatas);
  const [tag, setTag] = useState([]);
  const [timeout, settimeout] = useState("");
  const [listFile, setListFile] = useState([]);
  const [viewListFile, setViewListFile] = useState([]);
  const [cover, setCover] = useState();
  const [dialogName, setDialogName] = useState("");
  const imageUrl = "/images/profile-picture.jpg";
  const { menuDatas } = props;
  const formik = useFormik({
    initialValues: {
      cover: "",
      title: "",
      topic: "",
      subTopic: "",
      newTopic: "",
      newSubTopic: "",
      description: "",
      file: "",
      typeof: "personal",
      hastag: [],
      teamName: "",
      teamMember: [],
      viewAccessLevel: 0,
    },
    validationSchema: schema,
    onSubmit: (e, value) => handleOnSubmit(e, value),
  });
  const [avatar, setAvatar] = useState(imageUrl ? imageUrl : "");
  const [open, setOpen] = useState(false);

  function generatePureValue(inputValue) {
    let pure = "";

    for (let i = 0; i < inputValue.length; i += 1) {
      if (inputValue[i] !== ",") {
        pure += inputValue[i];
      }
    }

    return pure;
  }

  function postHastag(value) {
    var newHastag = value;
    const listener = (event) => {
      if (
        (event.code === "Enter" || event.code === "NumpadEnter") &&
        newHastag !== null
      ) {
        submitHastag({ name: newHastag }).then((data) => {
          if (data.status === 404) {
            // setWrongUser(true);
          } else {
            if (data) {
              setHastagList([
                ...hastagList,
                {
                  ...data,
                },
              ]);
            }
          }
        });
        event.preventDefault();
        newHastag = null;
      }
    };
    document.addEventListener("keydown", listener);
    return () => {
      document.removeEventListener("keydown", listener);
    };
  }

  const handleInputNewHastag = (e) => {
    setHoldSubmit(true);
    const { value: inputValue } = e.target;

    if (timeout) {
      clearTimeout(timeout);
    }

    const pure = generatePureValue(inputValue);

    const to = setTimeout(() => {
      if (inputValue !== 0 || inputValue !== "") {
        postHastag(pure);
        setHoldSubmit(false);
      }
    }, 1500);
    settimeout(to);
  };

  const handleInputSearchTeamMember = (e) => {
    setHoldSubmit(true);
    const { value: inputValue } = e.target;

    if (timeout) {
      clearTimeout(timeout);
    }

    const pure = generatePureValue(inputValue);

    const to = setTimeout(() => {
      if (inputValue !== 0 || inputValue !== "") {
        getTeamMemberList(pure).then((data) => {
          setTeamMemberList(data.data);
        });
        setHoldSubmit(false);
      }
    }, 1000);
    settimeout(to);
  };

  const handleClearList = (key) => {
    const newFileValueView = viewListFile.filter((row) => row.id !== key);
    const newFileValue = listFile.filter((row) => row.fileId !== key);
    setViewListFile(newFileValueView);
    setListFile(newFileValue);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUploadFile = (e) => {
    const reader = new FileReader();
    const files = [...e.target.files];
    setHoldSubmit(true);
    reader.onload = function () {
      setAvatar(reader.result);
    };
    const formData = new FormData();
    for (var x = 0; x < files.length; x++) {
      formData.append("formFiles", e.target.files[x]);
    }
    postFileMultiple(formData).then((response) => {
      var dataRespon = [];
      setViewListFile([...viewListFile, ...response]);
      response.map((row) => {
        dataRespon = [
          ...dataRespon,
          {
            fileId: row.id,
            filePath: row.filePath,
            fileOriginalName: row.fileOriginalName,
          },
        ];
        setListFile([...listFile, ...dataRespon]);
      });
      setHoldSubmit(false);
    });
    setHoldSubmit(false);
  };

  function keyPress(e) {
    if (e.keyCode === 13) {
      console.log("BERHASIL");
    }
  }

  const changePicture = (event) => {
    const reader = new FileReader();
    const files = event.target.files;
    reader.onload = function () {
      setAvatar(reader.result);
    };
    reader.readAsDataURL(files[0]);
    postCover(files[0]).then((data) => {
      setCover({
        fileId: data.id,
        filePath: data.filePath,
      });
    });
  };

  const handleChangeSunEditor = (event, editorContents) => {
    var res = encodeURI(editorContents);
    formik.setFieldValue("description", res);
  };

  const handleImageUploadBefore = (files, info, uploadHandler) => {
    let response = {};
    postCover(files[0]).then((data) => {
      if (data) {
        response = {
          result: [
            {
              url: `${API_URL}${API_IMAGEURL}/${data.id}`,
              name: data.originalName || "Image",
            },
          ],
        };
        uploadHandler(response);
      }
    });
  };

  const handleOnChangeTeamMember = (e, value) => {
    setHoldSubmit(true);
    var detailMember = [];
    if (formik.values.teamMember.length !== 0) {
      if (formik.values.teamMember.length < value.length) {
        for (var i = 0, l = formik.values.teamMember.length; i < l; i++) {
          for (var j = 0, m = value.length; j < m; j++) {
            if (formik.values.teamMember[i].nik !== value[j].nik) {
              detailMember = { ...value[j] };
            }
          }
        }
        getDetailTeamMember(detailMember.nik, detailMember.clusterCode)
          .then((data) => {
            formik.setFieldValue("teamMember", [
              ...formik.values.teamMember,
              {
                ...data.data,
              },
            ]);
            setHoldSubmit(false);
          })
          .catch((err) => {
            console.log(err);
            // setWrongUser(true);
          });
        detailMember = [];
      } else {
        const filterByReference = (arr1, arr2) => {
          let res = [];
          res = arr1.filter((el) => {
            return !arr2.find((element) => {
              return element.nik === el.nik;
            });
          });
          return res[0];
        };
        var selectedData = filterByReference(formik.values.teamMember, value);
        let newData = formik.values.teamMember.filter(
          (row) => row.nik !== selectedData.nik
        );
        formik.setFieldValue("teamMember", newData);
        setHoldSubmit(false);
      }
    } else {
      getDetailTeamMember(value[0].nik, value[0].clusterCode)
        .then((data) => {
          formik.setFieldValue("teamMember", [data.data]);
          setHoldSubmit(false);
        })
        .catch((err) => {
          console.log(err);
          // setWrongUser(true);
        });
    }
  };

  const handleOnSave = (value) => {
    // setHoldSubmit(true);
    var hastag = [];
    var submitData = {};
    let fixedTeamMember = [];
    formik.values.hastag.map((row, key) => {
      hastag = [
        ...hastag,
        {
          tagId: row.id,
          tagName: row.name,
        },
      ];
    });
    formik.values.teamMember.map((row, key) => {
      fixedTeamMember = [
        ...fixedTeamMember,
        {
          upn: row.userPrincipalName,
          jobTitle: row.jobTtlName,
          ...row,
        },
      ];
    });
    if (formik.values.topic === "Other") {
      submitData = {
        othersTopikName: formik.values.newTopic,
        topikName: formik.values.topic,
        subTopikName: formik.values.subTopic,
        title: formik.values.title,
        description: formik.values.description,
        teamName: formik.values.teamName,
        viewAccessLevel: 0,
        ideaCatalogThumbnails: { ...cover },
        ideaCatalogTeamMembers: [...fixedTeamMember],
        ideaCatalogFiles: [...listFile],
        ideaCatalogTags: hastag,
        ideaCatalogLikes: [],
        ideaCatalogComments: [],
        viewAccessLevel: parseInt(formik.values.viewAccessLevel),
        status: 3,
      };
      if (formik.values.subTopic === "Other") {
        submitData = {
          othersTopikName: formik.values.newTopic,
          othersSubTopikName: formik.values.newSubTopic,
          topikName: formik.values.topic,
          subTopikName: formik.values.subTopic,
          title: formik.values.title,
          description: formik.values.description,
          teamName: formik.values.teamName,
          viewAccessLevel: 0,
          ideaCatalogThumbnails: { ...cover },
          ideaCatalogTeamMembers: [...fixedTeamMember],
          ideaCatalogFiles: [...listFile],
          ideaCatalogTags: hastag,
          ideaCatalogLikes: [],
          ideaCatalogComments: [],
          viewAccessLevel: parseInt(formik.values.viewAccessLevel),
          status: 3,
        };
      }
    } else {
      submitData = {
        othersTopikName: formik.values.newTopic,
        othersSubTopikName: formik.values.newSubTopic,
        topikName: formik.values.topic,
        subTopikName: formik.values.subTopic,
        title: formik.values.title,
        description: formik.values.description,
        teamName: formik.values.teamName,
        viewAccessLevel: 0,
        ideaCatalogThumbnails: { ...cover },
        ideaCatalogTeamMembers: [...fixedTeamMember],
        ideaCatalogFiles: [...listFile],
        ideaCatalogTags: hastag,
        ideaCatalogLikes: [],
        ideaCatalogComments: [],
        viewAccessLevel: parseInt(formik.values.viewAccessLevel),
        status: 3,
      };
    }
    submitIdea(submitData)
      .then((data) => {
        Router.push("/idea_catalog");
      })
      .catch((err) => {
        console.log(err);
        // setWrongUser(true);
      });
  };

  const handleOnSubmit = (value) => {
    setHoldSubmit(true);
    var hastag = [];
    var submitData = {};
    var fixedTeamMember = [];
    formik.values.hastag.map((row, key) => {
      hastag = [
        ...hastag,
        {
          tagId: row.id,
          tagName: row.name,
        },
      ];
    });
    formik.values.teamMember.map((row, key) => {
      fixedTeamMember = [
        ...fixedTeamMember,
        {
          upn: row.userPrincipalName,
          jobTitle: row.jobTtlName,
          ...row,
        },
      ];
    });
    if (formik.values.topic === "Other") {
      submitData = {
        othersTopikName: formik.values.newTopic,
        topikName: formik.values.topic,
        subTopikName: formik.values.subTopic,
        title: formik.values.title,
        description: formik.values.description,
        teamName: formik.values.teamName,
        viewAccessLevel: 0,
        ideaCatalogThumbnails: { ...cover },
        ideaCatalogTeamMembers: [...fixedTeamMember],
        ideaCatalogFiles: [...listFile],
        ideaCatalogTags: hastag,
        ideaCatalogLikes: [],
        ideaCatalogComments: [],
        viewAccessLevel: parseInt(formik.values.viewAccessLevel),
        status: 4,
      };
      if (formik.values.subTopic === "Other") {
        submitData = {
          othersTopikName: formik.values.newTopic,
          othersSubTopikName: formik.values.newSubTopic,
          topikName: formik.values.topic,
          subTopikName: formik.values.subTopic,
          title: formik.values.title,
          description: formik.values.description,
          teamName: formik.values.teamName,
          viewAccessLevel: 0,
          ideaCatalogThumbnails: { ...cover },
          ideaCatalogTeamMembers: [...fixedTeamMember],
          ideaCatalogFiles: [...listFile],
          ideaCatalogTags: hastag,
          ideaCatalogLikes: [],
          ideaCatalogComments: [],
          viewAccessLevel: parseInt(formik.values.viewAccessLevel),
          status: 4,
        };
      }
    } else {
      submitData = {
        othersTopikName: formik.values.newTopic,
        othersSubTopikName: formik.values.newSubTopic,
        topikName: formik.values.topic,
        subTopikName: formik.values.subTopic,
        title: formik.values.title,
        description: formik.values.description,
        teamName: formik.values.teamName,
        viewAccessLevel: 0,
        ideaCatalogThumbnails: { ...cover },
        ideaCatalogTeamMembers: [...fixedTeamMember],
        ideaCatalogFiles: [...listFile],
        ideaCatalogTags: hastag,
        ideaCatalogLikes: [],
        ideaCatalogComments: [],
        viewAccessLevel: parseInt(formik.values.viewAccessLevel),
        status: 4,
      };
    }
    console.log(submitData);
    // submitIdea(submitData)
    //   .then((data) => {
    //     Router.push("/idea_catalog");
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     // setWrongUser(true);
    //   });
  };
  return (
    <div>
      <Head>
        <title>Input Ideas</title>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
      </Head>
      <BaseLayoutFull
        backOnly
        withContainer
        menuSubMenu={menuDatas}
        dataNotif={notifications[0]}
        notifCount={notifCount}
        profilePicture={profilePicture}
      >
        <h1 className="heading1">Submit Idea</h1>
        <Row>
          <Col /* lg="6" md="8" xs="12" */>
            <Form onSubmit={formik.handleSubmit}>
              <FormGroup>
                <Label className="label" for="profilePicture">
                  Cover Image
                </Label>
                <div className="form-image-container">
                  <Image
                    src={avatar}
                    layout="fill"
                    className="submit-idea-image-child"
                  />
                  <Button tag={Label} className="image-input-button">
                    <Camera set="light" className="font" />
                    <Input
                      hidden
                      onChange={changePicture}
                      type="file"
                      name="file"
                      id="profilePicture"
                      accept="image/*"
                    />
                  </Button>
                </div>
                <FormText className="sublabel">
                  Use 1100 x 300 (11:3) for best size
                </FormText>
              </FormGroup>
              <FormGroup>
                <Label className="label">Title</Label>
                <Input
                  type="text"
                  name="title"
                  className="input"
                  id="title"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.title}
                />
                {formik.errors.title && formik.touched.title ? (
                  <div className="text-danger">*{formik.errors.title}</div>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className="label" for="topic">
                  Topic
                </Label>
                <div style={{ display: "flex" }}>
                  <Input
                    type="select"
                    name="topic"
                    className="input"
                    id="topic"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.topic}
                  >
                    <ChevronDown set="light" />
                    <option>Choose Topic</option>
                    {topicwithSubtopic?.map((row, key) => (
                      <option key={key} value={row.name}>
                        {row.name}
                      </option>
                    ))}
                    <option value={"Other"}>Others</option>
                  </Input>
                  {/* <Button className="button ml-3" onClick={()=>handleClickOpen('Topic')}>Add New</Button> */}
                </div>
                {formik.errors.topic && formik.touched.topic ? (
                  <div className="text-danger">*{formik.errors.topic}</div>
                ) : null}
              </FormGroup>
              {formik.values.topic === "Other" ? (
                <FormGroup>
                  <Label className="label">New Topic</Label>
                  <Input
                    type="text"
                    name="newTopic"
                    className="input"
                    id="newTopic"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.newTopic}
                  />
                  {formik.errors.newTopic && formik.touched.newTopic ? (
                    <div className="text-danger">*{formik.errors.newTopic}</div>
                  ) : null}
                </FormGroup>
              ) : null}
              <FormGroup>
                <Label className="label">SubTopic</Label>
                <div style={{ display: "flex" }}>
                  <Input
                    type="select"
                    name="subTopic"
                    className="input"
                    id="subTopic"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.subTopic}
                  >
                    <ChevronDown set="light" />
                    <option>Choose Sub Topic</option>
                    {topicwithSubtopic
                      ?.filter((value) => value.name === formik.values.topic)[0]
                      ?.subTopiks.map((row, key) => (
                        <>
                          <option key={key} value={row.name}>
                            {row.name}
                          </option>
                        </>
                      ))}
                    {formik.values.topic === "" ? null : (
                      <option value={"Other"}>Others</option>
                    )}
                  </Input>
                </div>
                {formik.errors.subTopic && formik.touched.subTopic ? (
                  <div className="text-danger">*{formik.errors.subTopic}</div>
                ) : null}
              </FormGroup>
              {formik.values.subTopic === "Other" ? (
                <FormGroup>
                  <Label className="label">New Sub Topic</Label>
                  <Input
                    type="text"
                    name="newSubTopic"
                    className="input"
                    id="newSubTopic"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.newSubTopic}
                  />
                  {formik.errors.newSubTopic && formik.touched.newSubTopic ? (
                    <div className="text-danger">
                      *{formik.errors.newSubTopic}
                    </div>
                  ) : null}
                </FormGroup>
              ) : null}
              <FormGroup>
                <Label className="label" for="nameInput">
                  Describe your idea
                </Label>
                <SunEditor
                  id="description"
                  name="description"
                  value={formik.values.description}
                  handleBlur={handleChangeSunEditor}
                  onImageUploadBefore={handleImageUploadBefore}
                />
                {formik.errors.description && formik.touched.description ? (
                  <div className="text-danger">
                    *{formik.errors.description}
                  </div>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className="label" for="nameInput">
                  Attach File
                </Label>
                <Input
                  type="file"
                  name="attach_file"
                  onChange={handleUploadFile}
                  multiple
                  id="attach_file"
                />
                <List>
                  {viewListFile.map((row) => (
                    <ListItem>
                      <div>
                        {row.fileOriginalName}
                        <IconButton onClick={() => handleClearList(row.id)}>
                          <DeleteIcon />
                        </IconButton>
                      </div>
                    </ListItem>
                  ))}
                </List>
              </FormGroup>
              <FormGroup tag="Inputset" onClick={formik.handleChange}>
                <Label className="label">Type of Creator</Label>
                <FormGroup check>
                  <Label check>
                    <Input
                      type="radio"
                      name="typeof"
                      value="personal"
                      defaultChecked
                    />{" "}
                    Personal
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="radio" name="typeof" value="team" /> Team
                  </Label>
                </FormGroup>
              </FormGroup>
              {formik.values.typeof === "team" ? (
                <>
                  <FormGroup>
                    <Label className="label-kalbe-form">Team Name</Label>
                    <Input
                      type="text"
                      name="teamName"
                      className="border-radius-10 height-56"
                      id="teamName"
                      onChange={formik.handleChange}
                      value={formik.values.teamName}
                    />
                  </FormGroup>
                  <FormGroup>
                    {/* <List>
                        {formik.values.teamMember.map((row,key)=>(
                          <ListItem>{row.name}</ListItem>
                        ))}
                      </List> */}
                    <Autocomplete
                      multiple
                      id="teamMember"
                      name="teamMember"
                      options={teamMemberList}
                      onBlur={formik.handleBlur}
                      getOptionLabel={(option) => option.name}
                      filterSelectedOptions
                      onInputChange={(value) =>
                        handleInputSearchTeamMember(value)
                      }
                      getOptionSelected={(option, defaultValue) =>
                        option.name === defaultValue.name
                      }
                      renderTags={(tagValue, getTagProps) => (
                        <List>
                          {tagValue.map((option, index) => (
                            <ListItem>
                              <Chip
                                variant="outlined"
                                avatar={
                                  <img
                                    src="/images/profile-picture.jpg"
                                    className="mr-3 mb-0 border-radius-50"
                                    style={{
                                      objectFit: "cover",
                                      height: "2.75em",
                                      width: "2.75em",
                                    }}
                                  />
                                }
                                label={
                                  <div>
                                    <div>{option.name}</div>
                                    <div>{option.jobTtlName}</div>
                                  </div>
                                }
                                {...getTagProps({ index })}
                              />
                            </ListItem>
                          ))}
                        </List>
                      )}
                      value={formik.values.teamMember}
                      onChange={(e, value) =>
                        // formik.setFieldValue("teamMember", value)
                        handleOnChangeTeamMember(e, value)
                      }
                      renderInput={(params) => (
                        <div>
                          <TextField
                            {...params}
                            variant="outlined"
                            fullWidth
                            label="Team Member"
                            placeholder="Favorites"
                          />
                        </div>
                      )}
                    />
                  </FormGroup>
                </>
              ) : null}
              <br />
              <FormGroup>
                <div style={{ display: "flex" }}>
                  <Autocomplete
                    multiple
                    id="hastag"
                    name="hastag"
                    options={hastagList}
                    getOptionLabel={(option) => option.name}
                    filterSelectedOptions
                    fullWidth
                    onBlur={formik.handleBlur}
                    onInputChange={(value) => handleInputNewHastag(value)}
                    onChange={(e, value) =>
                      formik.setFieldValue("hastag", value)
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        label="Hastag"
                        placeholder="Favorites"
                      />
                    )}
                  />
                  {/* <Button className="button ml-3" onClick={()=>handleClickOpen('Hastag')}>Add New</Button> */}
                </div>
                {formik.errors.hastag && formik.touched.hastag ? (
                  <div className="text-danger">*{formik.errors.hastag}</div>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className="label" for="topic">
                  Post Setting
                </Label>
                <div style={{ display: "flex" }}>
                  <Input
                    type="select"
                    name="viewAccessLevel"
                    className="input"
                    id="viewAccessLevel"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.viewAccessLevel}
                  >
                    <ChevronDown set="light" />
                    <option value={0}>Everyone</option>
                    <option value={1}>BU</option>
                    <option value={2}>SBU</option>
                    <option value={3}>Me</option>
                  </Input>
                </div>
                {/* {formik.errors.topic && formik.touched.topic ? (
                  <div className="text-danger">*{formik.errors.topic}</div>
                ) : null} */}
              </FormGroup>
              <div style={{ display: "flex" }}>
                <Button
                  className="button-primary button-large-full mt-3 mr-2"
                  disabled={holdSubmit}
                  onClick={() => handleOnSave()}
                >
                  Save
                </Button>
                <Button
                  className="button-primary button-large-full mt-3 ml-2"
                  type={"submit"}
                  disabled={holdSubmit}
                >
                  Submit Idea
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </BaseLayoutFull>
      <DialogAddNewItem
        open={open}
        data={formik.values}
        dialogName={dialogName}
        handleClose={handleClose}
      />
    </div>
  );
}

export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    return {
      props: {
        topicwithSubtopic: null,
        hastag: null,
        teamMemberDatas: null,
        menuDatas: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const response = await fetch(`${API_URL}${API_TOPIKSWITHSUBTOPIKS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const data = await response.json();
  const topicwithSubtopic = [];
  topicwithSubtopic.push(...data);

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const responseHastag = await fetch(`${API_URL}${API_TAGS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataHastag = await responseHastag.json();
  const hastagDatas = [];
  hastagDatas.push(...dataHastag);

  const responseTeamMember = await fetch(`${API_TEAMMEMBERLIST}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataTeamMember = await responseTeamMember.json();
  const teamMemberDatas = [];
  teamMemberDatas.push(...dataTeamMember.data);

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath;

  if (!topicwithSubtopic && !hastagDatas && !teamMemberDatas) {
    return { notFound: true };
  }
  return {
    props: {
      topicwithSubtopic: topicwithSubtopic,
      notifications: notifications,
      notifCount: notifCountData,
      hastagDatas: hastagDatas,
      teamMemberDatas: teamMemberDatas,
      menuDatas: menuDatas,
      profilePicture: profilePicture,
    },
  };
}
