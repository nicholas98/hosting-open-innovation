import { Card, CardBody, Col, Row } from "reactstrap";
import { Show, Chat } from "react-iconly";
import ProfileLayout from "components/layouts/ProfileLayout";
import Image from "next/image";
import {
  API_URL,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_KEY,
  API_MENUWITHSUBMENUS,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_NOTIFICATIONS,
} from "constant";

const CreateStatItem = ({ icon, bgColor, value, desc }) => (
  <Col lg="5" xs="6" className="mb-4">
    <Card className="border-radius-12">
      <CardBody className="py-4">
        <div className={`stats-container bg-stats-${bgColor}`}>{icon}</div>
        <h3 className="font heading2">{value}</h3>
        <p className="font body-copy text-kalbe-darkGrey">{desc}</p>
      </CardBody>
    </Card>
  </Col>
);

const OtherStats = (props) => {
  const {
    upn,
    userData,
    menuDatas,
    notifications,
    notifCount,
    profilePicture,
    otherUserPic,
  } = props;

  return (
    <ProfileLayout
      otherUser
      upn={upn}
      menuSubMenu={menuDatas}
      dataNotif={notifications[0]}
      notifCount={notifCount}
      userData={userData.data}
      border
      currentPosition={"stats"}
      profilePicture={profilePicture}
      otherUserPic={otherUserPic.profilePicture}
    >
      <h2 className="font heading2 mb-4">
        {userData && `${userData.data.name}'s Stats`}
      </h2>
      <Row>
        <CreateStatItem
          icon={
            <Image
              src="/images/like-icon.svg"
              width={24}
              height={24}
              className="icon-like-blue"
            />
          }
          bgColor="blue"
          value={`1,123`}
          desc="Likes other authors"
        />
        <CreateStatItem
          icon={
            <Image
              src="/images/like-icon.svg"
              width={24}
              height={24}
              className="icon-like-fuchsia"
            />
          }
          value={472}
          bgColor="fuchsia"
          desc="Likes from other authors"
        />
        <CreateStatItem
          icon={
            <Chat set="light" size="medium" className="icon-comment-orange" />
          }
          bgColor="orange"
          value={955}
          desc={userData && `Comment ${userData.data.name}'s ideas`}
        />
        <CreateStatItem
          icon={<Show set="light" size="medium" className="icon-view-green" />}
          bgColor="green"
          value={`8,291`}
          desc={userData && `User views ${userData.data.name}'s ideas`}
        />
      </Row>
    </ProfileLayout>
  );
};

export async function getServerSideProps(ctx) {
  const { req, res, params } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
      },
    };
  }

  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const upnOther = params.upn;

  const profileResponse = await fetch(`${API_USERPROFILE}/GetByUPN?UPN=${upnOther}`, {
    headers: {
      apikey: `${USERPROFILE_APIKEY}`,
    },
  });

  const dataUser = await profileResponse.json();
  const otherPicResponse = await fetch(
    `${API_PROFILEPICTURE}/${dataUser.data.clusterCode}-${dataUser.data.nik}`,
    {
      headers: {
        Authorization: PROFILEPICTURE_KEY,
      },
    }
  );
  const otherUserPic = await otherPicResponse.json();

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });
  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const upn = tokenJSON.upn;

  const profilePicture = req.cookies.profilePicturePath

  return {
    props: {
      upn: upnOther,
      userData: dataUser,
      menuDatas: menuDatas,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
      otherUserPic: otherUserPic,
    },
  };
}

export default OtherStats;
