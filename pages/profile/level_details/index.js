import BaseLayoutFull from "components/layouts/BaseLayoutFull";
import { Col, Progress, Row } from "reactstrap";
import {
  API_URL,
  API_KEY,
  API_MENUWITHSUBMENUS,
  API_USERPROFILE,
  USERPROFILE_APIKEY,
  API_PROFILEPICTURE,
  PROFILEPICTURE_KEY,
  API_NOTIFICATIONS,
} from "constant";
const CreateProgressBar = ({ value, min, max }) => (
  <Progress
    min={min}
    max={max}
    value={value}
    className="flex-fill progressBar-height"
    barClassName="bg-kalbe-orange border-radius-10"
  />
);

const CreateRankDetail = ({ end, rankName, rankValue }) => (
  <div className={`d-flex flex-column m-0 ${end && "justify-content-end"}`}>
    <p className="font detail-text text-kalbe-darkGrey">{rankName}</p>
    <p className="font body-copy text-kalbe-darkGrey">{`${rankValue} pts`}</p>
  </div>
);

const CreateDetailItem = ({ task, point }) => (
  <Col md="6" xs="12">
    <div className="border-bottom d-flex align-items-center pt-2 pb-1">
      <p className="font body-copy">{task}</p>
      <div className="d-flex align-items-center ml-auto">
        <h3 className="font heading3">{point}</h3>
        <p className="font body-copy ml-1 mb-2 align-items-end">pts</p>
      </div>
    </div>
  </Col>
);

const LevelDetails = (props) => {
  const { menuDatas, notifications, notifCount, profilePicture } = props;
  let currentValue = 15000;
  return (
    <BaseLayoutFull
      backOnly
      menuSubMenu={menuDatas}
      dataNotif={notifications[0]}
      notifCount={notifCount}
      profilePicture={profilePicture}
    >
      <div
        className="w-100"
        style={{ height: "120px", backgroundColor: "#4C41D8", opacity: "0.1" }}
      ></div>
      <div className="container" style={{ marginTop: "-67.5px" }}>
        <div className="d-flex flex-column justify-content-center align-items-center mb-3">
          <img src="/badge-rank/7-innovator.png" height={128} />
          <h3 className="font heading3">{`LV.7 - The Innovator`}</h3>
          <h3 className="font heading3 font-weight-normal">{`${`100,000`} pts`}</h3>
          <div className="w-100 mt-4 mb-5">
            <div className="d-flex">
              <CreateProgressBar value={currentValue} min={0} max={500} />
              <CreateProgressBar value={currentValue} min={500} max={1000} />
              <CreateProgressBar value={currentValue} min={1000} max={10000} />
              <CreateProgressBar value={currentValue} min={10000} max={30000} />
              <CreateProgressBar value={currentValue} min={30000} max={60000} />
              <CreateProgressBar
                value={currentValue}
                min={60000}
                max={100000}
              />
            </div>
            <div
              className="d-flex justify-content-between"
              style={{ marginTop: "-13.75px" }}
            >
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
              <img src="/images/level-indicator.png" height={24} width={24} />
            </div>
            <div className="d-flex justify-content-between">
              <CreateRankDetail rankName="Rookie" rankValue={0} />
              <CreateRankDetail rankName="Thinker" rankValue={500} />
              <CreateRankDetail rankName="Experimenter" rankValue={1000} />
              <CreateRankDetail rankName="Developer" rankValue={10000} />
              <CreateRankDetail rankName="Creator" rankValue={30000} />
              <CreateRankDetail rankName="Pioneer" rankValue={60000} />
              <CreateRankDetail end rankName="Innovator" rankValue={100000} />
            </div>
          </div>
          <p className="font body-copy text-kalbe-darkGrey">{`Congrats! You already reach the max level`}</p>
        </div>
        <hr className="m-0" />
        <div>
          <h3 className="font heading3 font-weight-bold mt-4 mb-1">
            How to get point
          </h3>
          <p className="font body-copy text-kalbe-darkGrey mb-1">
            You can increase your point everytime you doing this activities.
          </p>
          <Row>
            <CreateDetailItem task="View unique ideas" point={1} />
            <CreateDetailItem task="View unique ideas" point={1} />
            <CreateDetailItem task="Getting view from your ideas" point={2} />
            <CreateDetailItem task="Getting view from your ideas" point={2} />
            <CreateDetailItem task="Like an idea" point={3} />
            <CreateDetailItem task="Like an idea" point={3} />
            <CreateDetailItem task="Getting ideas post liked" point={5} />
            <CreateDetailItem task="Getting ideas post liked" point={5} />
            <CreateDetailItem task="Comment on others ideas" point={6} />
            <CreateDetailItem task="Comment on others ideas" point={6} />
            <CreateDetailItem task="Join a project" point={50} />
            <CreateDetailItem task="Join a project" point={50} />
          </Row>
        </div>
      </div>
    </BaseLayoutFull>
  );
};
export async function getServerSideProps(ctx) {
  const { req, res } = ctx;

  const cookies = req.cookies.Data;
  if (!cookies) {
    res.setHeader("location", "/login");
    res.statusCode = 302;
    res.end();
    // ganti validasi
    return {
      props: {
        menuDatas: null,
      },
    };
  }
  const tokenJSON = JSON.parse(cookies);
  const token = tokenJSON.token;

  const notifResponse = await fetch(`${API_URL}${API_NOTIFICATIONS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const dataNotif = await notifResponse.json();
  const notifications = [];
  notifications.push([...dataNotif]);

  const notifCount = await fetch(`${API_URL}${API_NOTIFICATIONS}/count`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey:
        "eyJ4NXQiOiJZamt5WkRVM05tRTRZbVZqT1RjeE4yRTRNbVZrT1dSak1XVmhZVGhoWWpjeE9UZzJNemt4WVE9PSIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuaWNob2xhc0BrYWxiZS5jby5pZCIsImFwcGxpY2F0aW9uIjp7Im93bmVyIjoibW9oYW1tYWQuYW1pcnJ1ZGluQGthbGJlLmNvLmlkIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJPcGVuIElubm92YXRpb24iLCJpZCI6NDgsInV1aWQiOiIwY2ZkMzQ0NC01ZWMyLTQxM2UtYmRmZS02NGI0NDk3MjM3ZDIifSwiaXNzIjoiaHR0cHM6XC9cL20tb25lLmthbGJlLmNvLmlkOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJ0aWVySW5mbyI6eyJVbmxpbWl0ZWQiOnsidGllclF1b3RhVHlwZSI6InJlcXVlc3RDb3VudCIsImdyYXBoUUxNYXhDb21wbGV4aXR5IjowLCJncmFwaFFMTWF4RGVwdGgiOjAsInN0b3BPblF1b3RhUmVhY2giOnRydWUsInNwaWtlQXJyZXN0TGltaXQiOjAsInNwaWtlQXJyZXN0VW5pdCI6bnVsbH19LCJrZXl0eXBlIjoiU0FOREJPWCIsInBlcm1pdHRlZFJlZmVyZXIiOiIiLCJzdWJzY3JpYmVkQVBJcyI6W3sic3Vic2NyaWJlclRlbmFudERvbWFpbiI6ImthbGJlLmNvLmlkIiwibmFtZSI6IkthbGJlT3Blbklubm92YXRpb25XZWJBUEkiLCJjb250ZXh0IjoiXC90XC9rYWxiZS5jby5pZFwvS2FsYmVPcGVuSW5ub3ZhdGlvbldlYkFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtYXR0aGV3LmJlbm5ldHRAa2FsYmUuY28uaWQiLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn0seyJzdWJzY3JpYmVyVGVuYW50RG9tYWluIjoia2FsYmUuY28uaWQiLCJuYW1lIjoiR2xvYmFsLUF1dGhlbnRpY2F0aW9uIiwiY29udGV4dCI6IlwvdFwva2FsYmUuY28uaWRcL2F1dGhlbnRpY2F0aW9uXC92MSIsInB1Ymxpc2hlciI6Im1vaGFtbWFkLmFtaXJydWRpbkBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifSx7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJrYWxiZS5jby5pZCIsIm5hbWUiOiJVc2VyUHJvZmlsZUFQSSIsImNvbnRleHQiOiJcL3RcL2thbGJlLmNvLmlkXC9Vc2VyUHJvZmlsZUFQSVwvdjEiLCJwdWJsaXNoZXIiOiJtdWhhbW1hZC5kaWFuaUBrYWxiZS5jby5pZCIsInZlcnNpb24iOiJ2MSIsInN1YnNjcmlwdGlvblRpZXIiOiJVbmxpbWl0ZWQifV0sInBlcm1pdHRlZElQIjoiIiwiaWF0IjoxNjIyNzgyNzc4LCJqdGkiOiI5NTJlZmZiMy05ODkwLTQ0ZWEtOGU3ZC0xZTQ3ZWY5MjA4OTQifQ==.aXj-1gpExsIv3DZt8ogZSiEmRv7jszu0S8EaIT4VNDrTVAFmnrYUHy0YH9F_aooZTh2cWzFzZO0lXs-hzOzzhtrJqFq2UPsV_KRIIGL_5b-UCZIWRMXX6aOHH-TcMYdGYlkrZEWgjiWQXJjbNMO_O6IIOPIprGczQ9Lp-tMEAgAytNVGk_BIOhif-h3_FVsDKbSrW9QAKQ7gFP_lej70i7uMbaPgfK8qmw1s-s5wXmwfy2zTIBhNkFw-cIeqxWKYRzHKTJvhfUkjALuviafOIYUoC-NKZm3AkSlHL-IKGelppO3lotlnAzI7JzRoM9C4wtEPi6rOeMUmkbfKcgnxkw==",
    },
  });
  const notifCountData = await notifCount.json();

  const responseMenu = await fetch(`${API_URL}${API_MENUWITHSUBMENUS}`, {
    headers: {
      OIAuthorization: `Bearer ${token}`,
      apiKey: API_KEY,
    },
  });

  const dataMenu = await responseMenu.json();
  const menuDatas = [];
  menuDatas.push([...dataMenu]);

  const upn = tokenJSON.upn;
  const profilePicture = req.cookies.profilePicturePath

  if (!menuDatas) {
    return { notFound: true };
  }
  return {
    props: {
      menuDatas: menuDatas,
      notifications: notifications,
      notifCount: notifCountData,
      profilePicture: profilePicture,
    },
  };
}
export default LevelDetails;
