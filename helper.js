import { useContext } from "react";
import {
  API_URL,
  API_BANNER,
  API_MENU,
  API_MENUWITHSUBMENUS,
  API_SUBMENU,
  API_IDEACATALOGS,
  API_KATEGORISWITHSUBKATEGORIS,
  API_KATEGORIS,
  API_SUBKATEGORIS,
  API_TOPIKS,
  API_SUBTOPIKS,
  API_TOPIKSWITHSUBTOPIKS,
  API_TAGS,
  API_LOGIN,
  API_POSTFILEMULTIPLE,
  API_POSTFILE,
  API_FILES,
  API_FILESDOWNLOAD,
  API_TEAMMEMBERLIST,
  API_ALLCOMMENTS,
  API_COMMENTS,
  API_LIKES,
  API_ALLLIKES,
  API_KEY,
  API_IMAGEURL,
  API_INBOX,
  API_PROFILEPICTURE,
  API_IMAGE,
  API_NOTIFICATIONS,
  API_TEAMMEMBERDETAIL,
  API_GROUPUSER,
  // TOKEN,
} from "./constant";
import { useCookies } from "react-cookie";
import lscache from "lscache";

// var now = new Date();
// var date = now.getDate();
// var month = now.getMonth() + 1;
// var year = now.getFullYear();

// const { state, dispatch, actions } = useContext(Stores);

const header = new Headers();
const headerPostFile = new Headers();
const headerApiKey = new Headers();
const headerEditProfile = new Headers();
const headerImage = new Headers();

// function getCookie() {
//   const [cookies, setCookie, removeCookie] = useCookies();
//   return console.log(cookies);
// }

// export default class ApiRequest {
//   static getFetch = (path) => new Promise((resolve, reject) => {
//     fetch(`${SERVER_DOMAIN}/${path}`, getMethods())
//       .then(helper.checkStatusResponseAPI)
//       .then((responseJson) => {
//         resolve(responseJson);
//       })
//       .catch((err) => {
//         reject(handleFetchError(err));
//       });
//   });

//   static postFileFetch = (path, data) => new Promise((resolve, reject) => {
//     fetch(`${SERVER_DOMAIN}/${path}`, postHeaderFiles(data))
//       .then(helper.checkStatusResponseAPI)
//       .then((responseJson) => {
//         resolve(responseJson);
//       })
//       .catch((err) => {
//         reject(handleFetchError(err));
//       });
//   });

//   static postFetch = (path, data) => new Promise((resolve, reject) => {
//     fetch(`${SERVER_DOMAIN}/${path}`, postHeaders(data))
//       .then(helper.checkStatusResponseAPI)
//       .then((responseJson) => {
//         resolve(responseJson);
//       })
//       .catch((err) => {
//         reject(handleFetchError(err));
//       });
//   });

//   static deleteFetch = (path) => new Promise((resolve, reject) => {
//     fetch(`${SERVER_DOMAIN}/${path}`, deleteHeaders())
//       .then(helper.checkStatusResponseAPI)
//       .then((responseJson) => {
//         resolve(responseJson);
//       })
//       .catch((err) => {
//         reject(handleFetchError(err));
//       });
//   });

//   static putFetch = (path, data) => new Promise((resolve, reject) => {
//     fetch(`${SERVER_DOMAIN}/${path}`, putHeaders(data))
//       .then(helper.checkStatusResponseAPI)
//       .then((responseJson) => {
//         resolve(responseJson);
//       })
//       .catch((err) => {
//         reject(handleFetchError(err));
//       });
//   });
// }

// const [cookies, setCookie, removeCookie] = useCookies();
if (typeof window !== "undefined") {
  header.append("Accept", "application/json");
  header.append("Content-Type", "application/json");
  // header.append("OIAuthorization", `Bearer ${window.localStorage.accessToken}`);
  header.append("OIAuthorization", `Bearer ${lscache.get("accessToken")}`);
  header.append("apikey", `${API_KEY}`);
  headerPostFile.append("Accept", "application/json");
  headerPostFile.append(
    "OIAuthorization",
    `Bearer ${lscache.get("accessToken")}`
  );
  // headerPostFile.append(
  //   "OIAuthorization",
  //   `Bearer ${window.localStorage.accessToken}}`
  // );
  headerPostFile.append("apikey", `${API_KEY}`);
  // headerImage.append("OIAuthorization", `Bearer ${window.localStorage.accessToken}`);
  // headerImage.append("apikey", `${API_KEY}`)
} else {
  header.append("Accept", "application/json");
  header.append("Content-Type", "application/json");
}

headerApiKey.append("Accept", "application/json");
headerApiKey.append("Content-Type", "application/json");
headerApiKey.append("apikey", `${API_KEY}`);

headerEditProfile.append(
  "Authorization",
  "api4265935d46724712a9e5e4978087d427f2824d7ca5be46b29b561392409fc9f097dfe264916240489b85f8c8abebe96cdb7ef3d2c054404ca718aca4067b8818"
);
headerEditProfile.append("Content-Type", "application/json");

const getBanner = () => {
  return fetch(API_URL + API_BANNER, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const statusEdit = (values) => {
  const arrayValue = values.ideaCatalogStatus;

  return fetch(API_URL + API_IDEACATALOGS + `/${values.id}`, {
    method: "PATCH",
    headers: header,
    body: JSON.stringify(arrayValue),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getMenu = () => {
  return fetch(API_URL + API_MENU, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getDetailTeamMember = (nik, cluster) => {
  return fetch(`${API_TEAMMEMBERDETAIL}?NIK=${nik}&cluster=${cluster}`, {
    headers: headerApiKey,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getMenuWithSubMenu = () => {
  return fetch(API_URL + API_MENUWITHSUBMENUS, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getImageUrl = (id) => {
  return fetch(API_URL + API_IMAGEURL + `/${id}`, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getIdeaCatalogs = () => {
  return fetch(API_URL + API_IDEACATALOGS, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getTrendingIdea = () => {
  return fetch(API_URL + API_IDEACATALOGS + "?PageNumber=1&PageSize=4", {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getIdeaTopics = () => {
  return fetch(API_URL + API_TOPIKSWITHSUBTOPIKS, {
    headers: {
      accept: "text/plain",
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};
const onLogin = (values) => {
  return fetch(API_URL + API_LOGIN, {
    method: "POST",
    headers: headerApiKey,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const onEditProfile = (content) => {
  return fetch(`${API_URL}/Users/editprofilepicture`, {
    method: "POST",
    mode: "cors",
    headers: header,
    body: JSON.stringify(content),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const onReadNotif = (id) => {
  return fetch(`${API_URL}${API_NOTIFICATIONS}/${id}`, {
    method: "PATCH",
    headers: header,
    body: JSON.stringify([
      {
        op: "replace",
        path: "/isRead",
        value: true,
      },
    ]),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getTeamMemberList = (value) => {
  return fetch(`${API_TEAMMEMBERLIST}?Search=${value}`, {
    headers: headerApiKey,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getTopicwithSubtopic = () => {
  return fetch(API_URL + API_TOPIKSWITHSUBTOPIKS, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};
const postCover = (values) => {
  const formData = new FormData();
  formData.append("formFile", values);
  return fetch(API_URL + API_POSTFILE, {
    method: "POST",
    headers: headerPostFile,
    body: formData,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const postFileMultiple = (values) => {
  return fetch(API_URL + API_POSTFILEMULTIPLE, {
    method: "POST",
    headers: headerPostFile,
    body: values,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const getTag = () => {
  return fetch(API_URL + API_TAGS, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const postTag = (newTagData) => {
  return fetch(API_URL + API_TAGS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(newTagData),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateTag = (newTagData, id) => {
  return fetch(`${API_URL}${API_TAGS}/${id}`, {
    method: "PUT",
    headers: header,
    body: JSON.stringify(newTagData),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteTag = (id) => {
  return fetch(`${API_URL}${API_TAGS}/${id}`, {
    method: "DELETE",
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getAllComments = (id) => {
  return fetch(API_URL + API_ALLCOMMENTS + `/${id}`, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getAllLikes = (id) => {
  return fetch(API_URL + API_ALLLIKES + `/${id}`, {
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const submitIdea = (values) => {
  return fetch(API_URL + API_IDEACATALOGS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};
const sendMessage = (values) => {
  return fetch(API_URL + API_INBOX, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};
const deleteMessage = (id) => {
  return fetch(API_URL + API_INBOX + `/${id}`, {
    headers: header,
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const submitTopic = (values) => {
  return fetch(API_URL + API_TOPIKS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const editIdea = (values, id) => {
  return fetch(API_URL + API_IDEACATALOGS + `/${id}`, {
    method: "PUT",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const submitSubTopic = (values) => {
  return fetch(API_URL + API_SUBTOPIKS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const submitHastag = (values) => {
  return fetch(API_URL + API_TAGS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const deleteIdea = (id) => {
  return fetch(API_URL + API_IDEACATALOGS + `/${id}`, {
    headers: header,
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const downloadFiles = (id) => {
  return fetch(API_URL + API_FILESDOWNLOAD + `/${id}`, {
    method: "GET",
    headers: header,
    responseType: "blob",
  })
    .then((response) => {
      return response.blob();
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const submitComment = (values) => {
  return fetch(API_URL + API_COMMENTS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const submitLike = (values) => {
  return fetch(API_URL + API_LIKES, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};
const deleteLike = (id, values) => {
  return fetch(API_URL + API_LIKES + `/${id}`, {
    headers: header,
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const editComment = (values) => {
  const arrayValue = [values.commentData];

  return fetch(API_URL + API_COMMENTS + `/${values.id}`, {
    method: "PATCH",
    headers: header,
    body: JSON.stringify(arrayValue),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteComment = (id) => {
  return fetch(API_URL + API_COMMENTS + `/${id}`, {
    headers: header,
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getImage = (id) => {
  return fetch(API_URL + API_IMAGE + `/${id}`, {
    method: "GET",
    headers: header,
    responseType: "blob",
  })
    .then((response) => {
      return response.blob();
    })
    .catch((err) => {
      console.log(err);
      // setWrongUser(true);
    });
};

const postTopik = (values) => {
  return fetch(API_URL + API_TOPIKS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateTopiks = (values) => {
  return fetch(`${API_URL}${API_TOPIKS}/${values.id}`, {
    method: "PUT",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const postSubTopik = (values) => {
  return fetch(API_URL + API_SUBTOPIKS, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteTopik = (id) => {
  return fetch(API_URL + API_TOPIKS + `/${id}`, {
    method: "DELETE",
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteSubTopik = (id) => {
  return fetch(API_URL + API_SUBTOPIKS + `/${id}`, {
    method: "DELETE",
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const updateSubTopik = (values) => {
  return fetch(`${API_URL}${API_SUBTOPIKS}/${values.id}`, {
    method: "PUT",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const postUsers = (values) => {
  return fetch(API_URL + API_GROUPUSER, {
    method: "POST",
    headers: header,
    body: JSON.stringify(values),
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteUsers = (id) => {
  return fetch(API_URL + API_GROUPUSER + `/${id}`, {
    method: "DELETE",
    headers: header,
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      console.log(err);
    });
};

export async function getIdeaById(id) {
  try {
    const settings = {
      headers: {
        // apiKey: TOKEN,
      },
    };
    const response = await fetch(`${API_URL}${API_IDEACATALOGS}/${id}`, {
      headers: header,
    });

    const errorCode = response.ok ? false : response.status;
    const data = await response.json();

    const ideaCatalogsId = [];

    ideaCatalogsId.push({ ...data, errorCode });

    return ideaCatalogsId;
  } catch (error) {
    console.log(error.name);
  }
}

export async function getIdeaByUPN(upn) {
  try {
    const response = await fetch(
      `${API_URL}${API_IDEACATALOGS}/?CreatedBy=${upn}`,
      {
        headers: header,
      }
    );

    const errorCode = response.ok ? false : response.status;

    const data = await response.json();

    const ideaCatalogsId = [];
    if (errorCode) {
      ideaCatalogsId.push([...errorCode]);
    }
    ideaCatalogsId.push([...data]);

    return ideaCatalogsId;
  } catch (error) {
    console.log(error.name);
  }
}

export async function getAPIfiles(id) {
  try {
    const settings = {
      headers: {
        // apiKey: TOKEN,
      },
    };
    const response = await fetch(`${API_URL}${API_FILES}/${id}`, {
      headers: header,
    });
    const errorCode = response.ok ? false : response.status;
    const data = await response.json();

    const files = [];

    files.push({ ...data, errorCode });

    return files;
  } catch (error) {
    console.log(error.name);
  }
}

export {
  getMenu,
  getBanner,
  getIdeaCatalogs,
  onLogin,
  onEditProfile,
  onReadNotif,
  getTopicwithSubtopic,
  getTag,
  postTag,
  updateTag,
  deleteTag,
  getImageUrl,
  submitIdea,
  submitTopic,
  submitSubTopic,
  submitHastag,
  postCover,
  postFileMultiple,
  getTrendingIdea,
  getMenuWithSubMenu,
  getTeamMemberList,
  sendMessage,
  deleteMessage,
  downloadFiles,
  deleteIdea,
  editIdea,
  getAllComments,
  submitComment,
  submitLike,
  deleteLike,
  editComment,
  deleteComment,
  getAllLikes,
  getImage,
  statusEdit,
  getDetailTeamMember,
  postTopik,
  postSubTopik,
  deleteSubTopik,
  deleteTopik,
  updateTopiks,
  updateSubTopik,
  postUsers,
  deleteUsers,
};
